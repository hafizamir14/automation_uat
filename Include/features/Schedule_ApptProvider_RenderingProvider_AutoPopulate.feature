Feature: Schedule Appoiotment  - Appointment provider, rendering provider auto populate
	Background: 
		Given I navigate to CMR_Schedule

@SmokeUSMM_VerifyAppt_RenderingProvider
	Scenario Outline: Verify Appointment Provider & Rendering Provider Auto Populated
	And I double click on screen to add appointment
    Then I should see schedule appointment popup
    When I enter <Patient> as appointment patient
    And I enter <Reason> as appointment reason
    * I should see <Patient> as actual patient name
    Then I should see appt provider is populated
    * I should see rendering provider is populated

    Examples: 
      | Patient          | Reason    | LocationOfVisit1 | Code1 | LocationOfVisit2                 | Code2 | PADTestToday | ReasonTest | LeftResults | RightResults | DiagnosisCode | Facility          | Constitutional       | MRN           | Soap_Status | AddendumNotes | Member_Verbally_Acknowledged | Billing_Status |
      | Dermo505, Mac505 | Back pain | Home             | visit | Telehealth utilizing audio/video | visit | Yes          | Diabetes   | 0.99-0.90   | 0.89-0.60    | Z13.6         | VPA PC WEST ALLIS | In no acute distress | MRN0000014455 | Signed      | Addendum Test | Yes                          | Signed         |
