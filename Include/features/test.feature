Feature: Soap Note - Add Vital Signs

  Background: 
    Given I navigate to patient grid

  @SmokeUSMM_Blank
  Scenario Outline: Blank
    When I search <Patient> using global search12
    And I click on care plan tab11
    And I click on add new care plan buttontest
    And I click on basedonpatientmedicalrecordtest
    When I enter titletest <Title>
    And I click on save and close buttontest
    Then I should see <ExpectedText> success message

    Examples: 
      | Patient          | Title          | SucessMessage                           | ExpectedText |
      | Dermo505, Mac505 | TestingPatient | successCare Plan Saved SuccessfullyHide | Saved        |
