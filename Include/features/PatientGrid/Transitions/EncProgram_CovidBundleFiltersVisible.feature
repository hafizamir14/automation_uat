Feature: Transition - EncProgram_CovidBundleFiltersVisible

  Background: 
    Given I navigate to Transition grid

  Scenario Outline: Verify That After Select EncProgram CovidBundleFilters Is Visible
    When I clear before and after dates
    And I click on care cordination reset button
    * I clear before and after dates
    * I enter <EncProgram> as encounter program
    Then I should see CovidBundle filter
    * I click on care cordination apply button
    * I select <Export> as export
    Then I verify that <Tranistion> file downloaded

    Examples: 
      | EncProgram | Enterprise       | Export               | Tranistion  |
      | BPCI       | Prime Healthcare | Export (All Columns) | Tranistions |
