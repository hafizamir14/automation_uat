Feature: Transition - Enc Program should auto-select in case of single value

  Background: 
    Given I navigate to patient grid

  Scenario Outline: Verify Transition - Enc Program should auto-select in case of single value
    When I click on care cordination
    And I clear before and after dates
    * I click on care cordination reset button
    * I clear before and after dates
    Then I should see <EncProgram> as auto_selected

    Examples: 
      | EncProgram | Enterprise       |
      | TCM       | Prime Healthcare |
