Feature: Transition - Move Enc. Type Under Enc. Program

  Background: 
    Given I navigate to Transition grid

  Scenario Outline: Verify Transition - Move Enc. Type Under Enc. Program
    When I clear before and after dates
    And I click on care cordination reset button
    * I clear before and after dates
    Then I should see notifyType under EncType filter

    Examples: 
      | EncProgram | Enterprise       |
      | BPCI       | Prime Healthcare |
