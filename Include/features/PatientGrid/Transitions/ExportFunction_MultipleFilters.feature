Feature: Export With Multiple Filters - Transition

  Background: 
    Given I navigate to Transition grid

  @Transition_ExportFunctionWithMultipleFilters
  Scenario Outline: Verify Exports Function With Multiple Filters
    When I clear before and after dates
    And I click on care cordination reset button
    * I clear before and after dates
    * I enter <ServingFacility> as serving facility
    * I enter <EncounterType> as encounter type
    * I click on care cordination apply button
    * I select <Export> as export
    Then I verify that <Tranistion> file downloaded

    Examples: 
      | Export                    | Tranistion  | ServingFacility                  | EncounterType |
      | Export (All Columns)      | Tranistions | Alvarado Hospital Medical Center | Inpatient     |
      | Export (Selected Columns) | Tranistions | Centinela Hospital               | Emergency     |
