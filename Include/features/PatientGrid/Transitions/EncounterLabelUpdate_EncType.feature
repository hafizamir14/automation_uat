Feature: Transition - Updated Encounter Label to Enc. Type

  Background: 
    Given I navigate to Transition grid

  Scenario Outline: Verify Updated Encounter Label to Enc. Type
    When I clear before and after dates
    And I click on care cordination reset button
    * I clear before and after dates
    Then I should see <EncType> Label filter2

    Examples: 
      | EncType   | Enterprise       |
      | Enc. Type | Prime Healthcare |
