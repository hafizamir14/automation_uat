Feature: Transition - DRG Description column

  Background: 
    Given I navigate to Transition grid

  @Smoke_USMM
  Scenario Outline: Verify Transition - DRG Description column
    When I clear before and after dates
    And I click on care cordination reset button
    * I clear before and after dates
    * I click on care cordination apply button
    Then I should see label as <DRGDesc> on transition grid1

    Examples: 
      | DRGDesc  |
      | DRG Description |
