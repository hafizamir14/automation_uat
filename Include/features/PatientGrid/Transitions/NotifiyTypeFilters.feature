Feature: Transitions Notifications Left Filters

	Background: 
		Given I navigate to Transition grid

	Scenario Outline: Verify Transitions Notification Type Filter
		When I clear before and after dates
    And I click on care cordination reset button
    * I clear before and after dates
		   * I select <NotificationType> as Notification Type
		   * I click on care cordination apply button
		Then I should see <NotificationType> care cordination Notifications

		Examples: 
			| NotificationType |
			| Admitted         |
