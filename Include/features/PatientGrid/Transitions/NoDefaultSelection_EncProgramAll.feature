Feature: Transition - No default selection when "All" under Enc. Program

  Background: 
    Given I navigate to Transition grid

  Scenario Outline: Verify Transition - No default selection when "All" under Enc. Program
    When I clear before and after dates
    And I click on care cordination reset button
    * I clear before and after dates
    #* I should see default <EncProgram> as encounter program
    * I select <EncType> from Enc Type
    Then I should see <AdmissionType> Label filter1

    Examples: 
      | EncProgram | Enterprise       | AdmissionType  | EncType   |
      | All        | Prime Healthcare | Admission Type | Inpatient |
