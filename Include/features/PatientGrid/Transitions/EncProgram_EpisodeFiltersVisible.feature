Feature: Transition - EncProgram_Episode Status Filters Visible

  Background: 
    Given I navigate to Transition grid

  Scenario Outline: Verify Episode filter is visible
    When I clear before and after dates
    And I click on care cordination reset button
    * I clear before and after dates
    * I enter <EncProgram> as encounter program
    Then I should see EpStatus filter

    Examples: 
      | EncProgram | Enterprise       |
      | BPCI       | Prime Healthcare |
