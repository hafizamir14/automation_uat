Feature: Transitions Left Filters - Serving Facility Type

  Background: 
    Given I navigate to patient grid

  Scenario Outline: Verify Applying Serving Facility Type Filter
    When I click on care cordination
    * I clear before and after dates
    * I click on care cordination reset button
    * I clear before and after dates
    * I enter <FacilityType> as serving facility type
    * I click on care cordination apply button
    Then I should see care cordination <FacilityType> as serving facility type
    
    Examples: 
      | FacilityType |
      | HOS|
