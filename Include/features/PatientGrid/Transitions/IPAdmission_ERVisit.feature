Feature: Transitions Left Filters - IP Admission” & ER Visits

  Background: 
    Given I navigate to patient grid

  Scenario Outline: IP Admission” column on Patient Grid
    When I click on care cordination
    Then I should see <Columns> on the patient grid as Column
    When I click on care cordination reset button
    And I enter <Date> as assign date of transition grid
    * I click on care cordination apply button
    Then I should be able to <Sort> this columns
    When I click on care cordination reset button
    And I enter <Date> as assign date of transition grid
    * I click on care cordination apply button
    * I filter using <Filters> on transition IP Admission grid
    * I select <Export> as export
    Then I verify that exported file downloaded successfully

    Examples: 
      | Columns       | Sort            | Export                    | Filters | Date     |
      | IP Admissions | Sort Descending | Export (Selected Columns) |       1 | 03262020 |

  Scenario Outline: “ER Visits column on Patient Grid
    When I click on care cordination
    Then I should see <Columns> on the patient grid as Columns
    When I click on care cordination reset button
    And I enter <Date> as assign date of transition grid
    * I click on care cordination apply button
    Then I should be able to <Sort> this column
    When I click on care cordination reset button
    And I enter <Date> as assign date of transition grid
    * I click on care cordination apply button
    * I filter using <Filters> on transition ER Visits grid
    * I select <Export> as export
    Then I verify that exported file downloaded successfully

    Examples: 
      | Columns   | Sort           | Export               | Filters | Date     |
      | ER Visits | Sort Ascending | Export (All Columns) |       1 | 03262020 |
