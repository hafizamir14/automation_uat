Feature: Transition - An enterprise have configuration only BPCI

  Background: 
    Given I navigate to Transition grid

  Scenario Outline: Verify Transition - An enterprise have configuration only BPCI
    When I clear before and after dates
    And I click on care cordination reset button
    * I clear before and after dates
    * I enter <EncProgram> as encounter program
    Then I should see EpStatus filter
    Then I should see CovidBundle filter
    * I should see <EncType> is auto selected as EncType by selecting BPCI
    * I click on care cordination apply button
    * I select <Export> as export
    Then I verify that <Tranistion> file downloaded

    Examples: 
      | EncProgram | Enterprise       | Export               | EncType   | NotifType   | Tranistion  |
      | BPCI       | Prime Healthcare | Export (All Columns) | Inpatient | Notif. Type | Tranistions |
