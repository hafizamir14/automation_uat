Feature: Transitions Left Filters - CoHort

  Background: 
    Given I navigate to patient grid

  Scenario Outline: Applying CoHort Adding Function
    When I click on care cordination
    And I click on care cordination reset button
    * I enter <Date> as assign date of transition grid
    * I enter <ServingFacility> as serving facility
    * I click on care cordination apply button
    * I click on save cohort from dropdown
    Then I should see <Title> as save cohort as
    When I enter <CoHortName> as cohort name
    And I click on cohort save button
    Then I should see <Message> as cohort message
    * I click on care cordination apply button
    * I should see care cordination serving facility <ServingFacility>

    Examples: 
      | ServingFacility   | Date     | Title          | CoHortName                    | Message                              |
      | Abcor Home Health | 03262020 | Save Cohort As | ServingFacilityPlusAssignDate | successCohort saved successfullyHide |

  Scenario Outline: Applying CoHort Deleting Function
    When I click on care cordination
    And I click on care cordination reset button
    * I click on cross button to delete cohort from dropdown
    * I click on proceed button to confirm delete cohort
    * I enter <Date> as assign date of transition grid
    * I enter <ServingFacility> as serving facility
    * I click on care cordination apply button
    Then I should see care cordination serving facility <ServingFacility>

    Examples: 
      | ServingFacility   | Date     | Title          | CoHortName                    | Message                              |
      | Abcor Home Health | 03262020 | Save Cohort As | ServingFacilityPlusAssignDate | successCohort saved successfullyHide |
