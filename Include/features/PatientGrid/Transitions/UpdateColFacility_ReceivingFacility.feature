Feature: Transition - Update Column Facility to Receiving Facility

  Background: 
    Given I navigate to Transition grid

  @Smoke_USMM
  Scenario Outline: Verify Update Column Facility to Receiving Facility
    When I clear before and after dates
    And I click on care cordination reset button
    * I clear before and after dates
    * I click on care cordination apply button
    Then I should see facility label as <ReceivingFacility> on transition grid

    Examples: 
      | ReceivingFacility  |
      | Column Settings Receiving Facility |
