Feature: Tranistion - Columns Title Update

  Background: 
    Given I navigate to Transition grid

  Scenario Outline: Verify Tranistion - Columns Title Update
    When I clear before and after dates
    And I click on care cordination reset button
    * I clear before and after dates
    * I enter <EncProgram> as encounter program
    Then I should see <AdmissionType> Label filter1
    And I should see <EncType> Label filter2
    * I should see <NotifType> Label filter3

    Examples: 
      | EncProgram | Enterprise       | AdmissionType  | EncType   | NotifType   |
      | BPCI       | Prime Healthcare | Admission Type | Enc. Type | Notif. Type |
