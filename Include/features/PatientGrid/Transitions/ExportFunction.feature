Feature: Export - Transition

  Background: 
    Given I navigate to Transition grid

  @Transition_ExportFunction
  Scenario Outline: Verify Exports Function
    When I select <Export> as export
    Then I verify that <Tranistion> file downloaded

    Examples: 
      | Export                    | Tranistion  |
      | Export (All Columns)      | Tranistions |
      | Export (Selected Columns) | Tranistions |
