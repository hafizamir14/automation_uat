Feature: Transition - EncProgram Left Filters

  Background: 
    Given I navigate to Transition grid

  Scenario Outline: Applying Enc Program Filter
    When I clear before and after dates
    And I click on care cordination reset button
    * I enter <Date> as assign date of transition grid
    * I select <EncProgram> patient program
    * I click on care cordination apply button
    Then I should see <EncProgram> care cordination Enc program

    Examples: 
      | EncProgram | Date     |
      | CJR        | 03262020 |
