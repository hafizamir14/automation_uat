Feature: Transition - DRG Column should show only code and not description

  Background: 
    Given I navigate to Transition grid

  Scenario Outline: Verify DRG Column should show only code and not description
    When I clear before and after dates
    And I click on care cordination reset button
    * I clear before and after dates
    * I enter <EncProgram> as encounter program
    * I click on care cordination apply button
    Then I should see Code in DRGCode and Desc in DRGDesc

    Examples: 
      | EncProgram | Enterprise       |
      | CJR       | Prime Healthcare | 
