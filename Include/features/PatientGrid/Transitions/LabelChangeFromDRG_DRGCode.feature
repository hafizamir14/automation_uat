Feature: Transition - Label change from DRG to DRG code

  Background: 
    Given I navigate to Transition grid

  @Smoke_USMM
  Scenario Outline: Transition - Label change from DRG to DRG code
    When I clear before and after dates
    And I click on care cordination reset button
    * I clear before and after dates
    * I click on care cordination apply button
    Then I should see label as <DRGCode> on transition grid

    Examples: 
      | DRGCode                  |
      | DRG Code |
