Feature: Transitions CM Status Left Filters

  Background: 
    Given I navigate to patient grid

  @Smoke_USMM
  Scenario Outline: CM Status Left Filters Verification
    When I click on care cordination
    And I clear before and after dates
    * I click on care cordination reset button
    * I enter <Date> as assign date of transition grid
    * I select <CM_Status> as cm status
    * I click on care cordination apply button
    Then I should see patients with CM Status as <CM_Status> on care coordination grid

    Examples: 
      | CM_Status | Date     |
      | Eligible  | 03262020 |
      | Active    | 03262020 |
