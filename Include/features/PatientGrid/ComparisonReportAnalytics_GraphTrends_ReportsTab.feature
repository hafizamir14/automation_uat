Feature: Comparison Report in Analytics > Graphs / Trends > Reports Tab

  Background: 
    Given I navigate to Analytics grid

  @Analytics_ComparisonReportsTab
  Scenario Outline: Analytics - Reports Tab
    When I click on Reports Tab
    Then I should see <ReportType1> as report type
    When I click on analytics reset button
    And I select ReportType:<Title>
    * I click on analytics apply button
    Then I should see <Label> as defaultlabel and <NoOfCalls> as No Of Calls label
    #When I select Time Period:<TimePeriod>
    #And I click on analytics apply button
    #Then I should see <TimePeriod> as label and <NoOfCalls> as No Of Calls label
    When I click on export report button
    Then I should see exported file with <Title> as name

    Examples: 
      | Patient          | Strtdate          | ReportType1           | Title                 | Label | NoOfCalls    | TimePeriod | Desription            | Owner     | Completeddate     | Taskname                            | Reason           | Conditions | Customs_Conditions | Problems                                             | ReferralSource | dynamicid                                                                           | message                                                                                        | UpdatedBy   | status   |
      | Dermo505, Mac505 | 03202021 04:54 AM | Telehealth Comparison | Telehealth Comparison | Range | No. of Calls | Week       | Task creation process | Care Team | 04202021 04:54 AM | Enjoy your life and forget you age! | Patient Eligible | BPH        | Automation Test    | Encounter for screening for cardiovascular disorders | Provider Team  | Program enrollment status successfully changed to Eligible for selected patient(s). | successProgram enrollment status successfully changed to Eligible for selected patient(s).Hide | Amir, Hafiz | Eligible |
