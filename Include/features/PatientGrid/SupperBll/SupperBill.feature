Feature: Supper Bill flows

  Background: 
    Given I navigate to patient grid With Supper Bill

  @SmokeUSMM_SupperBill_DateRange
  Scenario Outline: Verify that patient soap notes are shown on superbill UI in particular date range
    When I click on Reset button to reset supper bill filters
    And I enter <DateOfService_FromDate> as date of service from date
    * I click on Apply button to apply supper bill filters
    Then I should see particualar patient data in <DateOfService_FromDate> as selected date

    Examples: 
      | DateOfService_FromDate |
      |               12052020 |

  @SmokeUSMM_SupperBill_PracticeFacility
  Scenario Outline: Verify that patients are filtered from left filter against specific practice facility
    When I click on Reset button to reset supper bill filters
    And I enter <DateOfService_FromDate> as date of service from date
    * I enter <Practice> as practice
    * I click on Apply button to apply supper bill filters
    * I click on particular patient and expand
    Then I should see <Practice> particualar patient practice facility

    Examples: 
      | DateOfService_FromDate | Practice                 |
      |               12012020 | VPA OF TEXAS PLLC AUSTIN |

  @SmokeUSMM_SupperBill_Status_Signed
  Scenario Outline: Verify that patients are filtered against the selected soap status signed
    * I select <Soap_Status> as Signed status from soap status
    * I enter <DateOfService_FromDate> as date of service from date
    * I click on Apply button to apply supper bill filters
    Then I should see <Soap_Status> signed as soap status

    Examples: 
      | Soap_Status | DateOfService_FromDate |
      | Signed      |               12052020 |

  @SmokeUSMM_SupperBill_Status_Incomplete
  Scenario Outline: Verify that patients are filtered against Billing status Incomplete
    When I select <Billing_Status> Billing status
    * I enter <DateOfService_FromDate> as date of service from date
    * I click on Apply button to apply supper bill filters
    Then I should see <Billing_Status> patient billing status as incomplete

    Examples: 
      | Billing_Status | DateOfService_FromDate |
      | Incomplete     |               12052020 |

  @SmokeUSMM_SupperBill_PDFView_ParticularPatient
  Scenario Outline: Verify that on superbill UI User is able view soap note in PDF while selecting particular patient
    When I click on Reset button to reset supper bill filters
    * I select <Soap_Status> as Signed status from soap status
    * I enter <DateOfService_FromDate> as date of service from date
    * I click on Apply button to apply supper bill filters
    When I checked the patient checkbox
    * I click on Fax button
    Then I should see PDF file in Fax popup
    * I click on PDF file and Logout

    Examples: 
      | Soap_Status | DateOfService_FromDate |
      | Draft       |               12052020 |
