Feature: DFT - Coders Workflow to update problem code

	Background: 
		Given I navigate to patient grid

	@ProgramEnrollment_CoderWorkFlow
	Scenario Outline: DFT - Coders Workflow to update problem code
		When I search <Patient> using global search
		And I click on Task main tab
		* I select the patient that is associated with task
		* I click on expanding button on the enrollment history tab
		Then I should see the sequence of problem <Problems> is sorted
		
		Examples: 
			| Patient          | Strtdate          | Status      | Desription            | Owner     | Completeddate     | Taskname                            | Reason           | Conditions | Customs_Conditions | Problems                                             | ReferralSource | dynamicid                                                                           | message                                                                                        | UpdatedBy   | status   |
			| Dermo505, Mac505 | 03202021 04:54 AM | In Progress | Task creation process | Care Team | 04202021 04:54 AM | Enjoy your life and forget you age! | Patient Eligible | BPH        | Automation Test    | Encounter for screening for cardiovascular disorders | Provider Team  | Program enrollment status successfully changed to Eligible for selected patient(s). | successProgram enrollment status successfully changed to Eligible for selected patient(s).Hide | Amir, Hafiz | Eligible |
