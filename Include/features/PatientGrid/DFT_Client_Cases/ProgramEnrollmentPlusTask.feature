Feature: DFT - Program Enrollment As Eligible & Task Creation

	Background: 
		Given I navigate to patient grid

	@ProgramEnrollmentEligible
	Scenario Outline: DFT - Program Enrollment As Eligible & Task Creation
		When I search <Patient> using global search
		 And I click on program enrollment dropdown
		   * I expand PACM
		   * I click on eligible link
		Then I should see popup with correct title
		When I select reason2:<Reason>
		 And I enter <Conditions> as conditions
		   * I enter <Customs_Conditions> as customs condition
		   * I select problems:<Problems>
		   * I select referral source:<ReferralSource>
		   * I click on eligible button
		Then I should see message of success: <dynamicid> for following <message>
		   * I click on expanding button on the enrollment history tab
		Then I should see the latest record <status>,<Reason>,<Problems> after expanding the enrollment history
		When I click on Task tab
		 And I click on create task button
		   * I should see Create New Task form with title
		   * I enter <Taskname> as task field
		   * I enter <Strtdate> as task start date field
		   * I enter <Completeddate> as task completed on date field
		   * I enter <Status> as task status field
		   * I enter <Owner> as owner field
		   * I select assigned to from task
		   * I click on save button to save task
		Then I should see success message for created task
		   * I click on patient task tab in main menu
		 And I should see added task as <Taskname> with status <Status> and owner <Owner> present in task section

		Examples: 
			| Patient          | Strtdate          | Status      | Desription            | Owner     | Completeddate     | Taskname                            | Reason           | Conditions | Customs_Conditions | Problems                                             | ReferralSource | dynamicid                                                                           | message                                                                                        | UpdatedBy   | status   |
			| Dermo505, Mac505 | 11202022 04:54 AM | In Progress | Task creation process | Care Team | 12202022 04:54 AM | Enjoy your life and forget you age! | Patient Eligible | BPH        | Automation Test    | Encounter for screening for cardiovascular disorders | Provider Team  | Program enrollment status successfully changed to Eligible for selected patient(s). | successProgram enrollment status successfully changed to Eligible for selected patient(s).Hide | Amir, Hafiz | Eligible |
