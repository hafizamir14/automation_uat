Feature: DFT - PACM Care Manager enters data Using Patient Timer With All Filters & Program Eligible

	Background: 
		Given I navigate to patient grid

	@DFT_PatientTimer
	Scenario Outline: Enter Data Using Patient Timer Activity With All Filters & Programs Eligible
		When I search <Patient> using global search
		 And I verify patient is selected
		   * I click on patient timer button
		Then I should see patient <Patient> as patient_name in timer popup
		When I click on start timer button
		   * I select Program Dropdown as:<Program>
		   * I click on start timer button
		   * I select <CPTCode> as CPT Code
		   * I select <CompletedAction> as Completed Actions
		   * I select <CompletedAction2> as Completed Actions2
		   * I select <CompletedAction3> as Completed Actions3
		   * I select <Diagnosis> as diagnosis
		   * I enter <Comment> as comment in timer popup
		   * I click on stop timer button
		   * I enter <Time> as time duration
		   * I select <CPTCode> as CPT Code
		   * I click on update button
		Then I should see success message <TimerSucessMessage> of timer
		   * I click on history tab
		   * I should see patient timer <Program> and <CPTCode> and <Diagnosis> historyData

		Examples: 
			| Patient          | TimerSucessMessage                                     | Comment | Diagnosis | Program            | CPTCode | CompletedAction                         | Stat   | Start_Date | Billable | StartDate            | EndDate              | DateTime          | Duration | Time     | CompletedAction2           | CompletedAction3    | CompletedAction4           |
			| Dermo505, Mac505 | successPatient Time has been updated successfully.Hide | Test    | H26.40    | General BHI        |   99492 | Contact with Pt/ Community/ Family/ POA | Active |   12102020 | Yes      | 12192020 01:30:00 AM | 12072021 01:30:00 AM | 03152021 03:20 AM | 10:15:20 | 01:00:00 | Care Plan Updated          | Care Plan Developed | Care Plan Reviewed         |
			| Dermo505, Mac505 | successPatient Time has been updated successfully.Hide | Test    | H26.40    | Initial Psych CoCM |   99484 | Contact with Pt/ Community/ Family/ POA | Active |   12102020 | Yes      | 12192020 01:30:00 AM | 12072021 01:30:00 AM | 03152021 03:20 AM | 10:15:20 | 01:00:00 | Appropriate Psych Consults | Care Plan Developed | Appropriate Psych Consults |
