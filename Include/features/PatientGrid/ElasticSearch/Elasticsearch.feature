@PatientGrid
Feature: Search Patient

	Background: 
		Given I navigate to patient grid

	@ElastciSearch_StartsWith
	Scenario Outline: Elastic Search Using Starts With
		When I select Starts With Option
		 And I search <Patient> using global search
		Then I should see <Patient_Name> as patient with <DOB> as DOB and <Patient_Status> as Status on patient grid

		Examples: 
			| Patient          | Patient_Name     | DOB        | Patient_Status |
			| Dermo505, Mac505 | Dermo505, Mac505 | 02/02/2000 | Active         |
			| Dermo505         | Dermo505, Mac505 | 02/02/2000 | Active         |
			| EntMerging505    | Dermo505, Mac505 | 02/02/2000 | Active         |
			| 02/02/2000       | Dermo505, Mac505 | 02/02/2000 | Active         |
			|      03011448360 | Dermo505, Mac505 | 02/02/2000 | Active         |

	@ElastciSearch_Contains
	Scenario Outline: Elastic Search Using Contains
		When I select Contains Option
		 And I search <Patient> using global search
		Then I should see <Patient_Name> as patient with <DOB> as DOB and <Patient_Status> as Status on patient grid

		Examples: 
			| Patient          | Patient_Name     | DOB        | Patient_Status |
			| Dermo505, Mac505 | Dermo505, Mac505 | 02/02/2000 | Active         |
			| Dermo505         | Dermo505, Mac505 | 02/02/2000 | Active         |
			| Dermo            | Dermo505, Mac505 | 02/02/2000 | Active         |
			| Mac505           | Dermo505, Mac505 | 02/02/2000 | Active         |
			| EntMerging505    | Dermo505, Mac505 | 02/02/2000 | Active         |
			| 02/02/2000       | Dermo505, Mac505 | 02/02/2000 | Active         |
			|      03011448360 | Dermo505, Mac505 | 02/02/2000 | Active         |

	@ElastciSearch_StartsWith_Transition
	Scenario Outline: Elastic Search On Transition Using Starts With
		When I click on care cordination
		 And I select Starts With Option_Transition
		   * I search <Patient> using transition search
		Then I should see <Patient_Name> as patient with <DOB> as DOB and <Patient_Status> as Status on patient grid

		Examples: 
			| Patient          | Patient_Name     | DOB        | Patient_Status |
			| Dermo505, Mac505 | Dermo505, Mac505 | 09/21/1949 | Active         |
			| Dermo505         | Dermo505, Mac505 | 09/21/1949 | Active         |
			| EpisodeTest829   | Dermo505, Mac505 | 09/21/1949 | Active         |
			| 09/21/1949       | Dermo505, Mac505 | 09/21/1949 | Active         |
			|      03011448360 | Dermo505, Mac505 | 09/21/1949 | Active         |

	@ElastciSearch_Contains_Transition
	Scenario Outline: Elastic Search On Transition Using Contains
		When I click on care cordination
		 And I select Contains Option_Transition
		   * I search <Patient> using transition search
		Then I should see <Patient_Name> as patient with <DOB> as DOB and <Patient_Status> as Status on patient grid

		Examples: 
			| Patient          | Patient_Name     | DOB        | Patient_Status |
			| Dermo505, Mac505 | Dermo505, Mac505 | 09/21/1949 | Active         |
			| Dermo505         | Dermo505, Mac505 | 09/21/1949 | Active         |
			| Dermo            | Dermo505, Mac505 | 09/21/1949 | Active         |
			| Mac505           | Dermo505, Mac505 | 09/21/1949 | Active         |
			| EpisodeTest829   | Dermo505, Mac505 | 09/21/1949 | Active         |
			| 09/21/1949       | Dermo505, Mac505 | 09/21/1949 | Active         |
			|      03011448360 | Dermo505, Mac505 | 09/21/1949 | Active         |
