Feature: DFT - Coders Workflow to update problem code

  Background: 
    Given I navigate to patient grid

  @ProgramEnrollmentCoderWorkFlow
  Scenario Outline: DFT - Coders Workflow to update problem code
    When I click on reset button
    * I click on Care Physician field
    * I enter <CarePhysician> as Care Physician
    * I click on apply button
    Then I should see <CarePhysician> as Care Physician in grid
    When I search <Patient> using global search
    And I click on program enrollment dropdown
    * I expand PACM
    * I click on eligible link
    Then I should see popup with correct title
    When I select reason:<Reason>
    And I enter <Conditions> as conditions
    * I enter <Customs_Conditions> as customs condition
    * I select problems:<Problems>
    * I select referral source:<ReferralSource>
    * I click on eligible button
    Then I should see message of success: <dynamicid> for following <message>
    * I click on expanding button on the enrollment history tab
    Then I should see the latest record <status>,<Reason>,<Problems> after expanding the enrollment history
    * I verify patient is selected
    * I click on patient timer button
    Then I should see patient <Patient> as patient_name in timer popup
    * I should see current date
    * I click on start timer button
    * I click on minimize button
    And I click on notes tab
    * I click on plus notes button
    * I should see notes popup
    * I click on Template tab
    * I enter <TemplateName> as templateName
    * I enter <TemplateBody> as TemplateBody
    * I click on save button to save template
    * I click on Add Note tab
    * I select type
    * I click on save button to save patient data
    Then I should see current date of notes form
    And I select noteType:<NoteType>
    * I click on delete button to delete form
    Then I select Delete from the confirmation box
    * I click on plus form
    * I click on Transition Form
    * I should see <Patient> as patient name in form popup
    * I should see transition form title
    * I enter <NotificationDate> as notificationDate
    * I enter <DischargeDate> as discharge Date
    * I enter <TransitionDate> as transitionDate
    * I click on save button to save patient data
    * I should see current date of notes form
    * I click on edit button of forms
    * I should see <Patient> as patient name in form popup
    * I click on complete button to save transition of care form data
    * I click on patient timer button again
    * I click on stop timer button
    * I verify Non_billable checkbox is checked
    * I enter <Comment> as comment in timer popup
    * I click on update button
    Then I should see <ExpectedText> success message
    * I click on history tab
    * I should see patient timer history

    Examples: 
      | NoteType | Patient          | Strtdate          | CarePhysician | Comment | ExpectedText | TemplateBody      | NotificationDate  | DischargeDate     | TransitionDate    | TemplateName | Status      | Desription            | Owner     | Completeddate     | Taskname                            | Reason           | Conditions | Customs_Conditions | Problems                                             | ReferralSource | dynamicid                                                                           | message                                                                                        | UpdatedBy   | status   |
      | All      | Dermo505, Mac505 | 03202021 04:54 AM | Dove, Jane    | Test    | updated      | TemplateBody Text | 03152021 03:20 AM | 03152021 11:45 PM | 03152021 03:20 AM | Template Tab | In Progress | Task creation process | Care Team | 04202021 04:54 AM | Enjoy your life and forget you age! | Patient Eligible | BPH        | Automation Test    | Encounter for screening for cardiovascular disorders | Provider Team  | Program enrollment status successfully changed to Eligible for selected patient(s). | successProgram enrollment status successfully changed to Eligible for selected patient(s).Hide | Amir, Hafiz | Eligible |
