Feature: Care Plan - ADD Allergies

	Background: 
		Given I navigate to patient grid

	@Smoke_USMM
	Scenario Outline: Verify That User Is Able To Add Allergies
		When I search <Patient> using global search
		 And I click on care plan tab
		   * I click on add new care plan button
		   * I click on basedonpatientmedicalrecord
		   * I hover over on encounter
		   * I click on encounter plus button
		   * I select encounter as:<Encounter>
		   * I enter <EncounterValue> as ecounter input
		   * I enter <StartDate> as encounterStartDate
		   * I enter <EndDate> as encounterEndDate
		   * I enter <DischargeDisposition> as discharge disposition
		   * I click on updated button
		   * I enter title <Title>
		   * I click on save and close button
		   * I click on title from care plan grid
		Then I should see patient <Patient> as patient_name
		   * I should see <EncounterValue> and <StartDate> and <EndDate> and <DischargeDisposition> as updated encounter

		Examples: 
			| Patient          | Encounter | SucessMessage                           | EncounterValue | StartDate | Title     | EndDate  | DischargeDisposition |
			| Dermo505, Mac505 | C4        | successCare Plan Saved SuccessfullyHide | 0005F          |  12012020 | Encounter | 12012021 | Home                 |
