Feature: Patient Timer

	Background: 
		Given I navigate to patient grid

	@Regression_Timer_disable
	Scenario Outline: Timer should be Disable for other patient if already running for A Patient
		When I search <Patient> using global search
		   * I click on patient timer button
		Then I should see patient <Patient> as patient_name in timer popup
		   * I click on start timer button
		   * I click on minimize button
		When I click on message tab
		   * I search patient <Patient> in message field
		   * I select searched patient
		   * I click on patient from message box
		Then I should see patient <Patient> as patient_name in timer popup
		   * I click on minimize button
		   * I click on WFB cross button
		When I click on scheduleTab
		 And I click on three dots
		   * I click on view patient record
		Then I should see patient <Patient> as patient_name in timer popup
		When I click on stop timer button
		 And I verify Non_billable checkbox is checked
		   * I click on update button
		Then I should see success message <TimerSucessMessage> of timer

		Examples: 
			| Patient          | Reason    | DateOfService_FromDate | MRN           | Practice         | TimerSucessMessage                                     |
			| Dermo505, Mac505 | Back pain |               12052020 | MRN0000014455 | Support Test Fac | successPatient Time has been updated successfully.Hide |
