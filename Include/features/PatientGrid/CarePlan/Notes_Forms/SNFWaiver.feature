Feature: Care Plan - Notes - Forms

  Background: 
    Given I navigate to patient grid

  Scenario Outline: Verify SNF Waiver
    When I search <Patient> using global search
    And I click on notes tab
    * I click on plus form
    * I click on SNF Waiver
    * I should see <Patient> as patient name in form popup
    * I should see SNFWaiver care form title
    * I enter SNF <DateTime> as datetime
    * I enter SNF <AprimaID> as aprima id
    * I select SNF referral source
    * I enter SNF <ReferralSourceDate> as referral source date
    * I select nurse navigator
    * I enter SNF <ReferralName> as referral name
    * I select VPA office location
    * I click patient attribute to ACO
    * I select patient attribute to ACO
    * I select patient status
    * I enter SNF <LastSeenByVPAProvider> as last seen by VPA provider
    * I select patient patient attributeIAH
    * I enter SNF <AdmittingDiagnosis> as AdmittingDiagnosis
    * I select reason for admission
    * I select SNF state
    * I enter SNF <Facility> as facility
    * I enter SNF <EstimatedLOS> as estimated LOS days
    * I enter SNF <DischargeDate> as discharge date
    * I enter SNF <ActualLOS> as actual LOS days
    * I select discharge disposition SNF
    * I select post SNF checkIn
    * I enter SNF <Comments> as SNF comments
    * I click on save button to save patient data
    Then I should see <ExpectedText> success message
    * I should see current date of notes form
    When I click on notes tab
    And I select noteType:<NoteType>
    * I drag chat list2
    And I click on edit button where priority:<priority> and type:<type>
    * I should see <DateTime> as SNFWaiver
    * I click on save button to save patient data
   
    * I click on delete button to delete form
    Then I select Delete from the confirmation box

    Examples: 
      | Patient          | FormType   | DateTime          | priority | type       | AprimaID | ReferralSourceDate | ReferralName | LastSeenByVPAProvider | AdmissionDate | Facility | EstimatedLOS | DischargeDate | ExpectedText | Comments                                                                   | AdmittingDiagnosis | NoteType   | ActualLOS |
      | Dermo505, Mac505 | SNF Waiver | 03152021 03:20 AM | Low      | SNF Waiver |      122 |           12022020 | Ali          |              12022020 |      12202021 | MHPN     |           10 |      15092020 | saved        | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | A00.0              | SNF Waiver |        20 |
