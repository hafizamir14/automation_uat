Feature: Care Plan - Notes - Forms

  Background: 
    Given I navigate to patient grid

  @CareManagementForm
  Scenario Outline: Add Care Management Form
    When I search <Patient> using global search
    And I click on notes tab
    * I click on plus form
    * I click on Care Management Form
    * I should see <Patient> as patient name in form popup
    * I should see Care Management form title
    * I enter <DateTime> in care management form
    * I select privacy:<privacy>
    * I select priority:<priority>
    * I select practice
    * I enter provider name:<name>
    * I select relates eo behavioral health
    * I select modality of patient contact
    * I select updated payer
    * I select outreach:<outreach>
    * I enter billing code:<BillingCode>
    * I enter duration of patient contact:<time>
    * I enter the name with whom assessment interaction was completed with:<name>
    * I enter Date and Time and and Duration of each contact attempted contact:<Durationofeachcontactattemptedcontact>
    * I enter active diagnosis:<ActiveDiagnosis>
    * I enter Nature of Discussion and Pertinent Details:<NatureofDiscussionandPertinentDetails>
    * I enter Status of individualized short and longterm goalscare plans including anticipated interventions target dates and followup time frames:<Statusofindividualizedshort>
    * I enter Perceived barriers to treatment plan and activities to overcome barriers:<Perceivedbarrierstotreatmentplanandactivitiestoovercomebarriers>
    * I enter Concerns gaps in care other important details:<ConcernGaps>
    * I select whether the participants are in agreement with the plan
    * I select whether the medication reconciliation is completed
    * I click on save button to save patient data
    Then I should see <ExpectedText> success message
    When I select noteType:<NoteType>
    And I drag chat list2
    * I click on edit button where priority:<priority> and type:<type>
    Then I should see <Patient> as patient name in form popup
    * I click on save button to save patient data
    * I click on delete button where priority:<priority> and type:<type>
    * I select Delete from the confirmation box

    Examples: 
      | type                 | NoteType             | ConcernGaps | Perceivedbarrierstotreatmentplanandactivitiestoovercomebarriers | Statusofindividualizedshort    | NatureofDiscussionandPertinentDetails | Patient          | DateTime          | privacy | priority | outreach        | time   | name        | ExpectedText | BillingCode | Durationofeachcontactattemptedcontact | ActiveDiagnosis |
      | Care Management Form | Care Management Form | ConcernGaps | Perceived Barriers To Treatment Plan                            | Status Of Individualized Short | Nature Of Discussion                  | Dermo505, Mac505 | 03152021 03:20 AM | Private | Low      | Initial Contact | 15 min | Amir, Hafiz | saved        |       98966 |                                    12 | A00.9           |
  