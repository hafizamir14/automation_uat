Feature: Care Plan - Notes - Forms

  Background: 
    Given I navigate to patient grid

  Scenario Outline: Verify Transition Of Care Form - Hospital Discharge Calls
    When I search <Patient> using global search
    And I click on notes tab
    * I click on plus form
    * I click on hospital discharge Call
    * I should see <Patient> as patient name in form popup
    * I should see transition of hospital discharge care form title
    * I enter <DateTime> as date of service date and time
    * I select Hospital Discharge Calls privacy:<privacy>
    * I select Hospital Discharge Calls priority:<priority>
    * I select practice
    * I enter provider name:<name>
    * I select updated payer
    * I enter <ProviderName> as Provider Name and Contact
    * I enter <Attempt1_DateTime> as Attempt1_DateTime
    * I enter <Attempt1_BY> as Attempt1_BY
    * I select Attempt1_DateTime Dropdown:<Attempt1_Dropdown>
    * I enter <Attempt2_DateTime> as Attempt2_DateTime
    * I enter <Attempt2_BY> as Attempt2_BY
    * I select Attempt2_DateTime Dropdown:<Attempt2_Dropdown>
    * I select Attempted twice without success:<AttemptedTwiceWithoutSuccess>
    * I select contact method:<ContactMethod>
    * I select Discuss With:<DiscussWith>
    * I select DischargeValue:<DischargeValue>
    * I enter <DischargeDiagnosis> as DischargeDiagnosis
    * I enter <WhatLeadYouToBeing> as What lead you to being in the hospital
    * I enter <HaveYourSymptomsImproved> as Have your symptoms improved worsened or stayed the same since returning home
    * I select AreYouAbleToTakeCareYourselftAtHome:<Value>
    * I select Doyouhavesomeonetotakecareofyourmedicalneeds:<Value>
    * I select Doyouhaveanyquestionsaboutyourdischargeinstructions:<Value>
    * I select Medicationsreconciledthisencounter:<Value>
    * I select Anynewmedicationsprescribedatdischargeorchangestohomemedications:<Value>
    * I select Anynormalhomemedicationsdiscontinuedatdischarge:<Value>
    * I select Areyoutakingyourmedicationsasprescribed:<Value>
    * I select Doyouhaveanyconcernsaboutyourhealthatthistime:<Value>
    * I select AreYouScheduledForAnyFollowUpTestsTherapiesMedicalEquipmentOrHomeCare:<Value>
    * I select referralsNeeded:<Value>
    * I select referralsNeeded_SubField:<referralsNeeded_SubField>
    * I enter <AppointmentDate> as followUpAppointmentDate
        * I select Is there anything that might prevent you from keeping this appointment:<KeepingThisAppointment>
        * I enter <KeepingThisAppointmentField> as Is there anything that might prevent you from keeping this appointment
    
    * I enter <whatSymptomsWouldPromptYouToCallYourPhysicianBeforeYourNextScheduledAppointment> as whatSymptomsWouldPromptYouToCallYourPhysicianBeforeYourNextScheduledAppointment
    * I enter <whenDidYouLastDiscussYourAdvancedMedicalDirectivesWithYourPhysician> as whenDidYouLastDiscussYourAdvancedMedicalDirectivesWithYourPhysician
    * I select Social Determinants of Health Screening Completed:<SocialDetermination>
    * I select Any barriers identified:<AnyBarriersIdentified>
    * I enter <AnyBarriersIdentifiedField> as any barriers
    * I enter <Notified> as notified
    * I enter <NotifiedTitle> as Notified Title
    * I enter <NotifiedDateCompleted> as notified date complete
    * I enter <NotifiedSignature> as Notified Signature
    * I enter <NotifiedSignatureCompleteDate> as notified signature date complete
    * I click on save button to save patient data
    Then I should see <ExpectedText> success message
    When I select noteType:<NoteType>
    And I drag chat list2
    * I click on edit button where priority:<priority> and type:<type>
    * I click on save button to save patient data
    * I click on delete button where priority:<priority> and type:<type>
    * I select Delete from the confirmation box

    Examples: 
      | DischargeValue | referralsNeeded_SubField | whenDidYouLastDiscussYourAdvancedMedicalDirectivesWithYourPhysician | whatSymptomsWouldPromptYouToCallYourPhysicianBeforeYourNextScheduledAppointment | HaveYourSymptomsImproved         | WhatLeadYouToBeing          | NoteType                 | ExpectedText | type                     | NotifiedSignatureCompleteDate | NotifiedSignature  | NotifiedDateCompleted | NotifiedTitle  | Notified | AppointmentDate   | DidYouContactYourPCPPriorEmergencyRoomVisit_SubField | Value | Attempt2_BY | Attempt1_BY | DischargeDiagnosis | name        | Patient          | EmergencyRommTreatment   | SymptomsLeadingToEmergency         | DateTime          | DateOfDischarge | privacy | priority | ProviderName | ContactMethod | DiscussWith | module | Title            | FaceToFaceName | Signature | Attempt1_DateTime | Attempt2_DateTime | DischargeLocation | HowAreYouFeeling | AdditionalRecords | DischargeDate | Attempt1_Dropdown | Attempt2_Dropdown | AttemptedTwiceWithoutSuccess | WasLetterMailed | DischargeLocation | HowAreYouFeelingSinceDischarge | HaveYouFilledYourPrescriptions | DoYouHaveFollowUpAppointmentScheduled | KeepingThisAppointment | KeepingThisAppointmentField | DoYouHaveAnyOtherQuestion | DoYouHaveAnyOtherQuestionField | SocialDetermination | AnyBarriersIdentified | AnyBarriersIdentifiedField | AnyFoundConcerns |  |
      | Observation    | Home Health Care         | Advance Medical Text                                                | Symptoms Text                                                                   | Have Your Symptoms Improved Text | What Lead You To Being Text | Hospital Discharge Calls | saved        | Hospital Discharge Calls | 03152021 03:20 AM             | Notified Signature | 03152021 03:20 AM     | Notified Title | Amir     | 03152021 03:20 AM | Go to Urgent Care                                    | Yes   | Mehmood     | Amir        |                 12 | Amir, Hafiz | Dermo505, Mac505 | Emergency Treatment Text | Symptoms Leading To Emergency Text | 03152021 03:20 AM |        03152021 | Private | Low      | Amir         | PHONE         | Patient     | adding | Transition Title | Zohaib         | amir      | 03152021 03:20 AM | 03152021 03:20 AM | Ascension         | Much better..!!  | Hospital          |      03152021 | Successful        | Successful        | Yes                          | Yes             | Ascension         | Much better..!!                | Yes                            | Yes                                   | Yes                    | Keeping this appointment    | Yes                       | Do you have any question? No   | Yes                 | Yes                   | Barriers                   | Yes              |  |
