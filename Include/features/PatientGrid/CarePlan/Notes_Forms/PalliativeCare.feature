Feature: Care Plan - Notes - Forms

  Background: 
    Given I navigate to patient grid

  Scenario Outline: Verify PalliativeCare
    When I search <Patient> using global search
    And I click on notes tab
    * I click on plus form
    * I click on palliative care
    * I should see <Patient> as patient name in form popup
    * I should see palliative care form title
    * I select privacy:<privacy>
    * I select PalliativePriority:<priority>
    * I enter <DateTime> as date and time
    * I enter <DateOfDischarge> as date of discharge
    * I select payer
    * I enter <PayerReferralDate> as Payer Referral Date
    * I enter <MedicareID> as Medicare ID
    * I enter <MedicaidID> as Medicaid ID
    * I enter <MarketplaceID> as Marketplace ID
    * I enter <InsuranceID> as Insurance ID
    * I select patient type
    * I select contracted patient
    * I select vpa office location
    * I enter <VPAProvider> as VPAProvider
    * I enter <HealthPlanCareManager> as health plan care manager
    * I enter <PalliativeCareNursePractitioner> as palliative care nurse practitioner
    * I enter <Diagnoses> as diagnoses
    * I select referral source
    * I select status
    * I enter <MMSS> as mmss
    * I enter <NPPalliativeCareAssessmentDate> as NP palliative care assessment date
    * I enter <PalliativeCareStartDate> as palliative care start date
    * I enter <NP3rdVisitDate> as NP third visit date
    * I enter <MostrecentNPPalliativeCareVisitDate> as most recent NP palliative care visit date
    * I enter <AdvanceCarePlanningDate> as advance care planning date
    * I enter <IDGDate> as IDG date
    * I select Inpatient admit or ER visit while on Palliative Care Services in the last thirty days
    * I enter <Date_of_Inpatient_admit_or_ER_visit_while_on_PalliativeCareservices_within_the_last30days> as Date of Inpatient admit or ER visit while on Palliative Care services within the last thirty days
    * I enter <ChaplinReferralDate> as chaplin referral date
    * I enter <ChaplinAssessmentDate> as chaplin assessment date
    * I enter <MostrecentChaplinVisitDate> as Most recent Chaplin Visit Date
    * I enter <SocialWorkReferralDate> as social work referral date
    * I enter <SocialWorkAssessmentDate> as social work assessment date
    * I enter <MostRecentSocialWorkVisitDate> as most recent social work visit date
    * I enter <ReferralDateHomeHealthCare> as Referral for Date Home Health Care
    * I enter <HomeHealthCareAdmitDate> as home health care admit date
    * I enter <HomeHealthCareCompany> as home health care company
    * I enter <HospiceReferralDate> as hospice referral date
    * I enter <HospiceAdmitDate> as hospice admit date
    * I enter <HospiceCompany> as hospice company
    * I enter <CallOutreachPlacedby> as call outreach Placed by
    * I enter <CallDate> as call date
    * I select call1 outcome
    * I enter <CallOutreachPlacedby> as call2 outreach Placed by
    * I enter <CallDate> as call2 date
    * I select call2 outcome
    * I enter <CallOutreachPlacedby> as call3 outreach Placed by
    * I enter <CallDate> as call3 date
    * I select call3 outcome
    * I enter <MostRecentMonthlyTelephoneCall> as MostRecentMonthlyTelephoneCall
    * I select hospitallization or death while on palliative care services
    * I enter <DateOfDeath> as date of death
    * I enter <Comments> as palliative comments
    * I click on save button to save patient data
    Then I should see <ExpectedText> success message
    * I should see <DateTime> as palliativeCare
    When I click on notes tab
    And I select noteType:<NoteType>
    * I drag chat list2
    And I click on edit button where priority:<priority> and type:<type>
    * I should see <DateTime> as palliativeCare
    * I click on save button to save patient data
   
    And I click on delete button where priority:<priority> and type:<type>
    * I select Delete from the confirmation box

    Examples: 
      |privacy| DateOfDeath | MostRecentMonthlyTelephoneCall | NoteType        | MMSS        | VPAProvider | Patient          | DateTime          | FormType        | ExpectedText | priority | type            | DateOfDischarge | PayerReferralDate | MedicareID | MedicaidID | MarketplaceID | InsuranceID | HealthPlanCareManager | PalliativeCareNursePractitioner | Diagnoses                         | NPPalliativeCareAssessmentDate | PalliativeCareStartDate | NP3rdVisitDate | MostrecentNPPalliativeCareVisitDate | AdvanceCarePlanningDate | IDGDate  | Date_of_Inpatient_admit_or_ER_visit_while_on_PalliativeCareservices_within_the_last30days | ChaplinReferralDate | ChaplinAssessmentDate | MostrecentChaplinVisitDate | SocialWorkReferralDate | SocialWorkAssessmentDate | MostRecentSocialWorkVisitDate | HospiceReferralDate | HospiceAdmitDate | HospiceCompany | CallOutreachPlacedby | CallDate | Comments                                                                   | ReferralDateHomeHealthCare | HomeHealthCareAdmitDate | HomeHealthCareCompany |
      |Private|    03152002 |                       03152021 | Palliative Care | Amir, Hafiz | Amir, Hafiz | Dermo505, Mac505 | 03152021 03:20 AM | Palliative Care | saved        | Low      | Palliative Care |        03152021 |          12032021 |         12 |         15 |            20 |          30 | Mehmood Anjum         | Amir, Hafiz                     | Cholera due to Vibrio cholerae 01 |                       01032020 |                05032021 |       04032020 |                            05032020 |                06032020 | 07032020 |                                                                                  08032020 |            05022020 |              06022020 |                   07022020 |               08022020 |                 09022020 |                      01022020 |            02022020 |         03022021 | Soliton        | Amir                 | 02092020 | Lorem Ipsum is simply dummy text of the printing and typesetting industry. |                   03022021 |                03022021 | Health Care Company   |
