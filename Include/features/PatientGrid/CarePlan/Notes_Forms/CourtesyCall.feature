Feature: Care Plan - Notes - Forms

  Background: 
    Given I navigate to patient grid

  Scenario Outline: Verify Transition Of Care Form - Courtesy Calls
    When I search <Patient> using global search
    And I click on notes tab
    * I click on plus form
    * I click on Courtesy Call
    * I should see <Patient> as patient name in form popup
    * I should see transition of care form title
    * I enter <DateTime> as date of service date and time
    * I select Coutesy Call privacy:<privacy>
    * I select Coutesy Call priority:<priority>
    * I select practice
    * I select updated payer
    * I enter <ProviderName> as Provider Name and Contact
    * I enter <DischargeDate> as dischargeDate
    * I enter <Attempt1_DateTime> as Attempt1_DateTime
    * I select Attempt1_DateTime Dropdown:<Attempt1_Dropdown>
    * I enter <Attempt2_DateTime> as Attempt2_DateTime
    * I select Attempt2_DateTime Dropdown:<Attempt2_Dropdown>
    * I select Attempted twice without success:<AttemptedTwiceWithoutSuccess>
    * I select contact method:<ContactMethod>
    * I select Discuss With:<DiscussWith>
    * I select Discharge Location:<DischargeLocation>
    * I enter <HowAreYouFeelingSinceDischarge> as How are you feeling since discharge
    * I select Have you filled your prescriptions:<HaveYouFilledYourPrescriptions>
    * I select Do you have a follow up appointment scheduled:<DoYouHaveFollowUpAppointmentScheduled>
    * I select Is there anything that might prevent you from keeping this appointment:<KeepingThisAppointment>
    * I enter <KeepingThisAppointmentField> as Is there anything that might prevent you from keeping this appointment
    * I select Do you have any other questions or concerns at this time:<DoYouHaveAnyOtherQuestion>
    * I enter <DoYouHaveAnyOtherQuestionField> as Do you have any other questions or concerns at this time
    * I select Social Determinants of Health Screening Completed:<SocialDetermination>
    * I select Any barriers identified:<AnyBarriersIdentified>
    * I enter <AnyBarriersIdentifiedField> as any barriers
    * I select Any found concerns or potential barriers reported to primary care physicians office:<AnyFoundConcerns>
    * I enter <Notified> as notified
    * I enter <NotifiedTitle> as Notified Title
    * I enter <NotifiedDateCompleted> as notified date complete
    * I enter <NotifiedSignature> as Notified Signature
    * I enter <NotifiedSignatureCompleteDate> as notified signature date complete
    * I click on save button to save patient data
    Then I should see <ExpectedText> success message
    When I select noteType:<NoteType>
    And I drag chat list2
    * I click on edit button where priority:<priority> and type:<type>
    * I click on save button to save patient data
    * I click on delete button where priority:<priority> and type:<type>
    * I select Delete from the confirmation box

    #		When I click on analytics tab
    #		   * I click on form option
    #		   * I click on analytics reset button
    #		   * I enter analytics from date
    #		   * I select form type:<FormType>
    Examples: 
      | type          | NoteType       | NotifiedSignatureCompleteDate | NotifiedSignature  | NotifiedDateCompleted | NotifiedTitle  | Notified | ExpectedText | Patient          | ProviderName | FormType   | DateTime          | DateOfDischarge | privacy | priority | module | ContactMethod | DiscussWith | NotifiedName  | Title            | FaceToFaceName | Signature | Attempt1_DateTime | Attempt2_DateTime | DischargeLocation | HowAreYouFeeling | AdditionalRecords | DischargeDate | Attempt1_Dropdown | Attempt2_Dropdown | AttemptedTwiceWithoutSuccess | WasLetterMailed | DischargeLocation | HowAreYouFeelingSinceDischarge | HaveYouFilledYourPrescriptions | DoYouHaveFollowUpAppointmentScheduled | AppointmentDate   | KeepingThisAppointment | KeepingThisAppointmentField | DoYouHaveAnyOtherQuestion | DoYouHaveAnyOtherQuestionField | SocialDetermination | AnyBarriersIdentified | AnyBarriersIdentifiedField | AnyFoundConcerns |
      | Courtesy Call | Courtesy Calls | 03152021 03:20 AM             | Notified Signature | 03152021 03:20 AM     | Notified Title | Amir     | saved        | Dermo505, Mac505 | Amir         | SNF Waiver | 03152021 03:20 AM |        03152021 | Private | Low      | adding | PHONE         | Patient     | Mehmood Anjum | Transition Title | Zohaib         | amir      | 03152021 03:20 AM | 03152021 03:20 AM | Ascension         | Much better..!!  | Hospital          |      03152021 | Successful        | Successful        | Yes                          | Yes             | Ascension         | Much better..!!                | Yes                            | Yes                                   | 03152021 03:20 AM | Yes                    | Keeping this appointment    | Yes                       | Do you have any question? No   | Yes                 | Yes                   | Barriers                   | Yes              |
