Feature: Care Plan - BOPMR

  Background: 
    Given I navigate to patient grid

  @Smoke_USMM
  Scenario Outline: Based On Patient Medical Report
    When I search <Patient> using global search
    And I click on patient from grid
    * I save dateformat from PWB
    * I click on care plan tab2
    * I click on add new care plan button
    * I click on basedonpatientmedicalrecord
    Then I verified date format is as expected
    

    Examples: 
      | Patient          | 
      | Dermo505, Mac505 | 
      
   