Feature: Soap Note - Add Immunizations

  Background: 
    Given I navigate to CMR_Schedule

  @Smoke_USMM_Immunizations
  Scenario Outline: Verify Creating Immunizations
    Then I should see already scheduled appointment
    When I click on three dots
    * I click on edit soapnotes
    Then I should see <Patient> as patient
    When I click on add Immunization plus button
    And I enter <ImmunizationVaccine> as the immunization Vaccine
    * I enter <Immunization_Date> as immunization date
    * I enter <Immunization_Comment> as immunizationComment
    * I enter <Immunization_LotNumber> as immunization lotNumber
    * I enter <Immunization_ManufacturerNumber> as immunization manufacturerNumber
    * I click on saveclose button to save immunizations
    Then I should see <ImmunizationVaccine> and <Immunization_Date> and <Immunization_Comment> and <Immunization_LotNumber> and <Immunization_ManufacturerNumber> as Immunization data in soap note grid
    When I click on edit Immunizations button
    * I enter <Immunization_CommentEdit> as immunizationCommentEdit
    * I click on saveclose button to save immunizations
    Then I should see <ImmunizationVaccine> and <Immunization_CommentEdit> and <Immunization_LotNumber> and <Immunization_ManufacturerNumber> as Immunization Edited data in soap note grid
    * I click on delete immunizations button

    Examples: 
      | ImmunizationVaccine | Immunization_Comment  | Immunization_Date | Immunization_LotNumber | Immunization_ManufacturerNumber | Patient          | Immunization_CommentEdit |
      | IPV::10             | Immunizations Testing |          04162021 |                      2 |                               2 | Dermo505, Mac505 | Immunization CommentEdit |
