Feature: Soap Note Creation from Schedule flow

  Background: 
    Given I navigate to CMR_Schedule

  @Smoke_USMM_CreateLabResults
  Scenario Outline: Verify Creating Lab Results - Based On Patient's Medical Record
    Then I should see already scheduled appointment
    When I click on three dots
    * I click on edit soapnotes
    * I click on add Lab Results plus button
    And I enter <LabResults> as lab results in search and select analyte
    * I click on lab resutls OK button
    Then I should see <LabResults> as Lab Resultss data in soap note popup
    When I click on SaveAll button to save Lab Results
        When I click on Save button to save SOAP NOTE
    Then I should see soap note saved message
    * I click on SaveClose button to save SOAP NOTE
    And I should see soap note saved message
    * I click on three dots
    * I should see Edit Soap Note option
    Then I should see <LabResults> as Lab Resultss data in soap note popup
    When I click on delete lab results button
    * I click on SaveAll button to save Lab Results

    Examples: 
      | LabResults | Patient          |
      | A1c        | Dermo505, Mac505 |
