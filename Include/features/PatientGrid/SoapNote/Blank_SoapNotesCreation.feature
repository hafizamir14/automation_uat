Feature: Soap Note Creation from Schedule flow

  Background: 
    Given I navigate to CMR_Schedule

  @Smoke_USMM_CreateSN_Blank
  Scenario Outline: Verify Creating Soap Notes - Blank
    When I double click on screen to add appointment
    Then I should see schedule appointment popup
    When I enter <Patient> as appointment patient
    And I enter <Reason> as appointment reason
    * I should see <Patient> as actual patient name
    * I drag chat list
    * I click create button to save appointment
    * I click on yes button
    * I click on proceed button to appointment
    Then I should see appointment success message
    And I click on three dots
    * I hover over on create soapnotes
    * I click on blankSoapNote
    Then I should see <Patient> as patient
    When I click on add HRA plus button
    * I enter <ThePatientUnderstands> as the patient understands
    * I enter <LocationOfVisit> as location of visit
    * I enter <Member_Verbally_Acknowledged> as Member verbally acknowledge
    * I enter <PCName> as pc name
    * I enter <PC_PhoneNumber> as pc phone number
    * I enter <PatientCareTeam_Notes> as patient care team
    * I click on Review Of System Tab
    * I enter <Temp> as temprature
    * I enter <Pulse> as pulse
    * I enter <Repeat_Pulse> as repeat pulse
    * I enter <Resp_Rate> as resp rate
    * I enter <Pulse_Ox> as pulse ox
    * I enter <Repeat_Pulse_Ox> as repeat pulse ox
    * I select Have you ever been told you have problems with sight
    * I select Do you have any of the following diagnosis
    * I select Diagnosis
    * I click on Exam and Recomendation Tab
    * I enter <Constitutional> as constitutional
    * I click on save button to save assessment
    Then I should see assessment saved successfully message
    * I click on cross icon to close the popup
    Then I should see added assessment data in soap note popup

    Examples: 
      | ThePatientUnderstands                                                             | LocationOfVisit | PCName | PC_PhoneNumber | PatientCareTeam_Notes                                                             | Temp | Pulse | Repeat_Pulse | Resp_Rate | Pulse_Ox | Repeat_Pulse_Ox | Constitutional       | Member_Verbally_Acknowledged | Patient          | Reason    |
      | I am not assuming any responsibility for their care of providing direct treatment | Home            | Amir   |   032545235210 | I am not assuming any responsibility for their care of providing direct treatment |  102 |    50 |           60 |        20 |      100 |              85 | In no acute distress | Yes                          | Dermo505, Mac505 | Back pain |
