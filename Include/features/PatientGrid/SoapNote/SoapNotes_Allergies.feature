Feature: Soap Note - Add Allergies

  Background: 
    Given I navigate to CMR_Schedule

  @ScheduleAppointment
  Scenario Outline: Verify Creating Schedule Appointment Using Soap Notes
    And I double click on screen to add appointment
    Then I should see schedule appointment popup
    When I enter <Patient> as appointment patient
    And I enter <Reason> as appointment reason
    * I should see <Patient> as actual patient name
    * I drag chat list
    * I click create button to save appointment
    * I click on yes button
    * I click on proceed button to appointment
    Then I should see appointment success message
    And I click on three dots
    * I hover over on create soapnotes
    * I click on blankSoapNote

    Examples: 
      | Patient          | Reason    |
      | Dermo505, Mac505 | Back pain |

  @Smoke_USMM_Allergies
  Scenario Outline: Verify Creating Allergies
    When I click on three dots
    And I click on edit soapnotes
    Then I should see <Patient> as patient
    When I select Source as:<Source>
    And I enter <Encounter> as encountercode
    * I verified billable checkbox as checked
    * I click on add Allergies plus button
    * I enter <Allergy> as allergyCode
    * I select severity:<Severity>
    * I enter <Allergy_Start_Date> as allergy_StartDate
    * I enter <Allergy_End_Date> as allergy_EndDate
    * I click on saveclose button to save allergy
    Then I should see <Allergy> and <Allergy_Start_Date> and <Allergy_End_Date> as allergy data in soap note grid
    When I click on edit allergies button
    * I enter <allergyComment> as allergyComment
    * I click on saveclose button to save allergy
    Then I should see <Allergy> and <Allergy_Start_Date> and <Allergy_End_Date> as allergy data in soap note grid
    Then I should see <allergyComment> as allergy comment in grid
    * I click on delete allergies button
    * I click on deleteConfirm allergies button

    Examples: 
      | Allergy | Patient          | Allergy_Start_Date | Allergy_End_Date | Encounter | Source | Severity | allergyComment |
      |   10323 | Dermo505, Mac505 |           04162020 |         04162021 |     99344 | C4     | Mild     | Edit Comment   |

  @DeleteAppointment
  Scenario: Verify deleting Scheduled Appointment
    Then I should see already scheduled appointment
    When I click on three dots
    And I click on delete appointment
    And I should see delete appointment message
