Feature: Soap Note - Add Diagnosis

  Background: 
    Given I navigate to CMR_Schedule

  @Smoke_USMM_CreateDiagnosis
  Scenario Outline: Verify Creating Diagnosis
    Then I should see already scheduled appointment
    When I click on three dots
    * I click on edit soapnotes
    Then I should see <Patient> as patient
    When I click on add diagnosis plus button
    And I enter <Diagnosis> as diagnosis
    * I select diagnosisseverity:<DiagnosisSeverity>
    * I enter <Diagnosis_StartDate> as diagnosis startDate
    * I enter <Diagnosis_EndDate> as diagnosis endDate
    * I enter <Diagnosis_Comment> as diagnosis_comment
    * I click on diagnosisSaveClose button
    Then I should see <Diagnosis> and <Diagnosis_Comment> as Diagnosis data in soap note grid
    When I click on edit Diagnosis button
    * I enter <Diagnosis_CommentEdit> as Diagnosis_CommentEdit
    * I click on diagnosisSaveClose button
    Then I should see <Diagnosis_CommentEdit> as Diagnosis Edited data in soap note grid
    * I click on delete Diagnosis button

    Examples: 
      | Diagnosis                                  | DiagnosisSeverity | Diagnosis_StartDate | Diagnosis_EndDate | Patient          | Diagnosis_Comment | Diagnosis_CommentEdit  |
      | Basal cell carcinoma of anal skin::C44.510 | Mild              |            04162020 |          04162021 | Dermo505, Mac505 | Diagnosis Comment | Diagnosis Comment Edit |
