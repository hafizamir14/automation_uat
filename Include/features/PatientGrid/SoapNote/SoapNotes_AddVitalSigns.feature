Feature: Soap Note - Add Vital Signs

  Background: 
    Given I navigate to CMR_Schedule

  @Smoke_USMM_AddVitalSigns
  Scenario Outline: Verify Creating Vital Signs
    Then I should see already scheduled appointment
    When I click on three dots
    * I click on edit soapnotes
    Then I should see <Patient> as patient
    When I click on add vital sign plus button
    And I enter <TEMP> as Vital TEMP
    * I enter <PULSE> as Vital PULSE
    * I enter <RESP_RATE> as Vital RESP_RATE
    * I enter <WEIGHT> as Vital WEIGHT
    * I enter <HEIGHT> as Vital HEIGHT
    * I enter <BMI> as Vital BMI
    * I enter <BSA> as Vital BSA
    * I click on saveclose button to save VitalSign
    Then I should see <TEMP> and <PULSE> and <RESP_RATE> and <WEIGHT> and <HEIGHT> as Vital Signs data in soap note grid
    When I click on edit Vital button
    * I enter <Sys> and <Dia> as blood pressure
    * I click on saveclose button to save VitalSign
    Then I should see <TEMP> and <PULSE> and <RESP_RATE> and <Sys> and <Dia> and <WEIGHT> and <HEIGHT> as Vital Signs Edited data in soap note grid
    * I click on delete Vital button

    Examples: 
      | TEMP | PULSE | RESP_RATE | WEIGHT | Patient          | HEIGHT | BMI | BSA | Sys | Dia |
      |  100 |    50 |        20 |     70 | Dermo505, Mac505 |     20 |  10 |   3 |  80 | 120 |
