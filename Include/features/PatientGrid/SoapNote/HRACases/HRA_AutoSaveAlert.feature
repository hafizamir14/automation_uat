Feature: Soap Note - HRA_Auto Save Alert

  Background: 
    Given I navigate to CMR_Schedule

  @Smoke_USMM_AutoSave_HRA
  Scenario Outline: Soap Note - HRA_Auto Save Alert
    And I double click on screen to add appointment
    Then I should see schedule appointment popup
    When I enter <Patient> as appointment patient
    And I enter <Reason> as appointment reason
    * I should see <Patient> as actual patient name
    * I drag chat list
    * I click create button to save appointment
    * I click on yes button
    * I click on proceed button to appointment
    Then I should see appointment success message
    And I click on three dots
    * I hover over on create soapnotes
    * I click on blankSoapNote
    * I click on add HRA plus button
    * I enter <LocationOfVisit1> as location of visit1
    * I enter <Member_Verbally_Acknowledged> as Member verbally acknowledge
    * I click on cross icon to close the popup_HRA
    Then I should see <Code1> in Billing Information1
    * I click on SaveClose button to save SOAP NOTE
    When I click on three dots
    And I click on delete appointment

    Examples: 
      | Patient          | Reason    | LocationOfVisit1 | Code1 | LocationOfVisit2                 | Code2 | PADTestToday | ReasonTest | LeftResults | RightResults | DiagnosisCode | Facility          | Constitutional       | MRN           | Soap_Status | AddendumNotes | Member_Verbally_Acknowledged | Billing_Status |
      | Dermo505, Mac505 | Back pain | Home             | visit | Telehealth utilizing audio/video | visit | Yes          | Diabetes   | 0.99-0.90   | 0.89-0.60    | Z13.6         | VPA PC WEST ALLIS | In no acute distress | MRN0000014455 | Signed      | Addendum Test | Yes                          | Signed         |
