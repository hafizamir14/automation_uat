Feature: Soap Note Creation from Schedule flow

	Background: 
		Given I navigate to CMR_Schedule

	@Smoke_USMM_CreateScheduleAppointment_HRA
	Scenario Outline: HRA Soap Notes New Flows
		When I click on three dots
		 And I click on edit soapnotes
		   * I click on add HRA plus button
		   * I enter <LocationOfVisit1> as location of visit1
		   * I enter <Member_Verbally_Acknowledged> as Member verbally acknowledge
		   * I click on save button to save assessment
		   * I click on cross icon to close the popup
		Then I should see <Code1> in Billing Information1
		When I click on add HRA plus button
		 And I enter <LocationOfVisit2> as location of visit2
		   * I click on save button to save assessment
		   * I click on cross icon to close the popup
		Then I should see <Code2> in Billing Information2
		When I click on add HRA plus button
		 And I click on Exam and Recomendation Tab
		   * I enter <PADTestToday> as pad test today
		   * I enter <ReasonTest> as reason or conditon test
		   * I enter <LeftResults> as left results
		   * I enter <RightResults> as right result
		   * I click on save button to save assessment
		Then I should see assessment saved successfully message
		   * I click on cross icon to close the popup
		   * I should see <ReasonTest> in Recommended Lab section
		When I click on add HRA plus button
		 And I click on Exam and Recomendation Tab
		   * I select on Referral required
		   * I select urgency status as referral
		   * I enter reason of referralreason
		   * I click on save button to save assessment
		Then I should see assessment saved successfully message
		   * I click on cross icon to close the popup
		   * I enter <Facility> to add signature for sign button
		   * I select Rendering Provider
		When I click on Sign button to sign soapnotes
		   * I click on Create Signature button
		   * I set signature
		   * I click on submit signature button
		Then I should see sign saved message
		When I click on Addendum button
		   * I enter <AddendumNotes> as addendumNotes
		   * I click on addendum save button to save addendum
		Then I should see <AddendumNotes> as addendum in addendumsection
		   * I click on close button
		   * I should see signed status icon on appointment ui page
		When I navigate to supper bill tab
		   * I click on Reset button to reset supper bill filters
		When I click on referral status three lines
		Then I should see case management popup
		   * I should see case management popup data is showing
		   * I click on Done button
		   * I select <Soap_Status> as Signed status from soap status
		   * I click on Apply button to apply supper bill filters
		   * I open Patient using <MRN> on superbill screen
		Then I should see <MRN> on superbill grid
		When I click on schedule tab
		   * I click on three dots
		   * I click on view soap note
		   * I click on Unsign button
		   * I click on yes button to unsign

		Examples: 
			| Patient          | Reason    | LocationOfVisit1 | Code1 | LocationOfVisit2                 | Code2 | PADTestToday | ReasonTest | LeftResults | RightResults | DiagnosisCode | Facility          | Constitutional       | MRN           | Soap_Status | AddendumNotes | Member_Verbally_Acknowledged | Billing_Status |
			| Dermo505, Mac505 | Back pain | Home             | visit | Telehealth utilizing audio/video | visit | Yes          | Diabetes   | 0.99-0.90   | 0.89-0.60    | Z13.6         | VPA PC WEST ALLIS | In no acute distress | MRN0000014455 | Signed      | Addendum Test | Yes                          | Signed         |
