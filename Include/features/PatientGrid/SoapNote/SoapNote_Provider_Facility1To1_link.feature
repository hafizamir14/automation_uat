Feature: Soap Note - Provider and Facility 1-1 Link Case

	Background: 
		Given I navigate to CMR_Schedule

	@SmokeUSMM_Rendering_Facility
	Scenario Outline: Verify Rendering TO Facility 1-1 Link Cases Using Soap Notes
		When I click on three dots
		 And I click on edit soapnotes
		   * I select Render Provider
		Then I verify <Facility> as provider facility

		Examples: 
			| Facility          |
			| VPA PC WEST ALLIS |

	@SmokeUSMM_Facility_Rendering
	Scenario Outline: Verify Facility To Rendering 1-1 Link Cases Using Soap Notes
		When I click on three dots
		 And I click on edit soapnotes
		   * I enter <Facility> to add signature for sign button
		Then I verify <Rendering> Rending Provider

		Examples: 
			| Facility          | Rendering   |
			| VPA PC WEST ALLIS | Amir, Hafiz |
