Feature: DFT - Coders To Provider Workflow to update problem code

  Background: 
    Given I navigate to patient grid

  @ProgramEnrollmentCoderWorkFlow
  Scenario Outline: DFT - Coder To Provider Workflow to update problem code
    When I search <Patient> using global search
    When I click on Task tab
    And I click on create task button
    * I should see Create New Task form with title
    * I enter <Taskname> as task field
    * I enter <Strtdate> as task start date field
    * I enter <Completeddate> as task completed on date field
    * I enter <Status> as task status field
    * I enter <Owner> as owner field
    * I enter <Assigned> as assigned to
    * I click on save button to save task
    * I click on logout button
    * I enter Second_login credentials
    And I click on Task main tab
    * I click on my assignment tab
    * I click on assignment edit button
    * I enter <Biller> as task description field
    * I click on save button to update task
    * I click on logout button
    * I enter login credentials
    * I navigate to patient grid
    And I click on Task main tab
    Then I should see <ModifiedBy> as modified by update

    Examples: 
      | Patient          | Strtdate          | ModifiedBy  | Assigned     | Status      | Desription             | Desription            | Owner     | Completeddate     | Taskname                            | Reason           | Conditions | Customs_Conditions | Problems                                             | ReferralSource | dynamicid                                                                           | message                                                                                        | UpdatedBy   | status   |
      | Dermo505, Mac505 | 03202021 04:54 AM | hafiz, amir | hafiz, amir | In Progress | Task comment by biller | Task creation process | Care Team | 04202021 04:54 AM | Enjoy your life and forget you age! | Patient Eligible | BPH        | Automation Test    | Encounter for screening for cardiovascular disorders | Provider Team  | Program enrollment status successfully changed to Eligible for selected patient(s). | successProgram enrollment status successfully changed to Eligible for selected patient(s).Hide | Amir, Hafiz | Eligible |
