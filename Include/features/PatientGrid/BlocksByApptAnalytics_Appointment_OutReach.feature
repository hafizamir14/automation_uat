Feature: Analytics - Blocks by Apt. Providers Grid - Appointment/Outreach Tab

  Background: 
    Given I navigate to Analytics grid

  @Analytics_BlockAppointmentOutReachTab
  Scenario Outline: Analytics - Appointment Outreach Tab - Blocks By Appt Provider grid
    When I click on appointmentOutReachTab
    And I navigate to Blocks By Appt Provider grid
    Then I should see <Title> as Block By Appt Provider chart
    When I click on analytics reset button
    And I select Time Period1:<TimePeriod>
    * I select year selection:<YearSelection>
    * I click on analytics apply button
    Then I should see <Title> as Block By Appt Provider chart
    When I click on export appointment report
    Then I should see exported file with <Title> as name
    When I click on nonZero number and verify total number of blocks in drill
    Then I should see the info <BlockName>, <UserName>, <Date>, <StartDate> and <EndDate> with <Duration> and <Comments>
    When I click on export appointment report2
    Then I verify that exported file downloaded successfully

    Examples: 
      | Patient          | TimePeriod | BlockName  | UserName         | YearSelection | Date | Title                     | StartDate | EndDate | Duration        | Comments |
      | Dermo505, Mac505 | Yearly     | Block Name | User(s) to Block |          2021 | Date | Blocks by Appt. Providers | From      | To      | Duration [Mins] | Comments |
