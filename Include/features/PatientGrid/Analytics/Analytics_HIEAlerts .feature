Feature: Analytics - HIE Tab - HIE alerts 

  Background: 
    Given I navigate to Analytics grid

  @Analytics_VerifyHIEAlerts_FormTab
  Scenario Outline: Analytics - HIE Tab - HIE alerts 
    When I click on HIE_tab
    Then I should see <Title> as HIE Alert
    When I click on analytics reset button
    And I select Time Period as:<TimePeriod>
    * I select month selection:<MonthSelection>
    * I click on analytics apply button
    Then I should see <MonthSelection> as HIE secondary heading
    When I click on export HIE report
    Then I should see exported file with <Title> as name

    Examples: 
      | Patient | TimePeriod | LOB | MRN | YearSelection | Date | Title                             | Document | EndDate | Duration        | MonthSelection |
      | Patient | Month      | LOB | MRN |          2022 | Date | Avg. Time to Complete Assessments | Document | To      | Duration [Mins] | February       |
