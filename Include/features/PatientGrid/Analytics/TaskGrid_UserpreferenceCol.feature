Feature: User preference on column filter in "Tasks" grid

  Background: 
    Given I navigate to patient grid

  @UserPref_TaskGrid
  Scenario Outline: User preference on column filter in "Tasks" grid
    When I click on Task main tab
    And I filter patient:<Patient> from task grid
    * I click on logout button
    * I enter login credentials
    * I navigate to patient grid
    And I click on Task main tab
    Then I should see <Patient> is remain in filters

    Examples: 
      | Patient          | Strtdate          | ModifiedBy  | Assigned    | Status      | Desription             | Desription            | Owner     | Completeddate     | Taskname                            | Reason           | Conditions | Customs_Conditions | Problems                                             | ReferralSource | dynamicid                                                                           | message                                                                                        | UpdatedBy   | status   |
      | Dermo505, Mac505 | 03202021 04:54 AM | hafiz, amir | hafiz, amir | In Progress | Task comment by biller | Task creation process | Care Team | 04202021 04:54 AM | Enjoy your life and forget you age! | Patient Eligible | BPH        | Automation Test    | Encounter for screening for cardiovascular disorders | Provider Team  | Program enrollment status successfully changed to Eligible for selected patient(s). | successProgram enrollment status successfully changed to Eligible for selected patient(s).Hide | Amir, Hafiz | Eligible |
