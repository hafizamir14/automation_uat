Feature: Reminder Systems For Charts

  Background: 
    Given I navigate to patient grid

  @ReminderSystem_ShowNotifications
  Scenario Outline: Show notification alert for Un-signed SOAP note
    Then I should see the ring bell notification icon on top right corner
    When I click on notification icon
    Then I should see the label of alert notification of UnSign SOAP note
    And I should see the label of <Notifications> is in bold letter
    * I should see the list of all unsigned notifications list

    Examples: 
      | MiddleText                                           | Notifications |
      | That’s all your notifications from the last 30 days. | Notifications |
