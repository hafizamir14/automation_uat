Feature: Analytics - Graph_Trends - Form Tab

  Background: 
    Given I navigate to Analytics grid

  @Analytics_FromTab
  Scenario Outline: Analytics - Graph_Trends - Form Tab
    When I click on Form_tab
    And I navigate to Forms by CT Members grid
    Then I should see <Title> as CT Member chart
    When I select Form Type:<FormType>
    And I select Form Facility:<Facility>
    * I click on analytics apply button
    Then I should see <Title> as CT Member chart

    Examples: 
      | Facility            | Title               | FormType        |
      | RMED LLC GREENACRES | Forms by CT Members | Palliative Care |
