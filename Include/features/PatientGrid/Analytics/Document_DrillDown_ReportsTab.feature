Feature: Analytics - Document Report Grid - Report Tab

  Background: 
    Given I navigate to Analytics grid

  @Analytics_DocumentReportTab
  Scenario Outline: Analytics - Document Report Tab - DrillDown
    When I click on Reports Tab
    When I click on analytics reset button
    And I select ReportType:<Title>
    * I select Time Period as:<TimePeriod>
    * I select year selection:<YearSelection>
    * I click on analytics apply button
    When I click on nonZero number
    Then I should see the col <Patient> as doc
    * I should see that Last Modified By will be hidden by default
    * I should be able to see Last Modified By columns on UI from column configuration

    Examples: 
      | Patient | TimePeriod | LOB | MRN | YearSelection | Date | Title         | Document | EndDate | Duration        | Comments |
      | Patient | Yearly     | LOB | MRN |          2022 | Date | Documentation | Document | To      | Duration [Mins] | Comments |
