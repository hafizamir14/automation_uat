Feature: Analytics - Graph_Trends - Form Tab

  Background: 
    Given I navigate to Analytics grid

  @Analytics_FromTab_DrillDown
  Scenario Outline: Analytics - Graph_Trends - Form Tab - DrillDown
    When I click on Form_tab
    * I navigate to Forms by CT Members grid
    Then I should see <Title> as CT Member chart
    * I select Form Type:<FormType>
    * I click on analytics reset button
    * I select Time Period1:<TimePeriod>
    * I select year selection:<YearSelection>
    * I click on analytics apply button
    Then I should see <Title> as CT Member chart
    When I click on export CT Member report
    Then I should see exported file with <Title> as name
    When I click on nonZero number and verify total number of CTMembers in drill
    Then I should see the <PatientInfo> as info
    When I click on export CT Member report2
    Then I verify that exported file downloaded successfully

    Examples: 
      | PatientInfo | TimePeriod | BlockName  | UserName         | YearSelection | Date | Title               | StartDate | EndDate | Duration        | Comments | FormType        |
      | Patient     | Yearly     | Block Name | User(s) to Block |          2021 | Date | Forms by CT Members | From      | To      | Duration [Mins] | Comments | Palliative Care |
