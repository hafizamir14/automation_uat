Feature: Analytics - Blocks by Apt. Providers Grid - Appointment/Outreach Tab

  Background: 
    Given I navigate to Analytics grid

  @Analytics_AppointmentOutReachTab_Export
  Scenario Outline: Analytics - Appointment Outreach Tab Export Funtion
    When I click on ReportTab
    Then I click on export report and i should see exported file with DocumentTitle

    Examples: 
      | Patient          |
      | Dermo505, Mac505 |
