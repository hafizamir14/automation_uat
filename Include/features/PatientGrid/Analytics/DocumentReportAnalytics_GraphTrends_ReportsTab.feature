Feature: Documents Report in Analytics > Graphs / Trends > Reports Tab

  Background: 
    Given I navigate to Analytics grid

  @Analytics_ReportsTab
  Scenario Outline: Analytics - Reports Tab
    When I click on Reports Tab
    Then I should see <ReportType1> as report type
    When I click on analytics reset button
    And I select ReportType:<Title>
    * I click on analytics apply button
    Then I should see <Label1> as Label1 and <Label2> as Label2
    When I click on document report arrow
    Then I should see the <Ascending> and <Decending> options from column header dropdown
    * I should see the <ColumnConfiguration> options from column header dropdown as ColumnConfiguration
    * I should see the <Filter> options from column header dropdown as filters

    Examples: 
      | ReportType1   | Ascending      | Decending       | Title         | Label1        | Label2           | ColumnConfiguration | Filter |
      | Documentation | Sort Ascending | Sort Descending | Documentation | Document Type | No. of Documents | Columns             | Filter |
