Feature: Analytics - Form Tab - Verify Date Secondary Title Is According To Filter

  Background: 
    Given I navigate to Analytics grid

  @Analytics_VerifySecondaryHeading_FormTab
  Scenario Outline: Analytics - Verify Date Secondary Title Is According To Filter
    When I click on Form_tab
    And I click on analytics reset button
    * I select Time Period as:<TimePeriod>
    * I select month selection:<MonthSelection>
    * I click on analytics apply button
    Then I should see <MonthSelection> as secondary heading

    Examples: 
      | Patient | TimePeriod | LOB | MRN | YearSelection | Date | Title         | Document | EndDate | Duration        | MonthSelection |
      | Patient | Month      | LOB | MRN |          2022 | Date | Documentation | Document | To      | Duration [Mins] | February       |
