package patientTimer

import org.junit.runner.RunWith

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber


@RunWith(Cucumber.class)
@CucumberOptions(features="Include/features/PatientGrid/DFT_Client_Cases/PACM_PatientTimerWithFilters.feature",
glue="",
tags = "@DFT_PatientTimer",
plugin=["pretty", "html:ReportsFolder", "json:ReportsFolder/cucumber.json"])

public class CR_PACM_PatientTimerWithFilters {
}
