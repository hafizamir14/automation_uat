package patientTimer

import org.junit.runner.RunWith

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber


@RunWith(Cucumber.class)
@CucumberOptions(features="Include/features/PatientGrid/PACMProgramEnrollment_PatientTimer_Notes_TransitionsForms_CarePhysician.feature",
glue="",
tags = "@ProgramEnrollmentCoderWorkFlow",
plugin=["pretty", "html:ReportsFolder", "json:ReportsFolder/cucumber.json"])

public class CR_PACMProgramEnrollment_PatientTimer_Notes_TransitionsForm_CarePhysicians {
}
