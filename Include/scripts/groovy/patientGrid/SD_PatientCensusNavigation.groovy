package patientGrid

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.Given
import internal.GlobalVariable


public class SD_PatientCensusNavigation {



	@Given("I navigate to patient census")
	public void I_navigate_to_patient_Census() {



		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_HomePage/Obj_Enterprise'), 10)
		WebUI.click(findTestObject('Object Repository/OR_HomePage/Obj_Enterprise'))

		if(GlobalVariable.Enterprise=='MHPN') {

			WebUI.click(findTestObject('Object Repository/OR_HomePage/Obj_SelectMHPN'))
		}
		if(GlobalVariable.Enterprise=='Support Test Ent') {

			WebUI.click(findTestObject('Object Repository/OR_HomePage/Obj_SelectSupportTestEnt'))
		}


		if(WebUI.verifyElementPresent((findTestObject('OR_Alert/Obj_Hide')), 3, FailureHandling.OPTIONAL)) {

			WebUI.click(findTestObject('OR_Alert/Obj_Hide'))
		}else {
			WebUI.getUrl()
		}



		WebUI.mouseOver(findTestObject('OR_LandingPage/OR_CMR/Obj_CareCoordination'))


		Thread.sleep(2000)

		WebUI.click(findTestObject('Object Repository/OR_LandingPage/OR_CMR/Obj_Patients'))

		if(WebUI.verifyElementPresent((findTestObject('OR_Alert/Obj_Hide')), 3, FailureHandling.OPTIONAL)) {

			WebUI.click(findTestObject('OR_Alert/Obj_Hide'))
		}else {
			WebUI.getUrl()
		}


		WebUI.enableSmartWait()
		Thread.sleep(8000)
	}
}
