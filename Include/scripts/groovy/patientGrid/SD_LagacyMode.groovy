package patientGrid

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.Given
import utility_Functions.UtilityFunctions





public class SD_LagacyMode {

	UtilityFunctions obj=new UtilityFunctions();
	TestObject frame=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/CareManagementForm/Obj_CCMFrame')


	@Given("I switch theme between NEO and Lagacy")
	public void I_VerifyLagacyMode() {

		Thread.sleep(5000)

		if(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_LegacyMode/Obj_PatientClickLegacy'), 10, FailureHandling.OPTIONAL)) {

			'I click on Enterprise field'
			WebUI.click(findTestObject('Object Repository/OR_LegacyMode/Obj_EnterPriseClick'))
			WebUI.click(findTestObject('Object Repository/OR_LegacyMode/Obj_EnterPrise_Select'))

			WebUI.waitForElementVisible(findTestObject('OR_Alert/Obj_Hide'), 20)
			
			if(WebUI.verifyElementPresent((findTestObject('OR_Alert/Obj_Hide')), 2, FailureHandling.OPTIONAL)) {

				WebUI.click(findTestObject('OR_Alert/Obj_Hide'))
			}else {
				WebUI.getUrl()
			}
			
			Thread.sleep(2000)
			'I click on Switch Theme field While User Was On NEO Theme'
			WebUI.click(findTestObject('Object Repository/OR_LegacyMode/Obj_SwitchThemeClick'))
			'I click on Switch Theme Checkbox While User Was On NEO Theme'

			WebUI.click(findTestObject('Object Repository/OR_LegacyMode/Obj_SwitchTheme_Checkbox'))

			Thread.sleep(30000)
			
			String ActualText = WebUI.getText(findTestObject('Object Repository/OR_LegacyMode/Obj_VerifySureAlert'))
			if(ActualText.is("You are about to change the theme"))
				WebUI.verifyMatch(ActualText, "You are about to change the theme", false)
				WebUI.click(findTestObject('Object Repository/OR_LegacyMode/Obj_VerifySureAlert_YesBTN'))
				Thread.sleep(3000)
				
		}else if(WebUI.verifyElementPresent(findTestObject('OR_LandingPage/OR_CMR/Obj_CMR'), 10, FailureHandling.OPTIONAL)){

			Thread.sleep(2000)
			
			WebUI.selectOptionByLabel(findTestObject('OR_HomePage/Obj_Enterprise'), "US Medical Management ACO", false)
			Thread.sleep(10000)
			
			if(WebUI.verifyElementPresent((findTestObject('OR_Alert/Obj_Hide')), 3, FailureHandling.OPTIONAL)) {

				WebUI.click(findTestObject('OR_Alert/Obj_Hide'))
			}else {
				WebUI.getUrl()
			}


			'I click on Switch Theme field While User Was On NEO Theme'
			WebUI.click(findTestObject('Object Repository/OR_LegacyMode/Obj_SwitchThemeClickOld'))
			'I click on Switch Theme Checkbox While User Was On NEO Theme'

			WebUI.click(findTestObject('Object Repository/OR_LegacyMode/Obj_SwitchTheme_CheckboxOld'))

			Thread.sleep(30000)
			
			String ActualText = WebUI.getText(findTestObject('Object Repository/OR_LegacyMode/Obj_VerifySureAlert'))
			if(ActualText.is("You are about to change the theme"))
				WebUI.verifyMatch(ActualText, "You are about to change the theme", false)
				WebUI.click(findTestObject('Object Repository/OR_LegacyMode/Obj_VerifySureAlert_YesBTN'))
				
				
			Thread.sleep(5000)
		}
	}
}

