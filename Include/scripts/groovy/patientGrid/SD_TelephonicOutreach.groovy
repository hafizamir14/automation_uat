package patientGrid

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys

import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import cucumber.api.java.en.Then
import utility_Functions.UtilityFunctions

public class SD_TelephonicOutreach {

	UtilityFunctions obj=new UtilityFunctions();
	TestObject priotityobj=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/CareManagementForm/selectpriority')
	TestObject frame=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/CareManagementForm/Obj_CCMFrame')


	@And("I click on telephonic outreach")
	public void I_click_on_telephonicoutreach() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_TelephonicOutreach'))
	}


	@And("I should see TelephonicOutreach care form title")
	public void I_should_see_Telephonic_Care_Title() {

		String actual_Title = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PopupValidation/Obj_TelephonicOutreachTitle'))

		WebUI.verifyEqual(actual_Title, 'Telephonic Outreach Form')
	}

	@Then("I select TelephonicPriority:(.*)")
	public void i_select_priority_priority2(String priority) {
		'I click on priority field'
		WebUI.click(priotityobj)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='(//ul[contains(@id, "Telephonic_outreach_priority")])[2]//li[contains(text(), "'+priority+'")]'

		obj.selectdropdown(frame,xpath)
	}



	@And("I enter telephonic outreach (.*) as datetime")
	public void I_enter_DateTime_telephonicoutreach(String DateTime) {

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_telephonicStartDate'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_telephonicStartDate'), DateTime)
	}

	@And("I enter telephonic outreach (.*) as provider")
	public void I_enter_as_provider(String Provider) {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_ProviderClick'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_ProviderInput'), Provider)

		WebUI.enableSmartWait()
		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_ProviderInput'), Keys.chord(Keys.ENTER))
	}

	@And("I select outreach office location")
	public void I_select_OfficeLocation() {

		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_OfficeLocation_Click'), 10)

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_OfficeLocation_Click'))

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Page_Welcome to Persivia/Obj_OfficeLocationSelecttion'))
	}


	@And("I select outreach spoke with")
	public void I_select_SpokeWith() {

		Thread.sleep(3000)
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_SpokeWith_Click'))

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Page_Welcome to Persivia/Obj_SpokeWithSelecttion'))
	}


	@And("I enter telephonic outreach (.*) as duration")
	public void I_enter_as_duration(String Duration) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_Duration_Input'), Duration)

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_Duration_Input'), Duration)

		Thread.sleep(2000)
	}

	@And("I enter telephonic outreach (.*) as durations")
	public void I_enter_as_durations(String Duration) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_Duration_Input'), Duration)

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_Duration_Input'), Duration)
		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_Duration_Input'), Duration)

		Thread.sleep(2000)
	}


	@And("I select outreach call type")
	public void I_select_CallType() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_CallType_Click'))

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_CallType_Select'))
	}


	@And("I select outreach status")
	public void I_select_Outreach_Status() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_Status_Click'))
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_Status_Select'))
	}


	@And("I enter telephonic outreach (.*) as follow up plan")
	public void I_enter_as_followupPlan(String Follow_Up_Plan) {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_FollowUpPlan_Click'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_FollowUpPlan_Input'), Follow_Up_Plan)

		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_FollowUpPlan_Input'), Keys.chord(Keys.ENTER))
	}

	@And("I enter telephonic outreach (.*) as follow up planss")
	public void I_enter_as_followupPlanss(String Follow_Up_Plan) {


		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/PatientTimer/Obj_CacelBTN_outreachNotes'))
	}


	@And("I select Has patient gone to ER since placed on TOR within last thirty days?")
	public void I_select_HasPatientGoneER() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_HaspatientgonetoERsinceplacedonTOR_Click'))

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Page_Welcome to Persivia/Obj_HasPatientSelect'))
	}

	@And("I select Has patient gone to ER since placed on TOR within last thirty daysss?")
	public void I_select_HasPatientGoneERS() {

		Thread.sleep(5000)

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_HaspatientgonetoERsinceplacedonTOR_Click'))

		Thread.sleep(5000)

		WebUI.check(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Page_Welcome to Persivia/Obj_HasPatientSelect - Copy'))
	}


	@And("I select Was patient admittedreadmitted to acute care since placed on TOR in last thirty days?")
	public void I_select_WasPatientAdmitted() {


		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_WasPatientAdmitted_Click'))

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Page_Welcome to Persivia/Obj_WasPatientSelect'))
	}

	@And("I select Was patient admittedreadmitted to acute care since placed on TOR in last thirty dayss?")
	public void I_select_WasPatientAdmittedss() {


		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_WasPatientAdmitted_Click'))
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Page_Welcome to Persivia/Obj_WasPatientSelect'))
	}


	@And("I select Was ER visit or Acute Admission preventable?")
	public void I_select_WasERVisit() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_WasERVisit_Click'))

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Page_Welcome to Persivia/Obj_WasERSelect'))
	}

	@And("I select Was ER visit or Acute Admission preventabless?")
	public void I_select_WasERVisits() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Obj_WasERVisit_Click'))

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TelephonicOutreach/Page_Welcome to Persivia/Obj_WasERSelect'))
	}


	@And("I enter (.*) as outreach comments")
	public void I_enter_as_outreach_Comment(String Comments) {



		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_outreach_Comments'), Comments)
	}

	@And("I select reason for status change")
	public void I_select_statusChange() {

		Thread.sleep(3000)
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/TelephonicOutreach/Obj_ReasonStatusClick'))

		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/TelephonicOutreach/Obj_ReasonStatusSelect'))
	}
}
