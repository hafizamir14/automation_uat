package patientGrid

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import cucumber.api.java.en.Then
import utility_Functions.UtilityFunctions



public class SD_CourtesyCall {

	TestObject frame=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/CareManagementForm/Obj_CCMFrame')
	TestObject Attempt1Dropdown=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_attempt1Dropdown_Click')
	TestObject Attempt2Dropdown=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_attempt2Dropdown_Click')
	TestObject WasLetterMailedClick=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_WasLaterDropdown_Click')
	TestObject AttemptedTwiceWithoutSuccessClick=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_TwiseDropdown_Click')
	TestObject DischargeLocationClick=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_DischargeLocation_Click')
	TestObject HaveYouFilledYourPrescriptionsClick=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_HaveYouFilledYourPrescriptions_Click')
	TestObject KeepingThisAppointmentClick=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_KeepingThisAppointment_Click')
	TestObject DoYouHaveAnyOtherQuestionClick=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_DoYouHaveAnyOtherQuestion_Click')
	TestObject SocialDeterminationClick=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_SocialDetermination_Click')
	TestObject AnyBarriersIdentifiedClick=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_AnyBarriersIdentified_Click')
	TestObject AnyFoundConcernsClick=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_AnyFoundConcerns_Click')


	TestObject DoYouHaveFollowUpAppointmentScheduledClick=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_DoYouHaveFollowUpAppointmentScheduled_Click')


	UtilityFunctions obj=new UtilityFunctions();


	@And("I enter (.*) as dischargeDate")
	public void i_enter_DischargeDate(String DischargeDate) {

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_DischargeDate'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_DischargeDate'), DischargeDate)
	}



	@And("I enter (.*) as Attempt1_DateTime")
	public void i_enter_Attempt1_DateTime(String Attempt1_DateTime) {

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_attempt1Date'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_attempt1Date'), Attempt1_DateTime)
	}

	@Then("I select Attempt1_DateTime Dropdown:(.*)")
	public void i_select_Attempt1_Dropdown(String Attempt1_Dropdown) {
		'I click on privacy field'
		WebUI.click(Attempt1Dropdown)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id, "attemptNo1DateStatus_listbox")]//li[text()="'+Attempt1_Dropdown+'"]'
		obj.selectdropdown(frame,xpath)
	}

	@And("I enter (.*) as Attempt2_DateTime")
	public void i_enter_Attempt2_DateTime(String Attempt2_DateTime) {

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_attempt2Date'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_attempt2Date'), Attempt2_DateTime)
	}

	@Then("I select Attempt2_DateTime Dropdown:(.*)")
	public void i_select_Attempt2_Dropdown(String Attempt2_Dropdown) {
		'I click on privacy field'
		WebUI.click(Attempt2Dropdown)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id, "attemptNo2DateStatus_listbox")]//li[text()="'+Attempt2_Dropdown+'"]'
		obj.selectdropdown(frame,xpath)
	}



	@Then("I select Attempted twice without success:(.*)")
	public void i_select_Attemptedtwice(String AttemptedTwiceWithoutSuccess) {
		'I click on privacy field'
		WebUI.click(AttemptedTwiceWithoutSuccessClick)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"attemptedTwiceWithoutSuccess_listbox")]//li[text()="'+AttemptedTwiceWithoutSuccess+'"]'
		obj.selectdropdown(frame,xpath)
	}

	@Then("I select Was letter mailed:(.*)")
	public void i_select_WasLetterMailed(String WasLetterMailed) {
		'I click on privacy field'
		WebUI.click(WasLetterMailedClick)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='(//li[text()="'+WasLetterMailed+'"])[3]'
		obj.selectdropdown(frame,xpath)
	}

	@Then("I select Discharge Location:(.*)")
	public void i_select_DischargeLocation(String DischargeLocation) {

		WebUI.scrollToElement(DischargeLocationClick,10)
		'I click on privacy field'
		WebUI.click(DischargeLocationClick)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"dischargeLocation_listbox")]//li[text()="'+DischargeLocation+'"]'
		obj.selectdropdown(frame,xpath)
	}



	@And("I enter (.*) as How are you feeling since discharge")
	public void i_enter_HowAreYouFeelingSinceDischarge(String HowAreYouFeelingSinceDischarge) {

		WebUI.scrollToElement(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_HowAreYouFeelingSinceDischarge'), 10)

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_HowAreYouFeelingSinceDischarge'), HowAreYouFeelingSinceDischarge)
	}

	@Then("I select Have you filled your prescriptions:(.*)")
	public void i_select_HaveYouFilledYourPrescriptions(String HaveYouFilledYourPrescriptions) {

		WebUI.scrollToElement(HaveYouFilledYourPrescriptionsClick, 10)
		'I click on privacy field'
		WebUI.click(HaveYouFilledYourPrescriptionsClick)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[@id="haveYouFilledYourPrescriptions_listbox"]//li[contains(text(),"'+HaveYouFilledYourPrescriptions+'")]'
		obj.selectdropdown(frame,xpath)
	}

	@Then("I select Do you have a follow up appointment scheduled:(.*)")
	public void i_select_DoYouHaveFollowUpAppointmentScheduled(String DoYouHaveFollowUpAppointmentScheduled) {

		WebUI.scrollToElement(DoYouHaveFollowUpAppointmentScheduledClick, 10)

		'I click on privacy field'
		WebUI.click(DoYouHaveFollowUpAppointmentScheduledClick)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"UpAppointmentScheduled_listbox")]//li[contains(text(),"'+DoYouHaveFollowUpAppointmentScheduled+'")]'
		obj.selectdropdown(frame,xpath)
	}


	@And("I enter (.*) as Appointment Dates")
	public void i_enter_AppointmentDate(String AppointmentDate) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_AppointmentDate'), AppointmentDate)
	}


	@And("I enter (.*) as Is there anything that might prevent you from keeping this appointment")
	public void i_enter_KeepingThisAppointmentField(String KeepingThisAppointmentField) {


		WebUI.scrollToElement(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_KeepingThisAppointmentField'), 5)

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_KeepingThisAppointmentField'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_KeepingThisAppointmentField'), KeepingThisAppointmentField)
	}


	@Then("I select Is there anything that might prevent you from keeping this appointment:(.*)")
	public void i_select_KeepingThisAppointment(String KeepingThisAppointment) {

		WebUI.scrollToElement(KeepingThisAppointmentClick, 10)

		'I click on privacy field'
		WebUI.click(KeepingThisAppointmentClick)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"isThereAnythingThatMightPreventYouFromKeepingThisAppointment_listbox")]//li[contains(text(),"'+KeepingThisAppointment+'")]'
		obj.selectdropdown(frame,xpath)
	}


	@Then("I select Do you have any other questions or concerns at this time:(.*)")
	public void i_select_DoYouHaveAnyOtherQuestion(String DoYouHaveAnyOtherQuestion) {

		WebUI.scrollToElement(DoYouHaveAnyOtherQuestionClick, 10)

		'I click on privacy field'
		WebUI.click(DoYouHaveAnyOtherQuestionClick)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"doYouHaveAnyOtherQuestionsOrConcernsAtThisTime_listbox")]//li[contains(text(),"'+DoYouHaveAnyOtherQuestion+'")]'
		obj.selectdropdown(frame,xpath)
	}

	@And("I enter (.*) as Do you have any other questions or concerns at this time")
	public void i_enter_DoYouHaveAnyOtherQuestionField(String DoYouHaveAnyOtherQuestionField) {

		WebUI.scrollToElement(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_DoYouHaveAnyOtherQuestionField'), 5)


		WebUI.scrollToElement(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_DoYouHaveAnyOtherQuestionField'), 10)

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_DoYouHaveAnyOtherQuestionField'), DoYouHaveAnyOtherQuestionField)
	}

	@Then("I select Social Determinants of Health Screening Completed:(.*)")
	public void i_select_SocialDetermination(String SocialDetermination) {

		WebUI.scrollToElement(SocialDeterminationClick, 10)

		'I click on privacy field'
		WebUI.click(SocialDeterminationClick)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id ,"socialDeterminantsOfHealthScreeningCompleted_listbox")]//li[contains(text(),"'+SocialDetermination+'")]'
		obj.selectdropdown(frame,xpath)
	}

	@Then("I select Any barriers identified:(.*)")
	public void i_select_AnyBarriersIdentified(String AnyBarriersIdentified) {

		WebUI.scrollToElement(AnyBarriersIdentifiedClick, 10)

		'I click on privacy field'
		WebUI.click(AnyBarriersIdentifiedClick)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"anyBarriersIdentified_listbox")]//li[contains(text(),"'+AnyBarriersIdentified+'")]'
		obj.selectdropdown(frame,xpath)
	}

	@And("I enter (.*) as any barriers")
	public void i_enter_AnyBarriersIdentifiedField(String AnyBarriersIdentifiedField) {

		WebUI.scrollToElement(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_AnyBarriersIdentifiedField'), 10)

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_AnyBarriersIdentifiedField'), AnyBarriersIdentifiedField)
	}

	@Then("I select Any found concerns or potential barriers reported to primary care physicians office:(.*)")
	public void i_select_AnyFoundConcerns(String AnyFoundConcerns) {

		WebUI.scrollToElement(AnyFoundConcernsClick, 10)

		'I click on privacy field'
		WebUI.click(AnyFoundConcernsClick)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"anyFoundConcernsOrPotentialBarriersReportedToPrimaryCarePhysicianOffice_listbox")]//li[contains(text(),"'+AnyFoundConcerns+'")]'
		obj.selectdropdown(frame,xpath)
	}

	@Then("I select Discuss With:(.*)")
	public void i_select_DiscussWith(String DiscussWith) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/CourtesyCall/Obj_DiscussedWithClick'), 10)

		'I click on DiscussWith field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/CourtesyCall/Obj_DiscussedWithClick'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id, "discussedWith_listbox")]//li[contains(text(),"'+DiscussWith+'")]'
		obj.selectdropdown(frame,xpath)
	}

	@Then("I enter (.*) as notified")
	public void i_enter_Notified(String Notified) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/CourtesyCall/Obj_Notified'), 10)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/CourtesyCall/Obj_Notified'), Notified)
	}


	@Then("I enter (.*) as Notified Title")
	public void i_enter_NotifiedTitle(String NotifiedTitle) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/CourtesyCall/Obj_NotifiedTitle'), 10)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/CourtesyCall/Obj_NotifiedTitle'), NotifiedTitle)
	}


	@Then("I enter (.*) as notified date complete")
	public void i_enter_NotifiedDateCompleted(String NotifiedDateCompleted) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/CourtesyCall/Obj_NotifiedDateComplete'), 10)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/CourtesyCall/Obj_NotifiedDateComplete'), NotifiedDateCompleted)
	}


	@Then("I enter (.*) as Notified Signature")
	public void i_enter_NotifiedSignature(String NotifiedSignature) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/CourtesyCall/Obj_NotifiedSignature'), 10)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/CourtesyCall/Obj_NotifiedSignature'), NotifiedSignature)
	}


	@Then("I enter (.*) as notified signature date complete")
	public void i_enter_NotifiedSignatureCompleteDate(String NotifiedSignatureCompleteDate) {

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/CourtesyCall/Obj_NotifiedSignatureDateComplete'), NotifiedSignatureCompleteDate)
	}

	@Then("I should see (.*) as courtesy date of service")
	public void VerifyDateOfServices(String DateTime) {
		'Verify date of services'
		String date4 = DateTime.substring(0, 8)

		String date5 = DateTime.substring(0, 8)

		WebUI.verifyMatch(date4, date5, false)
	}
}
