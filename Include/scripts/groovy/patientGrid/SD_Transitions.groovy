package patientGrid

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver

import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import groovypackage.Methods
import internal.GlobalVariable
import utility_Functions.UtilityFunctions


public class SD_Transitions {

	TestObject frame=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/CareManagementForm/Obj_CCMFrame')
	TestObject privacyobj=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/selectprivacy')
	TestObject priotityobj=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/selectpriority')
	TestObject ERprivacyobj=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/ER Follow Up Calls/selectprivacy')
	TestObject ERpriotityobj=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/ER Follow Up Calls/selectpriority')

	TestObject Hosprivacyobj=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Hospital Discharge Calls/selectprivacy')
	TestObject Hospriotityobj=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Hospital Discharge Calls/selectpriority')

	TestObject ContactMethodClick=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_ContactMethod_Click')
	TestObject DiscussWithClick=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_DiscussedWith_CLick')
	TestObject AdditionalRecordsClick=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_AdditionalRecord_Click')
	TestObject OutreachClick=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_OutreachDropdown_Click')
	TestObject FormTypeClick=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_FormType_Click')
	TestObject DOB=findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/Obj_DOB_Rearange')
	TestObject Program=findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/Obj_EncProgram_Rearange')
	TestObject EncTypeClick=findTestObject('Object Repository/Transitions/Obj_EncTypeClick')


	TestObject InGeneralHowAreYouFeelingClick=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Surgical Follow Up Calls/Obj_InGeneralHowAreYouFeeling_Click')

	UtilityFunctions obj=new UtilityFunctions();




	WebDriver driver = null;

	@And("I select (.*) as cm status")

	public void selectStatus(String CM_Status) {

		Thread.sleep(5000)

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_CmStatus_Click'))

		if(CM_Status == "Eligible") {

			WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_CmStatus_Eligible'))
		}

		if(CM_Status== "Active") {

			WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_CmStatus_Active'))
		}


		if(CM_Status== "Completed") {

			WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_CmStatus_Completed'))
		}
	}


	@And("I click on CM link")
	public void Click_CMStatus() {

		'Search Patient'

		WebUI.waitForElementClickable(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_inputSearchCareCooridnation'), 20)

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_inputSearchCareCooridnation'))

		WebUI.setText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_inputSearchCareCooridnation'), "MAIER, BARBARA")

		Thread.sleep(4000)

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_inputSearchSelect'))

		Thread.sleep(10000)

		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Grid/Obj_ClickCMStatusLink'),5)

		'Click On Hyper Link'
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Grid/Obj_ClickCMStatusLink'))

		Thread.sleep(18000)
	}

	@And("I search Patient from transition")
	public void SearchTransitionPatient() {

		'Search Patient'

		WebUI.waitForElementClickable(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_inputSearchCareCooridnation'), 20)

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_inputSearchCareCooridnation'))
		WebUI.clearText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_inputSearchCareCooridnation'))

		WebUI.setText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_inputSearchCareCooridnation'), "MAIER, BARBARA")

		Thread.sleep(4000)

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_inputSearchSelect'))

		Thread.sleep(10000)

		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Grid/Obj_ClickCMStatusLink'),5)

		WebUI.refresh()

		WebUI.mouseOver(findTestObject('OR_LandingPage/OR_CMR/Obj_CMR'))
		Thread.sleep(1000)

		WebUI.click(findTestObject('Object Repository/OR_LandingPage/OR_CMR/Obj_Patients'))
	}
	@And("I search Patient from transitionAgain")
	public void SearchTransitionPatient2() {

		'Search Patient'

		WebUI.waitForElementClickable(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_inputSearchCareCooridnation'), 20)

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_inputSearchCareCooridnation'))
		WebUI.clearText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_inputSearchCareCooridnation'))

		WebUI.setText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_inputSearchCareCooridnation'), "MAIER, BARBARA")

		Thread.sleep(4000)

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_inputSearchSelect'))

		Thread.sleep(10000)

		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Grid/Obj_ClickCMStatusLink'),5)
	}




	@And("I click on CM link again")
	public void Click_CMStatus_again() {


		'Click On Hyper Link'
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Grid/Obj_ClickCMStatusLink'))

		Thread.sleep(4000)
	}



	@Then("I should see popup details")
	public void I_should_see_popup_details() {


		'Verify Title'
		String Actual_Title = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Grid/Obj_VerifyAlertTitle'))

		WebUI.verifyEqual(Actual_Title, "Notification Details")



		'Verify Name'
		String Actual_Name = WebUI.getText(findTestObject('Object Repository/Transitions/Obj_PatientNameGet'))

		String Expected_Name = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_Name'))

		WebUI.verifyEqual(Actual_Name, Expected_Name)

		//		'Verify MRN'
		//		String Actual_MRN = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Grid_Verification/Obj_MRN'))
		//
		//		String Expected_MRN = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_MRN'))
		//
		//		WebUI.verifyEqual(Actual_MRN, Expected_MRN)


		//		'Verify Admitted Date'
		//		String Actual_AdmitDate = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Grid_Verification/Obj_AdmittedDate'))
		//
		//		String Expected_AdmitDate = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_AdmittedDate'))
		//
		//		if(Expected_AdmitDate.contains(Actual_AdmitDate)) {
		//
		//			WebUI.verifyEqual(Actual_AdmitDate, Expected_AdmitDate);
		//		}
		//
		//
		//
		//		'Verify Dischare Date'
		//		String Actual_DischargeDate = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Grid_Verification/Obj_DischargeDate'))
		//
		//		String Expected_DischargeDate = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_DischargeDate'))
		//
		//
		//		if(Expected_DischargeDate.contains(Actual_DischargeDate)) {
		//
		//			WebUI.verifyEqual(Actual_DischargeDate, Expected_DischargeDate);
		//		}
		//
		//
		//		'Verify Serving Facility'
		//		String Actual_Facility = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Grid_Verification/Obj_ServingFacility'))
		//
		//		String Expected_Facility = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_ServingFacility'))
		//
		//		WebUI.verifyEqual(Actual_Facility, Expected_Facility)


		//		'Verify Lace'
		//		String Actual_Lace = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Grid_Verification/Obj_Lace'))
		//
		//		String Expected_Lace = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_Lace'))
		//
		//		WebUI.verifyEqual(Actual_Lace, Expected_Lace)
	}

	@When("I enter due date (.*) as DT")
	public void user_Enter_DueDate(String Due_Date) {

		WebUI.clearText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_DueDate'))

		WebUI.setText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_DueDate'), Due_Date)
		WebUI.setText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_DueDate'), Due_Date)

		Thread.sleep(2000)
	}

	@When("I select CM Status")
	public void Select_CMStatus() {


		String CMText = WebUI.getText(findTestObject('Object Repository/Transitions/Obj_GetCMStatusText'))

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_CMStatus_Updated_Click'))

		if(CMText == "Active")

			WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_CMStatus_Updated_SelectEligible'))

		if(CMText == "Eligible")

			WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_CMStatus_Updated_SelectActive'))
	}

	@When("I select CM Status again")
	public void Select_CMStatusAgain() {


		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_CMStatus_Updated_Click'))
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_CMStatus_Updated_SelectEligible'))
	}

	@When("I enter care provider (.*) as CP")
	public void user_Enter_CareProvider(String CareProvider) {



		WebUI.sendKeys(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_CareProvider'), Keys.chord(Keys.BACK_SPACE))
		Thread.sleep(2000)

		WebUI.setText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_CareProvider'), CareProvider)

		Thread.sleep(1000)

		WebUI.sendKeys(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_CareProvider'), Keys.chord(Keys.ENTER))
	}

	@When("I enter care coordination (.*) as CC")
	public void user_Enter_CareCoordination(String CareCoordination) {


		WebUI.sendKeys(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_CareCoordination'), Keys.chord(Keys.BACK_SPACE))
		Thread.sleep(2000)
		WebUI.setText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_CareCoordination'), CareCoordination)
		Thread.sleep(1000)

		WebUI.sendKeys(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_CareCoordination'), Keys.chord(Keys.ENTER))
	}

	@When("I click on save button to save notification details")
	public void ClickOnSaveBTNTO_() {


		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_SaveBTN'))

		Thread.sleep(3000)
	}

	@Then("I should see this message (.*) as Notification")
	public void I_should_see_notificationSuccessMessage(String SuccessMessage) {

		'Verify Notification Message'
		if(WebUI.verifyElementPresent((findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_NotificationMessage')), 3, FailureHandling.OPTIONAL)) {

			String actual_Message  = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_NotificationMessage'))

			WebUI.verifyEqual(actual_Message, SuccessMessage)
		}else {
			Thread.sleep(8000)

			WebUI.refresh()

			WebUI.mouseOver(findTestObject('OR_LandingPage/OR_CMR/Obj_CMR'))
			Thread.sleep(1000)

			WebUI.click(findTestObject('Object Repository/OR_LandingPage/OR_CMR/Obj_Patients'))
		}


		//		'Verify Notification Message'
		//		String Actual_Message = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_NotificationMessage'))
		//
		//		WebUI.verifyEqual(Actual_Message, SuccessMessage)

		//		Thread.sleep(8000)
		//
		//				WebUI.refresh()
		//
		//				WebUI.mouseOver(findTestObject('OR_LandingPage/OR_CMR/Obj_CMR'))
		//				Thread.sleep(1000)
		//
		//				WebUI.click(findTestObject('Object Repository/OR_LandingPage/OR_CMR/Obj_Patients'))

	}

	@Then("I should see patient details including (.*) AND (.*) AND (.*) AND CM_Status after updating data from popup")
	public void I_should_see_patientDataFromPopup(String CareCoordination, String CareProvider, String Due_Date) {


		'Verify Care Coordination'


		String Expected_CareCoordination = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Grid_Verification/Obj_CareCoordination'))


		if(!Expected_CareCoordination.contains(CareCoordination)) {

			WebUI.verifyMatch(CareCoordination, Expected_CareCoordination, false)
		}


		'Verify Care Provider'
		String Actual_CareProvider = CareProvider

		String Expected_CareProvider = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Grid_Verification/Obj_CareProvider'))


		WebUI.verifyEqual(Actual_CareProvider, Expected_CareProvider);



		'Verify Due Date'
		String date = Due_Date.substring(0, 8)

		String actual_DueDate = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Grid_Verification/Obj_DueDate'))

		String date1 = Due_Date.substring(0, 8)

		WebUI.verifyMatch(date, date1, false)


		'Verify Status'
		String Actual_CM_Status = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Grid/Obj_VerifyCMStatus'))

		String Expected_CM_Status = ""

		if(Expected_CM_Status == "Active")

			WebUI.verifyEqual(Actual_CM_Status, Expected_CM_Status)

		if(Expected_CM_Status == "Eligible")

			WebUI.verifyEqual(Actual_CM_Status, Expected_CM_Status)
	}

	@When("I click on LOG icon button")
	public void Click_LogIconBTN() {

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_LogIcon_Click'))
		Thread.sleep(6000)
	}

	@Then("I should see LOG Title")
	public void VerifyLOGTitle() {

		'Verify LOG Change Title'
		String Actual_Title = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_LOGTitle'))

		WebUI.verifyEqual(Actual_Title, "CM Status Change Log")
	}


	@Then("I should see updated LOGS with (.*) as updatedDate")
	public void I_should_see_updatedLogs(String Due_Date) {



		'Verify Updated BY'
		String Actual_UpdatedBY = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_LOG_UpdatedBY'))

		String Expected_UpdatedBY = "Hafiz,Amir"

		WebUI.verifyEqual(Actual_UpdatedBY, Expected_UpdatedBY)



		'Verify Date'
		String date = Due_Date.substring(0, 8)

		String actual_DueDate = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_LOG_UpdatedDate'))

		String date1 = Due_Date.substring(0, 8)

		WebUI.verifyMatch(date, date1, false)

		WebUI.verifyElementPresent(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_LOG_UpdatedStatus'), 3)


		'Verify Status'
		String Actual_CM_Status = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_LOG_UpdatedStatus'))

		String Expected_CM_Status = ""

		if(Expected_CM_Status == "Active")

			WebUI.verifyEqual(Actual_CM_Status, Expected_CM_Status)

		if(Expected_CM_Status == "Eligible")

			WebUI.verifyEqual(Actual_CM_Status, Expected_CM_Status)

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_LOG_Popup_Close'))
	}


	@Then("I should see LOGS including (.*) as Date")
	public void I_should_see_LOGS(String Due_Date) {


		'Verify Updated BY'
		String Actual_Updatedby = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_LOG_UpdatedBY'))

		String Expected_UpdatedbY = "Hafiz,Amir"

		WebUI.verifyEqual(Actual_Updatedby, Expected_UpdatedbY)



		'Verify Date'
		String date = Due_Date.substring(0, 8)

		String actual_DueDate = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_LOG_UpdatedDate'))

		String date1 = Due_Date.substring(0, 8)

		WebUI.verifyMatch(date, date1, false)

		WebUI.verifyElementPresent(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_LOG_UpdatedStatus'), 3)


		'Verify Status'
		String Actual_CM_Status = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_LOG_UpdatedStatus'))

		String Expected_CM_Status = ""

		if(Expected_CM_Status == "Active")

			WebUI.verifyEqual(Actual_CM_Status, Expected_CM_Status)

		if(Expected_CM_Status == "Eligible")

			WebUI.verifyEqual(Actual_CM_Status, Expected_CM_Status)

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/Popup_Verification/Obj_LOG_Popup_Close'))
	}

	@When("I click on care cordination tab")
	public void I_click_on_carecordinationtab() {

		WebUI.waitForElementClickable(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/Obj_CareCoordination'), 20)

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/Obj_CareCoordination'))
		Thread.sleep(2000)

		'Search Patient'

		WebUI.waitForElementClickable(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_inputSearchCareCooridnation'), 20)

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_inputSearchCareCooridnation'))

		WebUI.setText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_inputSearchCareCooridnation'), "Xsah829, Axir829")

		Thread.sleep(4000)

		WebUI.sendKeys(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Left Filters/Obj_inputSearchCareCooridnation'), Keys.chord(Keys.ENTER))

		Thread.sleep(10000)
	}

	@When("I swap DOB with Enc Program")
	public void I_swapDOBwithProgram() {


		WebUI.dragAndDropToObject(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/Obj_DOB_Rearange'), findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/Obj_EncProgram_Rearange'))
		Thread.sleep(10000)
	}

	@Then("I select (.*) as export")
	public void ClickExportOptions(String Export) {


		WebUI.waitForElementClickable(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Grid/Obj_Export_BTNCLick'), 15)

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Grid/Obj_Export_BTNCLick'))

		if(Export== "Export (All Columns)") {

			WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Grid/Obj_Export_AllColms'))
			Thread.sleep(20000)
		}

		if(Export== "Export (Selected Columns)") {

			WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Grid/Obj_Export_SelectedColms'))
			Thread.sleep(20000)
		}
	}
	@Then("I verify that exported file downloaded successfully")
	public void VerifyExportedFileDownloaded() {

		Methods.verifyDownloadedfile("C:\\Users\\hafiz.amir\\Downloads", ".xls")
	}

	@When("I click on care cordination")
	public void I_click_on_carecordination() {

		WebUI.waitForElementClickable(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/Obj_CareCoordination'), 20)

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/Obj_CareCoordination'))
		Thread.sleep(12000)
	}


	@And("I click on care cordination reset button")
	public void click_On_Reset_Button() {

		WebUI.waitForElementClickable(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/Obj_buttonReset'),20)

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/Obj_buttonReset'))

		Thread.sleep(10000)
	}

	@When("I clear before and after dates")
	public void ClearDates() {

		//		Thread.sleep(3000)
		//		WebUI.waitForElementVisible(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_ClearDates/Obj_ClearBeforeDate'), GlobalVariable.timeout)
		//		WebUI.clearText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_ClearDates/Obj_ClearBeforeDate'))
		//		WebUI.clearText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_ClearDates/Obj_ClearAfterDate'))
	}

	@When("I enter (.*) as serving facility")
	public void user_Enter_ServingFacility(String ServingFacility) {


		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_Facility/Obj_ServingFacility_Click'))
		Thread.sleep(1000)

		WebUI.setText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_Facility/Obj_ServingFacility_Input'), ServingFacility)
		Thread.sleep(2000)
		WebUI.sendKeys(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_Facility/Obj_ServingFacility_Input'), Keys.chord(Keys.ENTER))

		Thread.sleep(4000)
	}

	@When("I enter (.*) as serving facility type")
	public void user_Enter_ServingFacilityType(String FacilityType) {


		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_Facility/Obj_ServingFacilityType_Click'))
		Thread.sleep(1000)

		WebUI.setText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_Facility/Obj_ServingFacilityType_Input'), FacilityType)

		WebUI.sendKeys(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_Facility/Obj_ServingFacilityType_Input'), Keys.chord(Keys.ENTER))
	}

	@When("I enter (.*) as assign date from")
	public void user_Enter_AssignFromDate(String AssignDateFrom) {

		WebUI.setText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_AssignDates/Obj_FromDate'),AssignDateFrom )
	}
	@When("I enter (.*) as assign date to")
	public void user_Enter_AssignToDate(String AssignDateTo) {

		WebUI.setText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_AssignDates/Obj_ToDate'),AssignDateTo )
	}



	@And("I click on care cordination apply button")
	public void click_On_CareCordination_Apply_Button() {


		WebUI.waitForElementClickable(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/Obj_buttonApply'), 10)

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/Obj_buttonApply'))

		Thread.sleep(8000)
	}




	@Then("I should see care cordination serving facility (.*)")
	public void I_should_see_care_cordination_ServingFacility_filters(String ServingFacility) {


		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/GridObjects/Obj_ServingFacility_MHPN3'), 10)
		String Actual_ServingFacility = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/GridObjects/Obj_ServingFacility_MHPN3'))
		WebUI.verifyEqual(ServingFacility, Actual_ServingFacility)
	}


	@Then("I should see (.*) care cordination Enc program")
	public void I_should_see_care_cordination_patientprogram_filters(String EncProgram) {


		String Actual_PatientProgram = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/GridObjects/Obj_Program_TCM'))
		WebUI.verifyEqual(Actual_PatientProgram, EncProgram)
	}


	@Then("I should see patients with CM Status as (.*) on care coordination grid")
	public void I_should_see_care_cordination_LOB_filters(String CM_Status) {


		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Grid/Obj_VerifyCMStatus'),5)

		String Actual_CMStatus = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/CM Status/CM Grid/Obj_VerifyCMStatus'))

		if(!Actual_CMStatus.is(CM_Status)) {

			WebUI.verifyEqual(CM_Status, Actual_CMStatus)
		}else {

			throw new NoSuchElementException("Can't find " + CM_Status + " in Grid");
		}
	}

	@Then("I should see care cordination (.*) as serving facility type")
	public void I_should_see_care_cordination_ServingFacilityType_filters(String FacilityType) {

		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/GridObjects/Obj_ServingFacilityType_HOS'), 5)

		String Actual_ServingFacilityType = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/GridObjects/Obj_ServingFacilityType_HOS'))

		WebUI.verifyEqual(Actual_ServingFacilityType, FacilityType)
	}

	@Then("I should see (.*) care cordination Notifications")
	public void I_should_see_care_cordination_Notifications_filters(String NotificationType) {

		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/GridObjects/Obj_NotificationsType'), 5)
		String Actual_NotificatonType = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/GridObjects/Obj_NotificationsType'))

		WebUI.verifyEqual(Actual_NotificatonType, NotificationType)
	}

	@Then("I should see care cordination Encounter (.*) filters")
	public void I_should_see_care_cordination_Encounter_filters(String EncounterType) {


		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/GridObjects/Obj_EncounterType'), 10)

		String Actual_EncounterType = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/GridObjects/Obj_EncounterType'))
		WebUI.verifyEqual(EncounterType, Actual_EncounterType)
	}

	@And("I select (.*) patient program")
	public void Select_PatientProgram(String EncProgram) {

		WebUI.waitForElementClickable(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_Programe/Obj_PatientProgram_Click'), 10)

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_Programe/Obj_PatientProgram_Click'))

		WebUI.setText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_Programe/Obj_PatientProgram_select'), EncProgram)

		WebUI.sendKeys(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_Programe/Obj_PatientProgram_select'), Keys.chord(Keys.ENTER))
	}

	@And("I select (.*) as Notification Type")
	public void Select_Notification(String NotificationType) {


		WebUI.waitForElementClickable(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_NotificationType/Obj_NotificationType_Click'), 10)

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_NotificationType/Obj_NotificationType_Click'))
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_NotificationType/Obj_NotificationType_Select'))
	}

	@And("I select (.*) as LOB")
	public void Select_LOB(String LOB) {

		WebUI.waitForElementClickable(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_LOB/Obj_LOBFilterClick'), 10)


		if(LOB.is(LOB)) {

			WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_LOB/Obj_LOBFilterClick'))
			Thread.sleep(2000);
			String xpath='//li[text()="'+LOB+'"]'
			obj.selectdropdown(frame,xpath)
		}

		else {

			throw new NoSuchElementException("Can't find " + LOB + " in dropdown");
		}
	}

	@And("I enter (.*) as encounter type")
	public void Select_Encounter(String EncounterType) {

		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_EcounterType/Obj_EcounterType_Click'))
		Thread.sleep(1000)

		WebUI.setText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_EcounterType/Obj_EcounterType_Select'), EncounterType)

		WebUI.sendKeys(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_EcounterType/Obj_EcounterType_Select'), Keys.chord(Keys.ENTER))

		Thread.sleep(4000)
	}





	@And("I click on Courtesy Call")
	public void I_click_on_Courtesy() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_CourtesyCall'))
	}

	@Then("I select Coutesy Call privacy:(.*)")
	public void i_select_privacy_privacy(String privacy) {
		'I click on privacy field'
		WebUI.click(privacyobj)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//li[text()="'+privacy+'"]'
		obj.selectdropdown(frame,xpath)
	}

	@Then("I select Coutesy Call priority:(.*)")
	public void i_select_priority_priority(String priority) {
		'I click on priority field'
		WebUI.click(priotityobj)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='(//li[text()="'+priority+'"])[4]'
		obj.selectdropdown(frame,xpath)
	}

	@Then("I select Hospital Discharge Calls privacy:(.*)")
	public void i_select_Hos_privacy(String privacy) {
		'I click on privacy field'
		WebUI.click(Hosprivacyobj)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//li[text()="'+privacy+'"]'
		obj.selectdropdown(frame,xpath)
	}

	@Then("I select Hospital Discharge Calls priority:(.*)")
	public void i_select_Hos_priority(String priority) {
		'I click on priority field'
		WebUI.click(Hospriotityobj)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='(//li[text()="'+priority+'"])[4]'
		obj.selectdropdown(frame,xpath)
	}

	@Then("I select ER Follow Up Call privacy:(.*)")
	public void i_select_ER_privacy(String privacy) {
		'I click on privacy field'
		WebUI.click(ERprivacyobj)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//li[text()="'+privacy+'"]'
		obj.selectdropdown(frame,xpath)
	}

	@Then("I select ER Follow Up Call priority:(.*)")
	public void i_select_ER_priority(String priority) {
		'I click on priority field'
		WebUI.click(ERpriotityobj)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='(//li[text()="'+priority+'"])[4]'
		obj.selectdropdown(frame,xpath)
	}


	@And("I should see transition of care form title")
	public void I_should_see_transition_Title() {

		String actual_Title = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PopupValidation/Obj_TransitionOfCareFormTitle'))

		WebUI.verifyEqual(actual_Title, 'Transition of Care - Courtesy Call')
	}

	@And("I enter (.*) as date of service date and time")
	public void i_enter_transition_AM_as_datetime(String DateTime) {

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_DateOFService'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_DateOFService'), DateTime)
	}



	@And("I enter (.*) as Provider Name and Contact")
	public void i_enter_ProviderNameANDContact(String ProviderName) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_ProviderName'), ProviderName)
	}

	@Then("I select contact method:(.*)")
	public void i_select_contactMethod(String ContactMethod) {

		WebUI.scrollToElement(ContactMethodClick, 5)
		'I click on privacy field'
		WebUI.click(ContactMethodClick)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//li[text()="'+ContactMethod+'"]'
		obj.selectdropdown(frame,xpath)
	}

	@Then("I select discussed with:(.*)")
	public void i_select_DiscussWIth(String DiscussWith) {

		WebUI.scrollToElement(DiscussWithClick, 5)

		'I click on privacy field'
		WebUI.click(DiscussWithClick)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//li[text()="'+DiscussWith+'"]'
		obj.selectdropdown(frame,xpath)
	}

	@Then("I select Additional records reviewed through:(.*)")
	public void i_select_AdditionalRecord(String AdditionalRecords) {

		WebUI.scrollToElement(AdditionalRecordsClick, 5)

		'I click on privacy field'
		WebUI.click(AdditionalRecordsClick)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//li[text()="'+AdditionalRecords+'"]'
		obj.selectdropdown(frame,xpath)
	}

	@Then("I select form type:(.*)")
	public void i_select_formType(String FormType) {

		WebUI.scrollToElement(FormTypeClick, 5)

		'I click on Form Type field'
		WebUI.click(FormTypeClick)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//li[text()="'+FormType+'"]'
		obj.selectdropdown(frame,xpath)
	}

	@And("I click on analytics reset button")
	public void ClickAnalyticsReset() {


		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_ResetBtn_Analytics'))
	}

	@And("I click on analytics apply button")
	public void ClickAnalyticsApply() {


		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_ApplyBtn_Analytics'))
		Thread.sleep(3000)
	}

	@And("I enter analytics from date")
	public void i_enter_FromDate() {

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_Analytics_FromDate'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_Analytics_FromDate'), '11022021')
	}


	@And("I enter (.*) as notified name")
	public void i_enter_notificationName(String NotifiedName) {

		WebUI.scrollToElement(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_NotifiedName'), 10)

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_NotifiedName'), NotifiedName)
	}


	@And("I enter (.*) as notifiedtitle")
	public void i_enter_notifiedTitle(String Title) {

		WebUI.scrollToElement(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_NotifiedTitle'), 10)

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_NotifiedTitle'), Title)
	}


	@And("I enter (.*) as notified date and time")
	public void i_enter_notifiedDate(String DateTime) {

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_NotifiedDate'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_NotifiedDate'), DateTime)
	}


	@And("I enter (.*) as Non Face to Face Completed By")
	public void i_enter_NonFace(String FaceToFaceName) {

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_FaceToFaceName'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_FaceToFaceName'), FaceToFaceName)
	}
	@And("I enter (.*) as Non Face to Face Completed By signature")
	public void i_enter_FaceCompleted(String Signature) {

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_FaceToFaceSignature'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_FaceToFaceSignature'), Signature)
	}
	@And("I enter (.*) as Non Face to Face Completed By date and time")
	public void i_enter_FaceCompletedDateTime(String DateTime) {

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_FaceToFaceDate'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_FaceToFaceDate'), DateTime)
	}

	@And("I enter (.*) as attempt1")
	public void i_enter_attempt1(String Attempt1_DateTime) {

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_attempt1Date'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_attempt1Date'), Attempt1_DateTime)
	}
	@And("I select attempt no one dropdown value")
	public void I_select_Attempt1() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_attempt1Dropdown_Click'))
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_attempt1Dropdown_select'))
	}

	@And("I click on save button to save transition of care form data")
	public void I_SaveBtn_Transition() {
		Thread.sleep(2000)

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_SaveBTN'))

		Thread.sleep(2000)
	}

	@And("I enter (.*) as how are you feeling since discharge")
	public void i_enter_HowAreYouFeeling(String HowAreYouFeeling) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Courtesy Calls/Obj_FaceToFaceDate'), HowAreYouFeeling)
	}


	@Then("I select Transition outreach:(.*)")
	public void i_select_OUtreach(String Outreach) {
		'I click on privacy field'
		WebUI.click(OutreachClick)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//li[text()="'+Outreach+'"]'
		obj.selectdropdown(frame,xpath)
	}

	@And("I enter (.*) as Symptoms leading to Emergency Room visit")
	public void i_enter_LeadingEmgergency(String SymptomsLeadingToEmergency) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/ER Follow Up Calls/Obj_SymptomsLeadingToEmergency'), SymptomsLeadingToEmergency)
	}

	@And("I enter (.*) as Emergency Room Treatment received")
	public void i_enter_RoomTreatment(String EmergencyRommTreatment) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/ER Follow Up Calls/Obj_EmergencyRommTreatment'), EmergencyRommTreatment)
	}

	@And("I enter (.*) as What lead you to being in the hospital")
	public void i_enter_LeadYouHospital(String WhatLeadYouToBeing) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Hospital Discharge Calls/Obj_WhatLeadYouToBeing'), WhatLeadYouToBeing)
	}

	@And("I enter (.*) as Have your symptoms improved worsened or stayed the same since returning home")
	public void i_enter_HaveYOurSymptomsImproved(String HaveYourSymptomsImproved) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Hospital Discharge Calls/Obj_HaveYourSymptomsImproved'), HaveYourSymptomsImproved)
	}

	@And("I should see data in analytics")
	public void i_enter_AnalyticsDate() {

		WebUI.verifyElementPresent(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/Surgical Follow Up Calls/Obj_DateTime'),3)
	}

	@Then("I verify that (.*) file downloaded")
	public void VerifyExportedFileDownloadedTransition(String Tranistion) {

		File downloadFolder = new File("C:\\Users\\hafiz.amir\\Downloads")
		List namesOfFiles = Arrays.asList(downloadFolder.list())
		if(namesOfFiles.is(Tranistion)) {

			println ("Your File Is Not Downloaded Successfully.")
		}
		else {

			println("Your File Is Not Downloaded Successfully.")
		}
	}

	@Given("I navigate to Transition grid")
	public void I_navigate_to_Transition_grid() {

		WebUI.selectOptionByLabel(findTestObject('OR_HomePage/Obj_Enterprise'), "Prime Healthcare", true)

		if(WebUI.verifyElementPresent((findTestObject('OR_Alert/Obj_Hide')), 3, FailureHandling.OPTIONAL)) {

			WebUI.click(findTestObject('OR_Alert/Obj_Hide'))
		}else {
			WebUI.getUrl()
		}

		WebUI.mouseOver(findTestObject('OR_LandingPage/OR_CMR/Obj_CMR'))

		WebUI.click(findTestObject('Object Repository/Transitions/Obj_TransitionNavigate'))

		Thread.sleep(5000)

		if(WebUI.verifyElementPresent((findTestObject('OR_Alert/Obj_Hide')), 3, FailureHandling.OPTIONAL)) {

			WebUI.click(findTestObject('OR_Alert/Obj_Hide'))
		}else {
			WebUI.getUrl()
		}

		WebUI.waitForPageLoad(10, FailureHandling.OPTIONAL)
		Thread.sleep(12000)
	}

	@When("I enter (.*) as encounter program")
	public void user_Enter_EncProgram(String EncProgram) {

		WebUI.click(findTestObject('Object Repository/Transitions/Obj_EncProgramClick'))
		Thread.sleep(1000)

		WebUI.setText(findTestObject('Object Repository/Transitions/Obj_EncProgramInput'), EncProgram)

		WebUI.sendKeys(findTestObject('Object Repository/Transitions/Obj_EncProgramInput'), Keys.chord(Keys.ENTER))
	}

	@And("I should see EpStatus filter")
	public void EpisodeFilterVerify() {

		WebUI.verifyElementPresent(findTestObject('Object Repository/Transitions/Obj_EpStatusClick'),3)
		WebUI.verifyElementText(findTestObject('Object Repository/Transitions/Obj_EpStatusLabel'), 'Ep. Status')
	}

	@And("I should see CovidBundle filter")
	public void CovidBundleFilterVerify() {

		WebUI.verifyElementPresent(findTestObject('Object Repository/Transitions/Obj_CovidBundleClick'),3)
		WebUI.verifyElementText(findTestObject('Object Repository/Transitions/Obj_CovidBundleVerify'), 'All')
		WebUI.verifyElementText(findTestObject('Object Repository/Transitions/Obj_CovidBundleLabel'), 'COVID Bundle')
	}

	@Then("I should see label as (.*) on transition grid")
	public void I_should_see_DRGCodeLabel(String DRGCode) {


		WebUI.scrollToElement(findTestObject('Object Repository/Transitions/Obj_DRGCodeVerify'),5)

		String Actual_Code = WebUI.getText(findTestObject('Object Repository/Transitions/Obj_DRGCodeVerify'))

		WebUI.verifyMatch(Actual_Code, DRGCode, false)
	}

	@Then("I should see label as (.*) on transition grid1")
	public void I_should_see_DRGDescLabel1(String DRGDesc) {


		WebUI.scrollToElement(findTestObject('Object Repository/Transitions/Obj_DRGDescriptionVerify'),5)

		String Actual_DRGDesc = WebUI.getText(findTestObject('Object Repository/Transitions/Obj_DRGDescriptionVerify'))

		WebUI.verifyMatch(Actual_DRGDesc, DRGDesc, false)
	}

	@Then("I should see Code in DRGCode and Desc in DRGDesc")
	public void I_should_see_DRGCodeandDRGDesc() {


		WebUI.scrollToElement(findTestObject('Object Repository/Transitions/Obj_DRGDescriptionVerify'),10)
		'Verify DRG Code Value'
		WebUI.verifyElementPresent(findTestObject('Object Repository/Transitions/Obj_DRGCodeValueVerify'), 3)
		'Verify DRG Desc Value'
		WebUI.verifyElementPresent(findTestObject('Object Repository/Transitions/Obj_DRGDescValueVerify'), 3)
	}

	@Then("I should see EncType under EncProgram filter")
	public void I_should_see_EncTypeUnderEncProgram() {


		WebUI.verifyElementPresent(findTestObject('Object Repository/Transitions/Obj_EncTpyeVerify'), 3)
	}

	@Then("I should see notifyType under EncType filter")
	public void I_should_see_NotifyTypeUnderEncType() {


		WebUI.verifyElementPresent(findTestObject('Object Repository/Transitions/Obj_NotifyTypeVerify'), 3)
	}

	@Then("I should see (.*) as auto_selected")
	public void user_auto_selected(String EncProgram) {

		WebUI.scrollToElement(findTestObject('Object Repository/Transitions/Obj_EncProgramClick'), 7)

		WebUI.verifyElementNotClickable(findTestObject('Object Repository/Transitions/Obj_EncProgramInput'), FailureHandling.STOP_ON_FAILURE)


		WebUI.getAttribute(findTestObject('Object Repository/Transitions/Obj_EncProgramInput'), EncProgram)
	}

	@Then("I should see (.*) Label filter1")
	public void AdmissionTypeFilterVerify1(String AdmissionType) {

		WebUI.verifyElementText(findTestObject('Object Repository/Transitions/Obj_AdmissionTypeLabel'), AdmissionType)
	}
	@Then("I should see (.*) Label filter2")
	public void EncTypeFilterVerify2(String EncType) {

		WebUI.verifyElementText(findTestObject('Object Repository/Transitions/Obj_EncTypeLabel'), EncType)
	}
	@Then("I should see (.*) Label filter3")
	public void NotifTypeFilterVerify3(String NotifType) {

		WebUI.verifyElementText(findTestObject('Object Repository/Transitions/Obj_NotifyTypeLabel'), NotifType)
	}

	@Then("I should see (.*) is auto selected as EncType by selecting BPCI")
	public void EncTypeValidations(String EncType) {

		WebUI.verifyElementText(findTestObject('Object Repository/Transitions/Obj_EncTypeValueVerify'), EncType)
	}


	@Then("I should see facility label as (.*) on transition grid")
	public void I_should_see_FacilityLabel(String ReceivingFacility) {


		WebUI.scrollToElement(findTestObject('Object Repository/Transitions/Obj_ReceivingFacilityColVerify'),5)

		String Actual_Facility = WebUI.getText(findTestObject('Object Repository/Transitions/Obj_ReceivingFacilityColVerify'))

		if(Actual_Facility.is(ReceivingFacility)) {

			WebUI.verifyMatch(Actual_Facility, ReceivingFacility, false)
		}
	}

	@Then("I should see default (.*) as encounter program")
	public void EncProgramDefaultAll(String EncProgram) {

		WebUI.verifyElementText(findTestObject('Object Repository/Transitions/Obj_EncProgramInput'), EncProgram)
	}

	@Then("I select (.*) from Enc Type")
	public void i_select_EncType(String EncType) {
		'I click on EncType field'
		WebUI.click(EncTypeClick)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//li[text()="'+EncType+'"]'
		obj.selectdropdown(frame,xpath)
		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/LeftFiltersObjects/OR_ClearDates/Obj_ClearBeforeDate'), 1)
		WebUI.scrollToElement(findTestObject('Object Repository/Transitions/Obj_EncTypeClick'), 5)
	}

	@Then("I should see (.*) is auto selected as EncType by selecting CJR")
	public void EncTypeValidationsCJR(String EncType) {

		WebUI.verifyElementText(findTestObject('Object Repository/Transitions/Obj_EncTypeValueVerify'), EncType)
	}

	@And("I click on save cohort from dropdown")
	public void CoHortSaveDropdown() {


		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHortBTNDropdownClick'))
		Thread.sleep(1000)
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHortBTNSave'))
		Thread.sleep(1000)
	}

	@Then("I should see (.*) as save cohort as")
	public void VerifyCoHortTitle(String Title) {

		String actualTitle = WebUI.getText(findTestObject("Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHortSaveTitle"))
		WebUI.verifyMatch(actualTitle, Title, false)
	}

	@When("I enter (.*) as cohort name")
	public void EnterCoHortName(String CoHortName) {

		Thread.sleep(2000)
		WebUI.setText(findTestObject("Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHort_SaveInput"), CoHortName)
	}

	@And("I click on cohort save button")
	public void CoHortSaveBTNs() {

		Thread.sleep(2000)
		WebUI.click(findTestObject("Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHortSave"))
		Thread.sleep(1000)
	}

	@Then("I should see (.*) as cohort message")
	public void VerifyCoHortMessage(String Message) {

		String actualMessage = WebUI.getText(findTestObject("Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHortSaveMessage"))
		WebUI.verifyMatch(actualMessage, Message, false)
	}

	@And("I click on cross button to delete cohort from dropdown")
	public void CoHortDeleteBTNDropdown() {

		Thread.sleep(2000)
		WebUI.click(findTestObject("Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHortDropdownClick"))
		Thread.sleep(1000)
		WebUI.click(findTestObject("Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHortDeleteClick"))
	}
	@And("I click on proceed button to confirm delete cohort")
	public void CoHortProceedBtn() {

		Thread.sleep(2000)
		WebUI.click(findTestObject("Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHortProceedBTN"))
		Thread.sleep(3000)
	}

	@And("I select added cohort from dropdown")
	public void CoHortSelect() {

		Thread.sleep(2000)
		WebUI.click(findTestObject("Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHortDropdownSelect"))
		Thread.sleep(3000)
	}

	@Then("I should see (.*) on the patient grid as Columns")
	public void I_should_see_ERVisit_filters(String Columns) {


		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyERVisitScroll'),5)

		"ER Visits Verify"
		String Actual_Col = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyERVisit'))
		WebUI.verifyMatch(Actual_Col, Columns, false)
	}

	@Then("I should see (.*) on the patient grid as Column")
	public void I_should_see_IPAdmission_filters(String Columns) {


		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyIPAdmissionScroll'),5)

		"IP Admission Verify"
		String Actual_Col2 = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyIPAdmission'))
		WebUI.verifyMatch(Actual_Col2, Columns, false)
	}

	@Then("I should be able to (.*) this column")
	void open_Care_Coordination_SortingERVisit(String Sort) {

		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyIPAdmissionScroll'),5)

		Thread.sleep(2000)

		'click on arrow'
		WebUI.waitForElementPresent(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisitArrowClick'), 0)
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisitArrowClick'))
		'Dscending'
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisit_SortDsc'))

		Thread.sleep(6000)

		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyIPAdmissionScroll'),5)

		'click on arrow'
		WebUI.waitForElementPresent(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisitArrowClick'), 0)
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisitArrowClick'))
		'Ascending'
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisit_SortAsc'))

		Thread.sleep(6000)
	}

	@Then("I should be able to (.*) this columns")
	void open_Care_Coordination_SortingIPAdmission(String Sort) {

		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyIPAdmissionScroll'),20)


		Thread.sleep(2000)

		'click on arrow'
		WebUI.waitForElementPresent(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IPAdmissionArrowClick'), 3)
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IPAdmissionArrowClick'))
		'Dscending'
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IPAdmission_SortDsc'))

		Thread.sleep(6000)

		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyIPAdmissionScroll'),5)

		'click on arrow'
		WebUI.waitForElementPresent(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IPAdmissionArrowClick'), 3)
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IPAdmissionArrowClick'))
		'Ascending'
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IPAdmission_SortAsc'))

		Thread.sleep(6000)
	}


	@And("I enter (.*) as assign date of transition grid")
	void open_Care_Coordination_Date(String Date) {

		Thread.sleep(4000)

		WebUI.setText(findTestObject("Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_Transitioni_AssignDate"), Date)
		Thread.sleep(2000)
	}


	@And("I filter using (.*) on transition IP Admission grid")
	void open_Care_Coordination_IP(String Filters) {

		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyIPAdmissionScroll'),5)

		Thread.sleep(2000)

		'click on ER Visits Arrow'
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IPAdmissionArrowClick'))

		'move to the ER Visits filter label'
		Thread.sleep(2000)
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IP_FilterLabel'))

		'Input the patient name'
		Thread.sleep(4000)
		WebUI.clearText(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IP_Filter_Input'))

		WebUI.sendKeys(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IP_Filter_Input'),Filters)

		'Click on filter button'
		WebUI.click(findTestObject('Object Repository/OR_OpenPatient/filterbutton'))

		Thread.sleep(2000)
	}

	@And("I filter using (.*) on transition ER Visits grid")
	void open_Care_Coordination_ERVisit(String Filters) {

		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyIPAdmissionScroll'),5)

		Thread.sleep(2000)

		'click on ER Visits Arrow'
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisitArrowClick'))

		'move to the ER Visits filter label'
		Thread.sleep(2000)
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisit_FilterLabel'))

		'Input the patient name'
		Thread.sleep(4000)
		WebUI.clearText(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisit_Filter_Input'))

		WebUI.sendKeys(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisit_Filter_Input'),Filters)

		'Click on filter button'
		WebUI.click(findTestObject('Object Repository/OR_OpenPatient/filterbutton'))

		Thread.sleep(2000)
	}

	@Then("I should see (.*) as care cordination LOB")
	public void I_should_see_LOB(String LOB) {


		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/GridObjects/Obj_LOB'),10)

		String Actual_Facility = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/OR_LeftFilters/GridObjects/Obj_LOB'))

		if(Actual_Facility.is(LOB)) {

			WebUI.verifyMatch(Actual_Facility, LOB, false)
		}
	}

	@Then("I enter (.*) as Attempt1_BY")
	public void I_EnterAttempt1_BY(String Attempt1_BY) {


		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_BY1'),10)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_BY1'), Attempt1_BY)
	}

	@Then("I enter (.*) as Attempt2_BY")
	public void I_EnterAttempt2_BY(String Attempt2_BY) {


		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_BY2'),10)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_BY2'), Attempt2_BY)
	}

	@Then("I enter (.*) as DischargeDiagnosis")
	public void I_EnterDischargeDiagnosis(String DischargeDiagnosis) {


		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_DischargeDiagnosis'),10)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_DischargeDiagnosis'), DischargeDiagnosis)
	}

	@And("I select TreatmentAttemptBeforeEmergencyRoomVisit:(.*)")
	public void i_select_TreatmentAttemptBeforeEmergencyRoomVisit(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_TreatmentAttemptBeforeEmergencyRoomVisit_Click'), 10)
		'I click on privacy field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_TreatmentAttemptBeforeEmergencyRoomVisit_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"treatmentAttemptedBeforeEmergencyRoomVisit_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_TreatmentAttemptBeforeEmergencyRoomVisit_Input'), "TreatmentAttemptBeforeEmergencyRoomVisit")
	}

	@And("I select DidYouContactYourPCPPriorEmergencyRoomVisit:(.*)")
	public void i_select_DidYouContactYourPCPPriorEmergencyRoomVisit(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_DidYouContactYourPCPPriorEmergencyRoomVisit_Click'), 10)
		'I click on privacy field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_DidYouContactYourPCPPriorEmergencyRoomVisit_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"didYouContactYourPCPPriorToEmergencyRoomVisit_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)
	}

	@And("I select DidYouContactYourPCPPriorEmergencyRoomVisit_SubField:(.*)")
	public void i_select_DidYouContactYourPCPPriorEmergencyRoomVisit_SubField(String DidYouContactYourPCPPriorEmergencyRoomVisit_SubField) {

		'I click on privacy field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_DidYouContactYourPCPPriorEmergencyRoomVisit_SubClick'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"whatDidThePCPInstructYouToDo__listbox")]//li[contains(text(),"'+DidYouContactYourPCPPriorEmergencyRoomVisit_SubField+'")]'
		obj.selectdropdown(frame,xpath)
	}


	@And("I select Anyquestionsondischargeinstructions:(.*)")
	public void i_select_Anyquestionsondischargeinstructions(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_Anyquestionsondischargeinstructions_Click'), 10)
		'I click on privacy field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_Anyquestionsondischargeinstructions_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"anyQuestionsOnDischargeInstructions_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_Anyquestionsondischargeinstructions_Input'), "Anyquestionsondischargeinstructions")
	}

	@And("I select AnyNewPrescriptions:(.*)")
	public void i_select_AnyNewPrescriptions(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_AnyNewPrescriptions_Click'), 10)
		'I click on privacy field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_AnyNewPrescriptions_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"anyNewPrescriptions_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_AnyNewPrescriptions_Input'), "AnyNewPrescriptions")
	}

	@And("I select Haveyoupickedupandstartedyourprescriptions:(.*)")
	public void i_select_Haveyoupickedupandstartedyourprescriptions(String Value) {

		'I click on privacy field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_Haveyoupickedupandstartedyourprescriptions_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"haveYouPickedUpAndStartedYourPrescriptions_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)
	}

	@And("I select StillHavingSymptoms:(.*)")
	public void i_select_StillHavingSymptoms(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_StillHavingSymptomsClick'), 10)
		'I click on privacy field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_StillHavingSymptomsClick'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"stillHavingSymptoms_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_StillHavingSymptomsInput'), "StillHavingSymptoms")
	}

	@And("I select AnyOtherQuestions:(.*)")
	public void i_select_AnyOtherQuestions(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_AnyOtherQuestionsClick'), 10)
		'I click on privacy field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_AnyOtherQuestionsClick'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"anyOtherQuestions_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_AnyOtherQuestionsInput'), "AnyOtherQuestions")
	}

	@And("I select RemindedpatienttocallPCPpriortofutureemergencyroomvisits:(.*)")
	public void i_select_RemindedpatienttocallPCPpriortofutureemergencyroomvisits(String Value) {

		'I click on privacy field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_RemindedpatienttocallPCPpriortofutureemergencyroomvisits_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"remindedPatientToCallPCPPriorToFutureEmergencyRoomVisits_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)
	}


	@And("I select RemindedpatientofExtendedhoursandoncallphysiciannumber:(.*)")
	public void i_select_RemindedpatientofExtendedhoursandoncallphysiciannumber(String Value) {

		'I click on privacy field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/ERFollowUpCalls/Obj_RemindedpatientofExtendedhoursandoncallphysiciannumber_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"remindedPatientOfExtendedHoursAndOnCallPhysicianNumber_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)
	}

	@And("I select AreYouAbleToTakeCareYourselftAtHome:(.*)")
	public void i_select_AreYouAbleToTakeCareYourselftAtHome(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_AreYouAbleToTakeCareYourselftAtHome_Click'),5)

		'I click on AreYouAbleToTakeCareYourselftAtHome field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_AreYouAbleToTakeCareYourselftAtHome_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"areYouAbleToTakeCareOfYourselfNowThatYouAreHome_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)
	}

	@And("I select Doyouhavesomeonetotakecareofyourmedicalneeds:(.*)")
	public void i_select_Doyouhavesomeonetotakecareofyourmedicalneeds(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Doyouhavesomeonetotakecareofyourmedicalneeds_Click'),5)

		'I click on Doyouhavesomeonetotakecareofyourmedicalneeds field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Doyouhavesomeonetotakecareofyourmedicalneeds_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"doYouHaveSomeoneToTakeCareOfYourMedicalNeedsIfNecessary_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Doyouhavesomeonetotakecareofyourmedicalneeds_Input'), "Doyouhavesomeonetotakecareofyourmedicalneeds")
	}

	@And("I select Doyouhaveanyquestionsaboutyourdischargeinstructions:(.*)")
	public void i_select_Doyouhaveanyquestionsaboutyourdischargeinstructions(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Doyouhaveanyquestionsaboutyourdischargeinstructions_Click'),5)

		'I click on Doyouhaveanyquestionsaboutyourdischargeinstructions field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Doyouhaveanyquestionsaboutyourdischargeinstructions_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"doYouHaveAnyQuestionsAboutYourDischargeInstructions_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Doyouhaveanyquestionsaboutyourdischargeinstructions_Input'), "Doyouhaveanyquestionsaboutyourdischargeinstructions")
	}

	@And("I select Medicationsreconciledthisencounter:(.*)")
	public void i_select_Medicationsreconciledthisencounter(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Medicationsreconciledthisencounter_Click'),5)

		'I click on Medicationsreconciledthisencounter field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Medicationsreconciledthisencounter_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"medicationsReconciledThisEncounter_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Medicationsreconciledthisencounter_Input'), "Medicationsreconciledthisencounter")
	}

	@And("I select Anynewmedicationsprescribedatdischargeorchangestohomemedications:(.*)")
	public void i_select_Anynewmedicationsprescribedatdischargeorchangestohomemedications(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Anynewmedicationsprescribedatdischargeorchangestohomemedications_Click'),5)

		'I click on Anynewmedicationsprescribedatdischargeorchangestohomemedications field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Anynewmedicationsprescribedatdischargeorchangestohomemedications_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"anyNewMedicationsPrescribedAtDischargeOrChangesToHomeMedications_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Anynewmedicationsprescribedatdischargeorchangestohomemedications_Input'), "Anynewmedicationsprescribedatdischargeorchangestohomemedications")
	}

	@And("I select Anynormalhomemedicationsdiscontinuedatdischarge:(.*)")
	public void i_select_Anynormalhomemedicationsdiscontinuedatdischarge(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Anynormalhomemedicationsdiscontinuedatdischarge_Click'),5)

		'I click on Anynormalhomemedicationsdiscontinuedatdischarge field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Anynormalhomemedicationsdiscontinuedatdischarge_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"anyNormalHomeMedicationsDiscontinuedAtDischarge_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Anynormalhomemedicationsdiscontinuedatdischarge_Input'), "Anynormalhomemedicationsdiscontinuedatdischarge")
	}

	@And("I select Areyoutakingyourmedicationsasprescribed:(.*)")
	public void i_select_Areyoutakingyourmedicationsasprescribed(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Areyoutakingyourmedicationsasprescribed_Click'),5)

		'I click on Areyoutakingyourmedicationsasprescribed field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Areyoutakingyourmedicationsasprescribed_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"areYouTakingYourMedicationsAsPrescribed_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Areyoutakingyourmedicationsasprescribed_Input'), "Areyoutakingyourmedicationsasprescribed")
	}

	@And("I select Doyouhaveanyconcernsaboutyourhealthatthistime:(.*)")
	public void i_select_Doyouhaveanyconcernsaboutyourhealthatthistime(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Doyouhaveanyconcernsaboutyourhealthatthistime_Click'),5)

		'I click on Doyouhaveanyconcernsaboutyourhealthatthistime field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Doyouhaveanyconcernsaboutyourhealthatthistime_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"doYouHaveAnyConcernsAboutYourHealthAtThisTime_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_Doyouhaveanyconcernsaboutyourhealthatthistime_Input'), "Doyouhaveanyconcernsaboutyourhealthatthistime")
	}

	@And("I select AreYouScheduledForAnyFollowUpTestsTherapiesMedicalEquipmentOrHomeCare:(.*)")
	public void i_select_AreYouScheduledForAnyFollowUpTestsTherapiesMedicalEquipmentOrHomeCare(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_AreYouScheduledForAnyFollowUpTestsTherapiesMedicalEquipmentOrHomeCare_Click'),5)

		'I click on AreYouScheduledForAnyFollowUpTestsTherapiesMedicalEquipmentOrHomeCare field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_AreYouScheduledForAnyFollowUpTestsTherapiesMedicalEquipmentOrHomeCare_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"areYouScheduledForAnyFollowUpTestsTherapiesMedicalEquipmentOrHomeCare_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_AreYouScheduledForAnyFollowUpTestsTherapiesMedicalEquipmentOrHomeCare_Input'), "AreYouScheduledForAnyFollowUpTestsTherapiesMedicalEquipmentOrHomeCare")
	}

	@And("I select referralsNeeded:(.*)")
	public void i_select_referralsNeeded(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_referralsNeeded_Click'),5)

		'I click on referralsNeeded field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_referralsNeeded_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"referralsNeeded_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)
	}

	@And("I select referralsNeeded_SubField:(.*)")
	public void i_select_referralsNeeded_SubField(String referralsNeeded_SubField) {

		'I click on referralsNeeded_SubField field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_referralsNeeded_SubFieldClick'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"referralsNeededIfYes_listbox")]//li[contains(text(),"'+referralsNeeded_SubField+'")]'
		obj.selectdropdown(frame,xpath)
	}

	@And("I enter (.*) as followUpAppointmentDate")
	public void i_EnterfollowUpAppointmentDate(String AppointmentDate) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_followUpAppointmentDate'),5)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_followUpAppointmentDate'), AppointmentDate)
	}

	@And("I enter (.*) as whatSymptomsWouldPromptYouToCallYourPhysicianBeforeYourNextScheduledAppointment")
	public void i_EnterwhatSymptomsWouldPromptYouToCallYourPhysicianBeforeYourNextScheduledAppointment(String whatSymptomsWouldPromptYouToCallYourPhysicianBeforeYourNextScheduledAppointment) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_whatSymptomsWouldPromptYouToCallYourPhysicianBeforeYourNextScheduledAppointment'),5)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_whatSymptomsWouldPromptYouToCallYourPhysicianBeforeYourNextScheduledAppointment'), whatSymptomsWouldPromptYouToCallYourPhysicianBeforeYourNextScheduledAppointment)
	}

	@And("I enter (.*) as whenDidYouLastDiscussYourAdvancedMedicalDirectivesWithYourPhysician")
	public void i_EnterwhenDidYouLastDiscussYourAdvancedMedicalDirectivesWithYourPhysician(String whenDidYouLastDiscussYourAdvancedMedicalDirectivesWithYourPhysician) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_whenDidYouLastDiscussYourAdvancedMedicalDirectivesWithYourPhysician'),5)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_whenDidYouLastDiscussYourAdvancedMedicalDirectivesWithYourPhysician'), whenDidYouLastDiscussYourAdvancedMedicalDirectivesWithYourPhysician)
	}

	@And("I select DischargeValue:(.*)")
	public void i_select_DischargeValue(String DischargeValue) {

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_DischargeDate'), "09062022")

		'I click on DischargeValue field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/HospitalDischargeCalls/Obj_DischargeDate_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"dischargedPatientType_listbox")]//li[contains(text(),"'+DischargeValue+'")]'
		obj.selectdropdown(frame,xpath)
	}

	@And("I enter (.*) as SurgeryDate")
	public void i_EnterSurgeryDate(String SurgeryDate) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_SurgeryDate'),5)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_SurgeryDate'), SurgeryDate)
	}

	@And("I enter (.*) as DischargeDate")
	public void i_EnterDischargeDate(String DateOfDischarge) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_DischargeDate'),5)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_DischargeDate'), DateOfDischarge)
	}

	@And("I enter (.*) as TypeOfSurgery")
	public void i_EnterTypeOfSurgery(String TypeOfSurgery) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_TypeOfSurgery'),5)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_TypeOfSurgery'), TypeOfSurgery)
	}

	@And("I select WasThisPlannedSurgery:(.*)")
	public void i_select_WasThisPlannedSurgery(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_WasThisPlannedSurgery_Click'), 5)

		'I click on WasThisPlannedSurgery field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_WasThisPlannedSurgery_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"wasThisAPlannedSurgery_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)
	}

	@And("I select InGeneralHowAreYouFeelingAfterYourSurgery:(.*)")
	public void i_select_InGeneralHowAreYouFeelingAfterYourSurgery(String InGeneralHowAreYouFeelingAfterYourSurgery) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_InGeneralHowAreYouFeelingAfterYourSurgery_Click'), 5)

		'I click on InGeneralHowAreYouFeelingAfterYourSurgery field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_InGeneralHowAreYouFeelingAfterYourSurgery_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"inGeneralHowAreYouFeelingAfterYourSurgery_listbox")]//li[contains(text(),"'+InGeneralHowAreYouFeelingAfterYourSurgery+'")]'
		obj.selectdropdown(frame,xpath)
	}

	@And("I select AreYouToleratingYourDiet:(.*)")
	public void i_select_AreYouToleratingYourDiet(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_AreYouToleratingYourDiet_Click'), 5)

		'I click on AreYouToleratingYourDiet field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_AreYouToleratingYourDiet_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"areYouToleratingYourDiet_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)
	}

	@And("I select AnyNauseaVomiting:(.*)")
	public void i_select_AnyNauseaVomiting(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_AnyNauseaVomiting_Click'), 5)

		'I click on AnyNauseaVomiting field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_AnyNauseaVomiting_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"anyNauseaOrVomiting_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)
	}

	@And("I select IsYourIncisionCovered:(.*)")
	public void i_select_IsYourIncisionCovered(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_IsYourIncisionCovered_Click'), 5)

		'I click on IsYourIncisionCovered field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_IsYourIncisionCovered_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"isYourIncisionCovered_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)
	}

	@And("I select IsYourIncisionCovered_SubField:(.*)")
	public void i_select_IsYourIncisionCovered_SubField(String IsYourIncisionCovered_SubField) {

		'I click on IsYourIncisionCovered_SubField field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_IsYourIncisionCovered_SubFieldClick'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"whatTypeOfDressing_listbox")]//li[contains(text(),"'+IsYourIncisionCovered_SubField+'")]'
		obj.selectdropdown(frame,xpath)
	}
	@And("I select AnyDrainageFromIncision:(.*)")
	public void i_select_AnyDrainageFromIncision(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_AnyDrainageFromIncision_Click'), 5)

		'I click on AnyDrainageFromIncision field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_AnyDrainageFromIncision_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"anyDrainageFromTheIncision_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_AnyDrainageFromIncision_Type_SubField'), "Surface Drainage System")
		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_AnyDrainageFromIncision_Amount_SubField'), "50000")
	}

	@And("I select AnyFever:(.*)")
	public void i_select_AnyFever(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_AnyFever_Click'), 5)

		'I click on AnyFever field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_AnyFever_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"anyFever_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_AnyFever_Temp_SubField'), "98")
	}

	@And("I select HowWouldYourPainOnScale:(.*)")
	public void i_select_HowWouldYourPainOnScale(String HowWouldYourPainOnScale) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_HowWouldYourPainOnScale_Click'), 5)

		'I click on HowWouldYourPainOnScale field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_HowWouldYourPainOnScale_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"howWouldYouRateYourPainOnAScaleOf1to10_listbox")]//li[contains(text(),"'+HowWouldYourPainOnScale+'")]'
		obj.selectdropdown(frame,xpath)
	}

	@And("I select TakingAnyPainMedications:(.*)")
	public void i_select_TakingAnyPainMedications(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_TakingAnyPainMedications_Click'), 5)

		'I click on TakingAnyPainMedications field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_TakingAnyPainMedications_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"takingAnyPainMedication_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_TakingAnyPainMedications_Medicaton_SubField'), "Panadol")
		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_TakingAnyPainMedications_LastDose_SubField'), "3")
	}

	@And("I select AreYouWalking:(.*)")
	public void i_select_AreYouWalkings(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_AreYouWalking_Click'), 5)

		'I click on AreYouWalking field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_AreYouWalking_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"areYouWalking_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_AreYouWalking_HowManyTimes_SubField'), "2")
		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_AreYouWalking_Distance_SubField'), "20km")
	}

	@And("I select HavingBowelMovements:(.*)")
	public void i_select_HavingBowelMovements(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_HavingBowelMovements_Click'), 5)

		'I click on HavingBowelMovements field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_HavingBowelMovements_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"havingBowelMovements_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_HavingBowelMovements_WhenWasLastBM_SubField'), "03152021 03:20 AM")
	}

	@And("I select UrinatingNormally:(.*)")
	public void i_select_UrinatingNormally(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_UrinatingNormally_Click'), 5)

		'I click on UrinatingNormally field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_UrinatingNormally_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"urinatingNormally_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)
	}
	@And("I select UsingIncentiveSpirometer:(.*)")
	public void i_select_UsingIncentiveSpirometer(String Value) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_UsingIncentiveSpirometer_Click'), 5)

		'I click on UsingIncentiveSpirometer field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_UsingIncentiveSpirometer_Click'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"usingIncentiveSpirometer_listbox")]//li[contains(text(),"'+Value+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_UsingIncentiveSpirometer_HowManyTimesPerDay_SubField'), "2")

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_UsingIncentiveSpirometer_NumberReached_SubField'), "5")
	}

	@And("I select HowManyHoursOfSleepAreYouGettingNowThatYourAreHome:(.*)")
	public void i_select_HowManyHoursOfSleepAreYouGettingNowThatYourAreHome(String HowManyHoursOfSleepAreYouGettingNowThatYourAreHome) {

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_HowManyHoursOfSleepAreYouGettingNowThatYourAreHome_FirstField'), HowManyHoursOfSleepAreYouGettingNowThatYourAreHome)
		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_HowManyHoursOfSleepAreYouGettingNowThatYourAreHome_SubFieldClick'), 5)

		'I click on HowManyHoursOfSleepAreYouGettingNowThatYourAreHome field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SurgicalFollowUpCalls/Obj_HowManyHoursOfSleepAreYouGettingNowThatYourAreHome_SubFieldClick'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[contains(@id,"howManyHoursOfSleepAreYouGettingNowThatYouAreHomeType_listbox")]//li[contains(text(),"'+HowManyHoursOfSleepAreYouGettingNowThatYourAreHome+'")]'
		obj.selectdropdown(frame,xpath)
	}
}