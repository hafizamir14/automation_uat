package patientGrid

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys

import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import utility_Functions.UtilityFunctions



public class SD_SNFWaiver {

	TestObject frame=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/CareManagementForm/Obj_CCMFrame')
	UtilityFunctions obj=new UtilityFunctions();

	@And("I click on SNF Waiver")
	public void I_click_on_SNF_Waiver() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_SNFWaiver'))
	}

	

	@And("I should see SNFWaiver care form title")
	public void I_should_see_SNF_Care_Title() {

		String actual_Title = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PopupValidation/Obj_SNFWaiverTitle'))

		WebUI.verifyEqual(actual_Title, 'SNF Waiver Form')

	}

	@And("I enter SNF (.*) as datetime")
	public void i_enter_SNF_AM_as_datetime(String DateTime) {

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_StartDate'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_StartDate'), DateTime)
	}

	@And("I enter SNF (.*) as aprima id")
	public void i_enter_SNF_as_aprima_id(String AprimaID) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_AprimaID'), AprimaID)
	}

	@And("I select SNF referral source")
	public void i_select_referralsource() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_ReferralSourceClick'))
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Page_Welcome to Persivia/li_Family'))
	}

	@And("I enter SNF (.*) as referral source date")
	public void i_enter_SNF_as_referral_source_date(String ReferralSourceDate) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_SNFReferralDate'), ReferralSourceDate)
	}

	@And("I select nurse navigator")
	public void i_select_nurse_navigator() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_NurseNavigatorClick'))
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_SelectNurseNavigator'))
	}

	@And("I select VPA office location")
	public void i_select_VPA_office_location() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_VPAOfficeLocationClick'))
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_SelectVPALocation'))
	}

	@And("I select patient status")
	public void i_select_patientstatus() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_PatientStatusClick'))
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_SelectPatientStatus'))
	}

	@And("I select patient attribute to ACO")
	public void i_select_patient_attribute_to_ACO() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Page_Welcome to Persivia/Page_Welcome to Persivia/Obj_ACOSelect'))

	}

	@And("I click patient attribute to ACO")
	public void i_click_patient_attribute_to_ACO() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_PatientAttributeACOClick'))
	}

	@And("I select patient patient attributeIAH")
	public void i_select_patient_attribute_to_IAH() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_PatientAttributedtoIAH'))

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Page_Welcome to Persivia/Page_Welcome to Persivia/Obj_IAHSelect'))

	}

	@When("I should see SNF Waiver data")
	public void ShouldSeeSNFWaiver() {

		WebUI.verifyElementPresent(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_SNFDate'), 3)

		Thread.sleep(4000)
	}


	@And("I enter (.*) as last seen by VPA provider")
	public void i_enter_SNF_Mehmood_Anjum_as_last_seen_by_VPA_provider(String LastSeenByVPAProvider) {

		WebUI.setText(	findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_LastSeenByVPA'), LastSeenByVPAProvider)
	}

	@And("I enter (.*) as referral name")
	public void i_enter_referralname(String ReferralName) {

		WebUI.setText(	findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_ReferralName'), ReferralName)
	}

	@And("I enter SNF (.*) as SNF admission date")
	public void i_enter_SNF_as_SNF_admission_date(String AdmissionDate) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_SNFAdmissionDate'), AdmissionDate)
	}

	@And("I enter SNF (.*) as facility")
	public void i_enter_SNF_MHPN_as_facility(String Facility) {

		WebUI.scrollToElement(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_Facility'), 5)

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_Facility'), Facility)
	}

	@And("I enter SNF (.*) as estimated LOS days")
	public void i_enter_SNF_as_estimated_LOS_days(String EstimatedLOS) {

		WebUI.scrollToElement(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_EstimatedLOSDate'), 5)


		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_EstimatedLOSDate'), EstimatedLOS)
	}

	@And("I enter SNF (.*) as discharge date")
	public void i_enter_SNF_as_discharge_date(String DischargeDate) {

		WebUI.scrollToElement(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_DischargeDate'), 5)

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_DischargeDate'), DischargeDate)
	}

	@And("I enter SNF (.*) as actual LOS days")
	public void i_enter_SNF_as_actual_LOS_days(String ActualLOS) {

		WebUI.scrollToElement(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_ActualLOSDate'), 5)

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_ActualLOSDate'), ActualLOS)
	}

	@And("I enter SNF (.*) as SNF comments")
	public void i_enter_SNF_Lorem_Ipsum_is_simply_dummy_text_of_the_printing_and_typesetting_industry_as_SNF_comments(String Comments) {

		WebUI.setText(	findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_SNFComment'), Comments)
	}


	@And("I should see (.*) as SNFWaiver")
	public void i_shouldSeeSNFDate(String DateTime) {
		'Verify Date'
		String date = DateTime.substring(0, 8)

		String date1 = DateTime.substring(0, 8)

		WebUI.verifyMatch(date, date1, false)

	}

	@And("I enter SNF (.*) as AdmittingDiagnosis")
	public void i_enter_AdmittingDiagnosis(String AdmittingDiagnosis) {

		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SNF/Obj_AdmittingDiagnosisClick'))

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SNF/Obj_DischargeDispositionSelect'), AdmittingDiagnosis)
		Thread.sleep(1000)
		WebUI.sendKeys(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SNF/Obj_DischargeDispositionSelect'), Keys.chord(Keys.ENTER))


	}

	@And("I select reason for admission")
	public void i_select_Reasonadmission() {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SNF/Obj_ReasonForAdmissionClick'), 5)

		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SNF/Obj_ReasonForAdmissionClick'))
		Thread.sleep(1000)
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SNF/Obj_ReasonForAdmissionSelect'))

	}



	@And("I select SNF state")
	public void i_select_state() {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SNF/Obj_StatusClick'), 5)

		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SNF/Obj_StatusClick'))
		Thread.sleep(1000)
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SNF/Obj_StatusSelect'))
	}



	@And("I select discharge disposition SNF")
	public void i_select_dischargedisposition() {


		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SNF/Obj_DischargeDispositionClick'), 5)

		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SNF/Obj_DischargeDispositionClick'))
		Thread.sleep(1000)
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SNF/Obj_DischargeDispositionSelect'))

	}



	@And("I select post SNF checkIn")
	public void i_select_postcheckIn() {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SNF/Obj_PostSNFCheckInClick'), 5)

		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SNF/Obj_PostSNFCheckInClick'))
		Thread.sleep(1000)
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SNF/Obj_PostSNFCheckInSelect'))

	}

	@Then("I select noteType:(.*)")
	public void i_select_privacy_privacy(String NoteType) {
		'I click on NoteType field'
		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/SNF/Obj_NoteTypeClick'))
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//ul[@id="note-type_listbox"]//li[contains(text(), "'+NoteType+'")]'

		obj.selectdropdown(frame,xpath)
	}

	@And("I drag chat list2")
	public void I_drag_chat_list2() {


		if(WebUI.verifyElementPresent((findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/SoapNotes/Obj_DragChatList')), 5, FailureHandling.OPTIONAL)) {

			WebUI.dragAndDropByOffset(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/SoapNotes/Obj_DragChatList'), 30, 50)
			WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/SoapNotes/Obj_DragChatList'))
		}else {

			WebUI.getUrl();
		}
	}

}
