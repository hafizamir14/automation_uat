package patientGrid

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.Given
import utility_Functions.UtilityFunctions


public class SD_PatientGrid_LegacyMode {

	UtilityFunctions obj=new UtilityFunctions();
	TestObject frame=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/CareManagementForm/Obj_CCMFrame')


	@Given("I navigate to patient gridLegacyMode")
	public void I_navigate_to_patient_gridLegacyMode() {



		Thread.sleep(10000)

		if(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_LegacyMode/Obj_PatientClickLegacy'), 15, FailureHandling.OPTIONAL)) {

			'I click on Enterprise field'
			WebUI.click(findTestObject('Object Repository/OR_LegacyMode/Obj_EnterPriseClick'))
			WebUI.click(findTestObject('Object Repository/OR_LegacyMode/Obj_EnterPrise_Select'))

			if(WebUI.verifyElementPresent((findTestObject('OR_Alert/Obj_Hide')), 3, FailureHandling.OPTIONAL)) {

				WebUI.click(findTestObject('OR_Alert/Obj_Hide'))
			}else {
				WebUI.getUrl()
			}

			Thread.sleep(2000)
			WebUI.click(findTestObject('Object Repository/OR_LegacyMode/Obj_PatientClickLegacy'))
			Thread.sleep(1000)

			WebUI.click(findTestObject('Object Repository/OR_LegacyMode/Obj_PatientClickLegacy'))

			Thread.sleep(3000)


			if(WebUI.verifyElementPresent((findTestObject('OR_Alert/Obj_Hide')), 3, FailureHandling.OPTIONAL)) {

				WebUI.click(findTestObject('OR_Alert/Obj_Hide'))
			}else {
				WebUI.getUrl()
			}
		}else if(WebUI.verifyElementPresent(findTestObject('OR_LandingPage/OR_CMR/Obj_CMR'), 10, FailureHandling.OPTIONAL)){

			Thread.sleep(3000)
			WebUI.selectOptionByLabel(findTestObject('OR_HomePage/Obj_Enterprise'), "Harmony Cares Medical Group", false)
			Thread.sleep(2000)

			if(WebUI.verifyElementPresent((findTestObject('OR_Alert/Obj_Hide')), 3, FailureHandling.OPTIONAL)) {

				WebUI.click(findTestObject('OR_Alert/Obj_Hide'))
			}else {
				WebUI.getUrl()
			}


			WebUI.mouseOver(findTestObject('OR_LandingPage/OR_CMR/Obj_CMR'))
			Thread.sleep(1000)

			WebUI.click(findTestObject('Object Repository/OR_LandingPage/OR_CMR/Obj_Patients'))

			if(WebUI.verifyElementPresent((findTestObject('OR_Alert/Obj_Hide')), 3, FailureHandling.OPTIONAL)) {

				WebUI.click(findTestObject('OR_Alert/Obj_Hide'))
			}else {
				WebUI.getUrl()
			}

			Thread.sleep(14000)
		}
	}
}
