package patientGrid
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import utility_Functions.UtilityFunctions


class SD_BlockByAppointmentAnalyticsOutreach {

	TestObject YearlySelectionClick=findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_YearlySelectionClick')
	TestObject TimePeriodClick=findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_Analytics_TimePeriodClick')
	TestObject MonthSelectionClick=findTestObject('Object Repository/Analytics/Obj_Analytics_MonthSelectionClick')
	TestObject frame=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/CareManagementForm/Obj_CCMFrame')
	UtilityFunctions obj=new UtilityFunctions();

	@When("I click on appointmentOutReachTab")
	public void ClickOnappointmentOutReachTab() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_Analytics_AppointmentOutReach'))
	}

	@And("I select Time Period1:(.*)")
	public void i_select_TimePeriod1(String TimePeriod) {
		'I click on TimePeriod field'
		WebUI.click(TimePeriodClick)
		'I select value from the dropdown'
		String xpath= '(//ul[contains(@id, "timePeriod")])[1]//li[contains(text(), "'+TimePeriod+'")]'

		obj.selectdropdown(frame,xpath)
	}

	@And("I navigate to Blocks By Appt Provider grid")
	public void NavigateToApptProvider() {

		Thread.sleep(5000)
		WebUI.scrollToElement(findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_NavigationToApptBlock'), 20)
		Thread.sleep(3000)
	}


	@Then("I should see (.*) as Block By Appt Provider chart")
	public void ShouldSeeApptProviderChart(String Title) {

		String actualText = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_Analytics_BlockApptChartVerify'))

		WebUI.verifyMatch(actualText, Title, false)
	}


	@And("I select year selection:(.*)")
	public void i_select_YearSelection(String YearSelection) {
		'I click on YearSelection field'
		WebUI.click(YearlySelectionClick)
		'I select value from the dropdown'
		String xpath= '//ul[@id="yearList_listbox"]//li[contains(text(), "'+YearSelection+'")]'

		obj.selectdropdown(frame,xpath)
	}

	@And("I select month selection:(.*)")
	public void i_select_monthSelection(String MonthSelection) {
		'I click on YearSelection field'
		WebUI.click(MonthSelectionClick)
		'I select value from the dropdown'
		String xpath= '//ul[contains(@id,"monthList")]//li[contains(text(), "'+MonthSelection+'")]'

		obj.selectdropdown(frame,xpath)
	}

	@Then("I should see (.*) as secondary heading")
	public void ShouldSeeCTMemberChart(String MonthSelection) {

		String actualText = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_Analytics_VerifyMonth_Heading'))

		if(!actualText.contains(MonthSelection)){

			WebUI.verifyMatch(actualText, MonthSelection, false)
		}
	}

	@Then("I should see (.*) as HIE secondary heading")
	public void ShouldSeeHEI(String MonthSelection) {

		Thread.sleep(7000)

		String actualText = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_Analytics_VerifyHEIMonth_Heading'))

		if(!actualText.contains(MonthSelection)){

			WebUI.verifyMatch(actualText, MonthSelection, false)
		}
	}



	@When("I click on export appointment report")
	public void ClickOnappointmentExport() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_ClickOnDots'))
		Thread.sleep(2000)
		WebUI.scrollToElement(findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_ClickOnExport'), 10)
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_ClickOnExport'))
	}

	@Then("I click on nonZero number and verify total number of blocks in drill")
	public void ShouldSeeTotalNumberOfBlocks() {

		WebDriver driver;


		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_ClickOnDots'))

		Thread.sleep(2000)

		WebUI.scrollToElement(findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_NonZeroNumberVerify'), 30)

		Thread.sleep(4000)

		String actualBlocks = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_NonZeroNumberVerify'))

		println('Blocks: ' + actualBlocks)

		Thread.sleep(1000)

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_ClickOnNonZeroNumber'))

		WebUI.switchToFrame(frame, 2)

		Thread.sleep(2000)

		driver = DriverFactory.getWebDriver()
		WebElement Table = driver.findElement(By.xpath("(//div[contains(@id, 'PatientReportGridDrillDownPaged')])/div[3]/table/tbody"))
		List<WebElement> rows_table = Table.findElements(By.tagName('tr'))
		int rows_count = rows_table.size()
		println('No. of rows: ' + rows_count)

		WebUI.verifyEqual(actualBlocks, rows_count)

		Thread.sleep(2000)
	}

	@When("I click on export appointment report2")
	public void ClickOnappointmentExport2() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_ClickOnExport2'))
	}


	@Then("I should see the info (.*), (.*), (.*), (.*) and (.*) with (.*) and (.*)")
	public void ShouldSeeDrillTitle(String BlockName,String UserName,String Date, StartDate,String EndDate,String Duration,String Comments) {

		WebUI.switchToDefaultContent()

		String actualBlockName = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_VerifyBlockName'))

		WebUI.verifyMatch(actualBlockName, BlockName, false)

		String actualUserName = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_VerifyUserBlock'))

		WebUI.verifyMatch(actualUserName, UserName, false)

		String actualDate1 = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_VerifyDate'))

		WebUI.verifyMatch(actualDate1, Date, false)

		String actualStartDate = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_VerifyStartDate'))

		WebUI.verifyMatch(actualStartDate, StartDate, false)

		String actualEndDate = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_VerifyEndDate'))

		WebUI.verifyMatch(actualEndDate, EndDate, false)

		String actualDuration = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_VerifyDuration'))

		WebUI.verifyMatch(actualDuration, Duration, false)

		String actualComments = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/BlocksByApptAnalytics_Appointment_OutReach/Obj_VerifyComment'))

		WebUI.verifyMatch(actualComments, Comments, false)



		Thread.sleep(2000)
	}
}