package patientGrid

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys

import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import cucumber.api.java.en.Then
import utility_Functions.UtilityFunctions

public class SD_PalliativeCare {

	UtilityFunctions obj=new UtilityFunctions();
	TestObject priotityobj=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/CareManagementForm/selectpriority')
	TestObject frame=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/CareManagementForm/Obj_CCMFrame')


	@And("I click on plus form")
	public void I_click_on_plus_form_button() {

		Thread.sleep(3000)
		WebUI.click(findTestObject('OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_FormsBtn'))
	}

	@And("I click on palliative care")
	public void I_click_on_palliative_care() {

		WebUI.click(findTestObject('OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_PalliativeCare'))
	}



	@And("I should see (.*) as patient name in form popup")
	public void I_should_see_form_popup_with_relevant_data(String Patient) {


		String actual_Name = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PopupValidation/Obj_Name'))

		WebUI.verifyEqual(actual_Name, Patient)

		//		String actual_Gender = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PopupValidation/Obj_Gender'))
		//
		//		WebUI.verifyEqual(actual_Gender, 'Male')
		//
		//		String actual_MRN = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PopupValidation/Obj_MRN'))
		//
		//		WebUI.verifyEqual(actual_MRN, 'MRN :  EntMerging505')
	}


	@And("I should see palliative care form title")
	public void I_should_see_palliative_Care_Title() {

		String actual_Title = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PopupValidation/Obj_PalliativeCareTitle'))

		WebUI.verifyEqual(actual_Title, 'Palliative Care Form')
	}


	@Then("I select PalliativePriority:(.*)")
	public void i_select_priority_priority2(String priority) {
		'I click on priority field'
		WebUI.click(priotityobj)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='(//ul[contains(@id, "palliative_care_priority")])[2]//li[contains(text(), "'+priority+'")]'

		obj.selectdropdown(frame,xpath)
	}



	@And("I enter (.*) as date and time")
	public void I_enter_as_date_and_time(String DateTime) {

		WebUI.clearText(findTestObject('OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_DateTime_Input'))
		WebUI.setText(findTestObject('OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_DateTime_Input'), DateTime)
	}

	@And("I enter (.*) as date of discharge")
	public void I_enter_as_DateOfDischarge(String DateOfDischarge) {

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_DateOfDischarge'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_DateOfDischarge'), DateOfDischarge)
	}

	@And("I select payer")
	public void I_select_payer() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_payer'))
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_selectPayer'))
	}

	@And("I enter (.*) as Payer Referral Date")
	public void I_enter_as_as_Payer_Referral_Date(String PayerReferralDate) {

		WebUI.scrollToElement(findTestObject("Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_PayerReferalDate"), 3)

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_PayerReferalDate'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_PayerReferalDate'), PayerReferralDate)
	}

	@And("I enter (.*) as Medicare ID")
	public void I_enter_as_as_Medicare_ID(String MedicareID) {

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_MedicareID'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_MedicareID'), MedicareID)
	}

	@And("I enter (.*) as Medicaid ID")
	public void I_enter_as_as_Medicaid_ID(String MedicaidID) {

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_MediID'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_MediID'), MedicaidID)
	}

	@And("I enter (.*) as Marketplace ID")
	public void I_enter_as_as_Marketplace_ID(String MarketplaceID) {

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_MarketPlaceID'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_MarketPlaceID'), MarketplaceID)
	}

	@And("I enter (.*) as Insurance ID")
	public void I_enter_as_Insurance_ID(String InsuranceID) {

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_InsuranceID'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_InsuranceID'), InsuranceID)
	}

	@And("I select patient type")
	public void I_select_patient_type() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_PatientType'))

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_SelectPatientType'))
	}

	@And("I select contracted patient")
	public void I_select_contractedpatient() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_ContaractedPatient'))

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_SelectContaractedPatient'))
	}

	@And("I select vpa office location")
	public void I_select_vpaOfficeLocation() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_VPALocation'))

		Thread.sleep(1000)
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_SelectVPALocation'))
	}

	@And("I enter (.*) as health plan care manager")
	public void I_enter_as_health_plan_care_manager(String HealthPlanCareManager) {


		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_HealthCarePlanManager'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_HealthCarePlanManager'), HealthPlanCareManager)
	}

	@And("I enter (.*) as palliative care nurse practitioner")
	public void I_enter_Palliative_Care_Nurse_Practitioner(String PalliativeCareNursePractitioner) {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_PalliativeCareNurseClick'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_PalliativeCareNurseinput'), PalliativeCareNursePractitioner)

		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/SNFWaiver/Obj_PalliativeCareNurseinput'),Keys.chord(Keys.ENTER))
	}

	@And("I enter (.*) as diagnoses")
	public void I_enter_as_diagnoses(String Diagnoses) {


		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_DiagnosesInput'))

		Thread.sleep(1000)
		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_DiagnosesInput'), Diagnoses)
		Thread.sleep(1000)

		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_DiagnosesInput'),Keys.chord(Keys.ENTER))

		//WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_SelectDiagnoses'))
	}


	@And("I select referral source")
	public void I_select_referral_source() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_ReferralSource'))

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_SelectReferralSource'))
	}

	@And("I select status")
	public void I_select_status() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_Status'))

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_SelectStatus'))
	}



	@And("I enter (.*) as NP palliative care assessment date")
	public void I_enter_as_NPpalliativecareassessmentdate(String NPPalliativeCareAssessmentDate) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_NPPalliativeCareAssessmentDate'), NPPalliativeCareAssessmentDate)
	}

	@And("I enter (.*) as palliative care start date")
	public void I_enter_as_palliativecarestartdate(String PalliativeCareStartDate) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_PalliativeCareStartDate'), PalliativeCareStartDate)
	}

	@And("I enter (.*) as NP third visit date")
	public void I_enter_as_NPthirdvisitdate(String NP3rdVisitDate) {


		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_np3rdVisitDate'), NP3rdVisitDate)
	}

	@And("I enter (.*) as most recent NP palliative care visit date")
	public void I_enter_as_MostrecentNPPalliativeCareVisitDate(String MostrecentNPPalliativeCareVisitDate) {



		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_MostrecentNPPalliativeCareVisitDate'), MostrecentNPPalliativeCareVisitDate)
	}

	@And("I enter (.*) as advance care planning date")
	public void I_enter_as_AdvanceCarePlanningDate(String AdvanceCarePlanningDate) {



		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_AdvanceCarePlanningDate'), AdvanceCarePlanningDate)
	}

	@And("I enter (.*) as IDG date")
	public void I_enter_as_IDGDate(String IDGDate) {



		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_IDGDate'), IDGDate)
	}


	@And("I select Inpatient admit or ER visit while on Palliative Care Services in the last thirty days")
	public void I_select_inpatientadmitorervisti() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_InpatientadmitorERvisitwhileonPalliativeCareServicesinthelast30days'))

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_SelectInpatientadmitorERvisitwhileonPalliativeCareServicesinthelast30days'))
	}

	@And("I enter (.*) as Date of Inpatient admit or ER visit while on Palliative Care services within the last thirty days")
	public void I_enter_as_dateofinpatientadmit(String Date_of_Inpatient_admit_or_ER_visit_while_on_PalliativeCareservices_within_the_last30days) {



		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_DateofInpatientadmitorERvisitwhileonPalliativeCareserviceswithinthelast30days'), Date_of_Inpatient_admit_or_ER_visit_while_on_PalliativeCareservices_within_the_last30days)
	}


	@And("I enter (.*) as chaplin referral date")
	public void I_enter_as_ChaplinReferralDate(String ChaplinReferralDate) {



		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_ChaplinReferralDate'), ChaplinReferralDate)
	}


	@And("I enter (.*) as chaplin assessment date")
	public void I_enter_as_ChaplinAssessmentDate(String ChaplinAssessmentDate) {



		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_ChaplinAssessmentDate'), ChaplinAssessmentDate)
	}


	@And("I enter (.*) as Most recent Chaplin Visit Date")
	public void I_enter_as_MostrecentChaplinVisitDate(String MostrecentChaplinVisitDate) {



		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_MostrecentChaplinVisitDate'), MostrecentChaplinVisitDate)
	}


	@And("I enter (.*) as social work referral date")
	public void I_enter_as_SocialWorkReferralDate(String SocialWorkReferralDate) {



		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_SocialWorkReferralDate'), SocialWorkReferralDate)
	}


	@And("I enter (.*) as social work assessment date")
	public void I_enter_as_SocialWorkAssessmentDate(String SocialWorkAssessmentDate) {



		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_SocialWorkAssessmentDate'), SocialWorkAssessmentDate)
	}


	@And("I enter (.*) as most recent social work visit date")
	public void I_enter_as_MostRecentSocialWorkVisitDate(String MostRecentSocialWorkVisitDate) {



		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_MostRecentSocialWorkVisitDate'), MostRecentSocialWorkVisitDate)
	}


	@And("I enter (.*) as hospice referral date")
	public void I_enter_as_HospiceReferralDate(String HospiceReferralDate) {



		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_HospiceReferralDate'), HospiceReferralDate)
	}


	@And("I enter (.*) as hospice admit date")
	public void I_enter_as_HospiceAdmitDate(String HospiceAdmitDate) {



		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_HospiceAdmitDate'), HospiceAdmitDate)
	}


	@And("I enter (.*) as hospice company")
	public void I_enter_as_HospiceCompany(String HospiceCompany) {


		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_HospiceCompany'), HospiceCompany)
	}

	@And("I enter (.*) as call outreach Placed by")
	public void I_enter_as_Call1OutreachPlacedby(String CallOutreachPlacedby) {


		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_Call1OutreachPlacedby'), CallOutreachPlacedby)
	}

	@And("I enter (.*) as call date")
	public void I_enter_as_Call1Date(String CallDate) {


		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_Call1Date'), CallDate)
	}

	@And("I enter (.*) as palliative comments")
	public void I_enter_as_Comment(String Comments) {



		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_Comment'), Comments)
	}

	@And("I click on save button to save patient data")
	public void I_click_on_save_button_to_save_patient_data() {

		WebUI.scrollToElement(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_SaveBtn'), 0)
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/PalliativeCareForm/Obj_SaveBtn'))
		Thread.sleep(2000)
	}

	@And("I should see current date of notes form")
	public void CurrentDate() {

		//
		//		String actual = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/TransitionOfCareForm/ER Follow Up Calls/Obj_CurrentDateForms'))
		//
		//		String date = actual.substring(0, 2)
		//
		//		String date1 = actual.substring(0, 2)
		//
		//		if(actual.println(actual)) {
		//
		//			WebUI.verifyMatch(date, date1, false)
		//		}else {
		//
		//			print("SOrry")
		//		}
	}


	@And("I should see (.*) as palliativeCare")
	public void I_click_DateOfPalliative(String DateTime) {

		'Verify Date'
		String date = DateTime.substring(0, 8)

		String date1 = DateTime.substring(0, 8)

		WebUI.verifyMatch(date, date1, false)
	}


	@And("I select call1 outcome")
	public void I_select_outcome1() {

		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_Call1OutcomeClick'))

		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_Call1OutcomeSelect'))
	}

	@And("I select call2 outcome")
	public void I_select_outcome2() {

		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_Call2OutcomeClick'))

		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_Call2OutcomeSelect'))
	}

	@And("I select call3 outcome")
	public void I_select_outcome3() {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_Call3OutcomeClick'), 7)

		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_Call3OutcomeClick'))

		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_Call3OutcomeSelect'))
	}

	@And("I enter (.*) as call2 outreach Placed by")
	public void I_enter_as_Call2OutreachPlacedby(String CallOutreachPlacedby) {


		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_Call2OutreachPlacedby'), CallOutreachPlacedby)
	}

	@And("I enter (.*) as call3 outreach Placed by")
	public void I_enter_as_Call3OutreachPlacedby(String CallOutreachPlacedby) {


		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_Call3OutreachPlacedby'), CallOutreachPlacedby)
	}

	@And("I enter (.*) as call2 date")
	public void I_enter_as_call2(String CallDate) {


		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_Call2Date'), CallDate)
	}

	@And("I enter (.*) as call3 date")
	public void I_enter_as_call3(String CallDate) {


		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_Call3Date'), CallDate)
	}


	@And("I enter (.*) as MostRecentMonthlyTelephoneCall")
	public void I_enter_as_MostRecentMonthlyTelephoneCall(String MostRecentMonthlyTelephoneCall) {


		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_MostRecentMonthlyTelephoneCall'), MostRecentMonthlyTelephoneCall)
	}

	@And("I select hospitallization or death while on palliative care services")
	public void I_select_hospitalizationsDeath() {

		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_hospitallizationClick'))

		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_hospitallizationSelect'))
	}

	@And("I enter (.*) as date of death")
	public void I_enter_as_DateOfDeath(String DateOfDeath) {


		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_DateOfDeath'), DateOfDeath)
	}

	@And("I enter (.*) as Referral for Date Home Health Care")
	public void I_enter_as_ReferralDateHomeHealthCare(String ReferralDateHomeHealthCare) {


		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_ReferralDateHomeHealthCare'), ReferralDateHomeHealthCare)
	}

	@And("I enter (.*) as home health care admit date")
	public void I_enter_as_HomeHealthCareAdmitDate(String HomeHealthCareAdmitDate) {


		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_HomeHealthCareAdmitDate'), HomeHealthCareAdmitDate)
	}

	@And("I enter (.*) as home health care company")
	public void I_enter_as_HomeHealthCareCompany(String HomeHealthCareCompany) {


		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_HomeHealthCareCompany'), HomeHealthCareCompany)
	}

	@And("I enter (.*) as VPAProvider")
	public void I_enter_as_VPAProvider(String VPAProvider) {

		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_VPAProviderClick'), 5)

		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_VPAProviderClick'))

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_VPAProviderInput'), VPAProvider)

		WebUI.sendKeys(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_VPAProviderInput'), Keys.chord(Keys.ENTER))
	}

	@And("I enter (.*) as mmss")
	public void I_enter_as_mmss(String MMSS) {


		WebUI.scrollToElement(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_MMSSClick'), 5)

		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_MMSSClick'))

		WebUI.setText(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_MMSSInput'), MMSS)

		WebUI.sendKeys(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/PalliativeCare/Obj_MMSSInput'), Keys.chord(Keys.ENTER))
	}
}
