package patientGrid
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys

import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import utility_Functions.UtilityFunctions

class SD_CarePlan_FutureAppointment {


	UtilityFunctions obj=new UtilityFunctions();
	TestObject frame=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/CareManagementForm/Obj_CCMFrame')


	@And("I click on future appointment plus button")
	public void I_click_appointmentPlus_button() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CP_ADDFutureAppointment/Obj_FutureAppointment_Plus'))
	}

	@And("I hover over on future appointment")
	public void I_HoverOver_appointment() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CP_ADDFutureAppointment/Obj_FutureAppointment_Hover'))
	}



	@And("I enter (.*) as future appointment reason")
	public void AppointmentReason(String FutureAppointmentReason) {

		Thread.sleep(1000)
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CP_ADDFutureAppointment/Obj_FutureAppointment_ReasonInputClick'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CP_ADDFutureAppointment/Obj_FutureAppointment_ReasonInput'), FutureAppointmentReason)
		Thread.sleep(1000)
		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CP_ADDFutureAppointment/Obj_FutureAppointment_ReasonInput'),Keys.chord(Keys.ENTER))
	}

	@And("I enter (.*) as future appointment participant")
	public void AppointmentParticipant(String FA_Participant) {

		Thread.sleep(1000)
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CP_ADDFutureAppointment/Obj_FutureAppointment_ParticipantClick'))
		Thread.sleep(1000)

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CP_ADDFutureAppointment/Obj_FutureAppointment_ParticipantInput'), FA_Participant)
		Thread.sleep(1000)
		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CP_ADDFutureAppointment/Obj_FutureAppointment_ParticipantInput'),Keys.chord(Keys.ENTER))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CP_ADDFutureAppointment/Obj_FutureAppointment_ParticipantInput'), FA_Participant)
		Thread.sleep(1000)
		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CP_ADDFutureAppointment/Obj_FutureAppointment_ParticipantInput'),Keys.chord(Keys.ENTER))
	}
	@And("I enter (.*) as future appointment date")
	public void AppointmentDate(String FP_Date) {

		Thread.sleep(1000)
		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CP_ADDFutureAppointment/Obj_FutureAppointment_Date'), FP_Date)
		Thread.sleep(1000)
	}
	@And("I enter (.*) as future appointment start time")
	public void AppointmentStartTime(String FA_StartTime) {

		Thread.sleep(1000)
		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CP_ADDFutureAppointment/Obj_FutureAppointment_StartTime'), FA_StartTime)
		Thread.sleep(1000)
	}
	@And("I enter (.*) as future appointment end time")
	public void AppointmentEndTime(String FA_EndTime) {

		Thread.sleep(1000)
		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CP_ADDFutureAppointment/Obj_FutureAppointment_EndTime'), FA_EndTime)
		Thread.sleep(1000)
	}

	@And("I should see (.*) and (.*) and (.*) as updated future appointment")
	public void I_click_ShouldSeeFutureAppointmentData(String FutureAppointmentReason, String FA_Participant, String FP_Date) {


		WebUI.scrollToElement(findTestObject("Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CP_ADDFutureAppointment/Obj_FutureAppointmentValidation_Reason"), 3)
		String Actual_Reason = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CP_ADDFutureAppointment/Obj_FutureAppointmentValidation_Reason'))

		if(!Actual_Reason.contains(FutureAppointmentReason)){

			WebUI.verifyMatch(Actual_Reason, FutureAppointmentReason, false)
		}

		String Actual_Participant = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CP_ADDFutureAppointment/Obj_FutureAppointmentValidation_Participant'))

		if(!Actual_Participant.contains(FA_Participant)){

			WebUI.verifyMatch(Actual_Participant, FA_Participant, false)
		}


		String date = FP_Date.substring(0, 2)

		String actual_DateOfServiceDate = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CP_ADDFutureAppointment/Obj_FutureAppointmentValidation_Date'))

		String date1 = FP_Date.substring(0, 2)

		WebUI.verifyMatch(date, date1, false)
	}
}