package patientGrid
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import cucumber.api.java.en.Then



class SD_CarePlan_DateFormat {


	@And("I click on patient from grid")
	public void ClickPatientGrid() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CarePlan_DateFormat/Obj_ClickPatient'))

		Thread.sleep(8000)
	}

	@And("I save dateformat from PWB")
	public void savedate() {

		String actualDateFormat = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CarePlan_DateFormat/Obj_CopyDateFormat'))
	}

	@Then("I verified date format is as expected")
	public void verifydateformat() {

		String ExpectedDateFormat = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CarePlan_DateFormat/Obj_ValidateDateFormat'))
		String actualDateFormat = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/CarePlan_DateFormat/Obj_CopyDateFormat'))

		if(!ExpectedDateFormat.contains(actualDateFormat)){

			WebUI.verifyMatch(actualDateFormat, ExpectedDateFormat, false)
		}
	}
}