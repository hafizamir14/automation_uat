package patientGrid
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import customs_Keywords.CustomsMethods
import utility_Functions.UtilityFunctions



class SD_Analytics {

	UtilityFunctions obj=new UtilityFunctions();
	TestObject frame=findTestObject('Object Repository/OR_OpenPatient/frame')
	TestObject FormTypeSelectionClick=findTestObject('Object Repository/Analytics/Obj_PMC_CLickOnFormType')
	TestObject FormFacilitySelectionClick=findTestObject('Object Repository/Analytics/Obj_PMC_CLickOnFormFacility')
	TestObject FormFacilityInput=findTestObject('Object Repository/Analytics/Obj_PMC_EnterFormFacility')

	TestObject TimePeriodClick=findTestObject('Object Repository/Analytics/Obj_Analytics_TimePeriodClick')



	@And("I select Time Period as:(.*)")
	public void i_select_TimePeriodAS(String TimePeriod) {
		'I click on TimePeriod field'

		WebUI.click(TimePeriodClick)
		'I select value from the dropdown'
		String xpath= '(//ul[contains(@id, "timePeriod")])[1]//li[contains(text(), "'+TimePeriod+'")]'

		obj.selectdropdown(frame,xpath)
	}

	@And("I click on nonZero number")
	public void ClickOnNonZeroLink() {

		if(WebUI.verifyElementPresent(findTestObject('Object Repository/Analytics/Obj_Document_ClickOnDots'), 1)) {

			WebUI.click(findTestObject('Object Repository/Analytics/Obj_Document_ClickOnDots'))
			WebUI.click(findTestObject('Object Repository/Analytics/Obj_Analytics_ClickOnNonZeroNumber'))
		}else {

			WebUI.click(findTestObject('Object Repository/Analytics/Obj_Analytics_ClickOnNonZeroNumber'))
		}
	}

	@Then("I should see the info (.*), (.*), (.*) and (.*) as doc")
	public void ShouldSeeDrillTitle2(String Patient,String LOB,String MRN,String Document) {

		WebUI.switchToDefaultContent()

		String actualPatient = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_Analytics_VerifyPatientColumn'))

		WebUI.verifyMatch(actualPatient, Patient, false)

		String actualLOB = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_Analytics_VerifyLOBColumn'))

		WebUI.verifyMatch(actualLOB, LOB, false)

		String actualMRN = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_Analytics_VerifyMRNColumn'))

		WebUI.verifyMatch(actualMRN, MRN, false)

		String actualDocument = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_Analytics_VerifyDocumentColumn'))

		WebUI.verifyMatch(actualDocument, Document, false)





		Thread.sleep(2000)
	}

	@And("I filter patient:(.*) from task grid")
	public void filter_patients(String Patient) {

		'click on arrow'
		WebUI.waitForElementPresent(findTestObject('Object Repository/Analytics/Obj_CLickPatientArrow'), 2)
		WebUI.click(findTestObject('Object Repository/Analytics/Obj_CLickPatientArrow'))

		'move to the filter label'
		Thread.sleep(2000)
		WebUI.click(findTestObject('Object Repository/Analytics/Obj_MoveFilterLabel'))

		'Input the patient name'
		Thread.sleep(2000)

		WebUI.clearText(findTestObject('Object Repository/Analytics/Obj_InputPatient'))

		WebUI.sendKeys(findTestObject('Object Repository/Analytics/Obj_InputPatient'),Patient)

		'Click on filter button'
		WebUI.click(findTestObject('Object Repository/Analytics/Obj_CLickOnFilterBTN'))
	}

	@Then("I should see (.*) is remain in filters")
	public void filterPatient_RemainOnGrid(String Patient) {


		'click on arrow'
		WebUI.waitForElementPresent(findTestObject('Object Repository/Analytics/Obj_CLickPatientArrow'), 2)
		WebUI.click(findTestObject('Object Repository/Analytics/Obj_CLickPatientArrow'))

		'move to the filter label'
		Thread.sleep(2000)
		WebUI.click(findTestObject('Object Repository/Analytics/Obj_MoveFilterLabel'))

		Thread.sleep(2000)

		'Verify patient Filter'
		Thread.sleep(2000)
		String ActualText = WebUI.getAttribute(findTestObject('Object Repository/Analytics/Obj_InputPatient'), 'value')

		WebUI.verifyMatch(ActualText, Patient, false)

		'Click on Clear button'
		WebUI.click(findTestObject('Object Repository/Analytics/Obj_CLickOnClearBTN'))

		Thread.sleep(2000)
	}

	@And("I click on export report and i should see exported file with DocumentTitle")
	public void I_should_see_exportedFileDoc() {


		Thread.sleep(2000)
		WebUI.scrollToElement(findTestObject('Object Repository/Analytics/Obj_VerifyDocTitle'), 10)

		String ActualDocTitle = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_VerifyDocTitle'))

		WebUI.click(findTestObject('Object Repository/Analytics/Obj_CLickOnExportBTN'))

		if(WebUI.verifyElementPresent(findTestObject('Object Repository/Analytics/Obj_CLickOnTHUsageReport'), 1)) {

			WebUI.click(findTestObject('Object Repository/Analytics/Obj_CLickOnTHUsageReport'))
		}else {

			WebUI.click(findTestObject('Object Repository/Analytics/Obj_CLickOnExportBTN'))
		}

		File downloadFolder = new File("C:\\Users\\hafiz.amir\\Downloads")
		List namesOfFiles = Arrays.asList(downloadFolder.list())
		if(namesOfFiles.contains(ActualDocTitle)) {
			println "Success"
		}
		else {
			println "Failure"
		}
	}

	@And("I click on ReportTab")
	public void ClickOnDocReportTab() {

		WebUI.click(findTestObject('Object Repository/Analytics/Obj_CLickOnReportTab'))
	}

	@And("I should see (.*) as Label1 and (.*) as Label2")
	public void ShouldSeeDefaultGridLabels(String Label1, String Label2) {

		if(WebUI.verifyElementPresent(findTestObject('Object Repository/Analytics/Obj_Document_ClickOnDots'), 1)) {

			WebUI.click(findTestObject('Object Repository/Analytics/Obj_Document_ClickOnDots'))
			Thread.sleep(2000)

			String actualLabel1 = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_Analytics_DocumentTypeTextVerify'))
			WebUI.verifyMatch(actualLabel1, Label1, false)

			String actualLabel2 = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_Analytics_NoOfDocumentsVerify'))
			WebUI.verifyMatch(actualLabel2, Label2, false)

			Thread.sleep(2000)
		}else {

			String actualLabel1 = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_Analytics_DocumentTypeTextVerify'))
			WebUI.verifyMatch(actualLabel1, Label1, false)

			String actualLabel2 = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_Analytics_NoOfDocumentsVerify'))
			WebUI.verifyMatch(actualLabel2, Label2, false)

			Thread.sleep(2000)
		}
	}

	@And("I should see the (.*) and (.*) options from column header dropdown")
	public void ShouldSeeSortsOptions(String Ascending, String Decending) {


		String actualLabel1 = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_VerifySortAscOption'))
		WebUI.verifyMatch(actualLabel1, Ascending, false)

		String actualLabel2 = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_VerifySortDescOption'))
		WebUI.verifyMatch(actualLabel2, Decending, false)

		Thread.sleep(2000)
	}

	@And("I should see the (.*) options from column header dropdown as ColumnConfiguration")
	public void ShouldSeeColumnConfigurationOptions(String ColumnConfiguration) {


		String actualColumnConfiguration = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_VerifyColumnsOption'))
		WebUI.verifyMatch(actualColumnConfiguration, ColumnConfiguration, false)

		Thread.sleep(2000)
	}

	@And("I should see the (.*) options from column header dropdown as filters")
	public void ShouldSeefiltersOptions(String Filter) {


		String actualfilters = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_VerifyFilterOption'))
		WebUI.verifyMatch(actualfilters, Filter, false)

		Thread.sleep(2000)
	}

	@And("I click on document report arrow")
	public void ClickOnDocArrow() {

		WebUI.click(findTestObject('Object Repository/Analytics/Obj_CLickOnAnalyticsDocArrow'))
	}

	@Then("I should see the ring bell notification icon on top right corner")
	public void VerifyNotificationIcon() {

		Thread.sleep(5000)
		WebUI.verifyElementPresent(findTestObject('Object Repository/Analytics/Obj_Notification_Icon'), 15)
	}

	@When("I click on notification icon")
	public void ClickOnNotificationIcon() {

		Thread.sleep(3000)
		WebUI.click(findTestObject('Object Repository/Analytics/Obj_Notification_Icon'))
	}

	@Then("I should see the label of alert notification of UnSign SOAP note")
	public void VerifyNotificationAlert() {

		Thread.sleep(5000)
		WebUI.verifyElementPresent(findTestObject('Object Repository/Analytics/Obj_NumberOfNotifications'), 10)
	}

	@And("I should see the label of (.*) is in bold letter")
	public void ShouldSeeNotificationTitle(String Notifications) {

		Thread.sleep(5000)
		String actual = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_NotificationTitle'))

		print(actual)

		WebUI.verifyMatch(actual, Notifications, false)

		Thread.sleep(3000)
	}


	@Then("I should see the list of all unsigned notifications list")
	public void ShouldSeeTotalNumberOfNotificationsList() {

		WebDriver driver;

		driver = DriverFactory.getWebDriver()
		WebElement Table = driver.findElement(By.xpath("(//ul[@class='notificationList'])[2]"))
		List<WebElement> rows_table = Table.findElements(By.tagName('li'))
		int rows_count = rows_table.size()
		println('No. of rows: ' + rows_count)

		Thread.sleep(2000)
	}

	@And("I click on Form_tab")
	public void ClickOnForm_Tab() {

		Thread.sleep(1000)
		WebUI.click(findTestObject('Object Repository/Analytics/Obj_PMC_CLickOnFormTab'))
	}

	@And("I click on HIE_tab")
	public void ClickOnHIE_tab() {

		Thread.sleep(1000)
		WebUI.click(findTestObject('Object Repository/Analytics/Obj_Analytics_ClickOnHIETab'))
	}

	@And("I navigate to Forms by CT Members grid")
	public void NavigateToFormByCTMember() {

		Thread.sleep(5000)
		WebUI.scrollToElement(findTestObject('Object Repository/Analytics/Obj_PMC_NavigateToCTMember'), 20)
	}

	@Then("I should see (.*) as CT Member chart")
	public void ShouldSeeCTMemberChart(String Title) {

		Thread.sleep(5000)

		String actualText = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_PMC_CTMemberTitle'))

		WebUI.verifyMatch(actualText, Title, false)
	}

	@Then("I should see (.*) as HIE Alert")
	public void ShouldSeeasHIEAlert(String Title) {

		String actualText = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_Analytics_HIEAlertTitle'))

		WebUI.verifyMatch(actualText, Title, false)
	}


	@When("I click on export HIE report")
	public void ClickOnHIEExport() {


		Thread.sleep(2000)
		WebUI.scrollToElement(findTestObject('Object Repository/Analytics/Obj_Forms_CLickOnExportBTN'), 10)
		WebUI.click(findTestObject('Object Repository/Analytics/Obj_Forms_CLickOnExportBTN'))
	}


	@When("I click on export CT Member report")
	public void ClickOnCTMemberExport() {

		if(WebUI.verifyElementPresent(findTestObject('Object Repository/Analytics/Obj_PMC_ClickOnDots'), 8)){

			WebUI.click(findTestObject('Object Repository/Analytics/Obj_PMC_ClickOnDots'))
		}
		else {
			Thread.sleep(2000)
			WebUI.scrollToElement(findTestObject('Object Repository/Analytics/Obj_Forms_CLickOnExportBTN'), 10)
			WebUI.click(findTestObject('Object Repository/Analytics/Obj_Forms_CLickOnExportBTN'))
		}
	}

	@Then("I click on nonZero number and verify total number of CTMembers in drill")
	public void ShouldSeeTotalNumberOfCTMembers() {

		WebDriver driver;


		WebUI.click(findTestObject('Object Repository/Analytics/Obj_PMC_ClickOnDots'))

		Thread.sleep(2000)

		WebUI.scrollToElement(findTestObject('Object Repository/Analytics/Obj_PMC_NonZeroNumberVerify'), 30)

		Thread.sleep(4000)

		String actualMembers = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_PMC_NonZeroNumberVerify'))

		println('Blocks: ' + actualMembers)

		Thread.sleep(1000)

		WebUI.click(findTestObject('Object Repository/Analytics/Obj_PMC_CLickOnNonZeroNumber'))

		Thread.sleep(4000)

		WebUI.switchToFrame(frame, 2)

		Thread.sleep(3000)

		driver = DriverFactory.getWebDriver()
		WebElement Table = driver.findElement(By.xpath("(//div[contains(@class, 'patient-detail full-height')])/div[3]/table//tbody"))
		List<WebElement> rows_table = Table.findElements(By.tagName('tr'))
		int rows_count = rows_table.size()
		println('No. of rows: ' + rows_count)

		WebUI.verifyEqual(actualMembers, rows_count)

		Thread.sleep(2000)
	}

	@And("I select Form Type:(.*)")
	public void i_select_YearSelection(String FormType) {
		'I click on FormType field'
		WebUI.click(FormTypeSelectionClick)
		'I select value from the dropdown'
		String xpath= '//ul[contains(@id, "enrollmentCategory")]//li[contains(text(), "'+FormType+'")]'

		obj.selectdropdown(frame,xpath)
	}

	@And("I select Form Facility:(.*)")
	public void i_select_FormFacility(String Facility) {
		'I click on Facility field'

		obj.ClickElement(FormFacilitySelectionClick)
		obj.setValues(FormFacilityInput, Facility)
		Thread.sleep(1000)
		obj.EnterKeys(FormFacilityInput)
		//		WebUI.sendKeys(FormFacilityInput, Keys.chord(Keys.ENTER))

	}

	@Then("I should see the (.*) as info")
	public void ShouldSeeDrillTitle2(String PatientInfo) {

		WebUI.switchToDefaultContent()

		String actualBlockName = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_PMC_VerifyPatientFormLabel'))

		WebUI.verifyMatch(actualBlockName, PatientInfo, false)


		Thread.sleep(2000)
	}


	@When("I click on export CT Member report2")
	public void ClickOnCTExport2() {

		WebUI.click(findTestObject('Object Repository/Analytics/Obj_PMC_Export2'))
	}

	@Then("I should see the col (.*) as doc")
	public void ShouldSeeDrillTitle3(String Patient) {

		WebUI.switchToDefaultContent()

		String actualBlockName = WebUI.getText(findTestObject('Object Repository/Analytics/Obj_Analytics_VerifyPatientColumn'))

		WebUI.verifyMatch(actualBlockName, Patient, false)


		Thread.sleep(2000)
	}

	@Then("I should see that Last Modified By will be hidden by default")
	public void ShouldNotSeeModifiedCol() {

		WebUI.switchToDefaultContent()

		WebUI.verifyElementNotVisible(findTestObject('Object Repository/Analytics/Obj_Analytics_Verify_LastModifiedColHidden'))

		Thread.sleep(2000)
	}

	@Then("I should be able to see Last Modified By columns on UI from column configuration")
	public void ShouldSeeModifiedCol_UI() {

		WebUI.switchToDefaultContent()

		'click on arrow'
		WebUI.waitForElementPresent(findTestObject('Object Repository/Analytics/Obj_Analytics_DocumentPatientColumnArrowClick'), 0)
		WebUI.click(findTestObject('Object Repository/Analytics/Obj_Analytics_DocumentPatientColumnArrowClick'))

		'move to the filter label'
		Thread.sleep(2000)
		WebUI.click(findTestObject('Object Repository/Analytics/Obj_Analytics_DocPatientColFilter'))

		WebUI.verifyElementVisible(findTestObject('Object Repository/Analytics/Obj_Analytics_DocumentVerifyCol_FromUI'))
	}
}

