package patientGrid
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys

import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import groovypackage.Methods
import utility_Functions.UtilityFunctions



public class SD_DFT_ProgramEnrollmentPlusTasks {

	UtilityFunctions obj=new UtilityFunctions();

	TestObject titleverify=findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_EligiblePopupTitleVerification')
	TestObject frame=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/CareManagementForm/Obj_CCMFrame')
	TestObject ReasonDropdown=findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ReasonDropdownClick')
	TestObject ConditionDropdown=findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ConditionFieldClick')
	TestObject ProblemDropdown=findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ProblemFieldDropdownClick')
	TestObject ReferralDropdown=findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ReferralSourceClick')
	TestObject StatusDropdown=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_CareGap/OR_Task/Obj_statusdropdown')
	TestObject OwnerDropdown=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_CareGap/Obj_Ownerdropdown')




	@And("I click on program enrollment dropdown")
	void I_Click_on_programEnrollment() {
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ProgramEnrollmentClick'))

		Thread.sleep(2000)
	}

	@And("I expand BHCM")
	void I_Click_on_BHCM() {
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_BHCMClick'))
		Thread.sleep(1000)
	}

	@And("I expand PACM")
	void I_Click_on_PACM() {

		Thread.sleep(2000)
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_PACMClick'))
		Thread.sleep(1000)
	}

	@And("I click on eligible link")
	void I_Click_on_EligibleLink() {
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_EligilbleClick'))
		Thread.sleep(2000)
	}
	@And("I click on eligible button")
	void I_Click_on_EligibleBTN() {
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_EligibleBTN'))
		Thread.sleep(2000)
	}

	@And("I should see popup with correct title")
	public void I_should_see_CorrectTitle() {

		String actual_Title = WebUI.getText(titleverify)

		WebUI.verifyEqual(actual_Title,'Reason')

		Thread.sleep(1000)
	}

	@And("I select reason:(.*)")
	public void i_select_reason(String Reason) {

		WebUI.click(ReasonDropdown)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='(//li[text()="'+Reason+'"])[2]'
		obj.selectdropdown(frame,xpath)
		Thread.sleep(2000)
	}

	@And("I select reason2:(.*)")
	public void i_select_reason2(String Reason) {

		WebUI.click(ReasonDropdown)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='(//li[text()="'+Reason+'"])[1]'
		obj.selectdropdown(frame,xpath)
		Thread.sleep(2000)
	}

	@And("I select problems:(.*)")
	public void i_select_problems(String Problems) {
		WebUI.click(ProblemDropdown)

		Thread.sleep(2000);

		WebUI.clearText(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ProblemFieldInput"))
		Thread.sleep(1000);

		WebUI.setText(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ProblemFieldInput"), Problems)
		Thread.sleep(1000);

		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ProblemFieldInput'), Keys.chord(Keys.ENTER))

		Thread.sleep(2000)
	}

	@And("I select referral source:(.*)")
	public void i_select_ReferralSource(String ReferralSource) {
		WebUI.click(ReferralDropdown)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//li[text()="'+ReferralSource+'"]'
		obj.selectdropdown(frame,xpath)
		Thread.sleep(2000)
	}

	@And("I enter (.*) as conditions")
	public void EnterCondition(String Conditions) {

		WebUI.click(ConditionDropdown)

		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ConditionFieldInput'), Keys.chord(Keys.BACK_SPACE))

		Thread.sleep(1000)

		WebUI.setText(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ConditionFieldInput"), Conditions)
		Thread.sleep(1000)
		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ConditionFieldInput'), Keys.chord(Keys.ENTER))
		Thread.sleep(2000)
	}

	@And("I enter (.*) as customs condition")
	public void EnterCustomsCondition(String Customs_Conditions) {

		WebUI.setText(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_CustomConditionInput"), Customs_Conditions)
		Thread.sleep(2000)
	}

	@Then("I should see the latest record (.*),(.*),(.*) after expanding the enrollment history")
	void EnrollmentHistoryverification(String status,String Reason,String Problems) {


		WebUI.scrollToElement(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ValidationStatus'), 20)

		String actualStatus = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ValidationStatus'))
		WebUI.verifyEqual(actualStatus, status)

		String actualReason = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ValidationReason'))
		WebUI.verifyEqual(actualReason, Reason)

		String actualProblem = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ValidationProblem'))
		WebUI.verifyEqual(actualProblem, Problems)

	}

	@Then("I should see the sequence of problem (.*) is sorted")
	void EnrollmentHistoryProblemverification(String Problems) {

		String actualProblem = WebUI.getText(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ValidationProblem"))
		WebUI.verifyEqual(actualProblem, Problems)
	}



	@When("I click on Task tab")
	void ClickOnTaskTab() {

		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_TaskBottomTabClick"))
	}

	@And("I click on Task main tab")
	void ClickOnTaskMainTab() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_TaskTabClick'))
	}

	@And("I select the patient that is associated with task")
	void ClickOnPatientNameAssociated() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_PatientNameClick'))
	}

	@And("I select the patient that is associated with task2")
	void ClickOnPatientNameAssociated2() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_PatientNameClick2'))
	}


	@And("I click on create task button")
	void ClickOnCreateTaskBTN() {

		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_AddTaskBTN"))
		Thread.sleep(1000)
		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_CustomTaskClick"))
		Thread.sleep(2000)
	}

	@Then("I should see Create New Task form with title")
	void VerifyCreateTaskTitleForm() {

		String actualTitle = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_TaskCreationTitleVerification'))
		WebUI.verifyEqual(actualTitle, "Create New Task")
	}

	@And("I click on save button to save task")
	void TaskSaveBTN() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_TaskSaveBTN'))
	}

	@And("I click on save button to update task")
	void TaskupdateBTN() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_TaskUpdateBTN'))
	}

	@And("I click on assignment edit button")
	void TaskEditBTN() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_EditBTNClick'))
	}


	@And("I enter (.*) as task field")
	void buttoncreatetask(String Taskname) {

		'Click on Add button'
		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_CareGap/OR_Task/Obj_taskfield'), Taskname)
	}


	@And("I enter (.*) as task start date field")
	void Strtdatetask(String Strtdate) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_CareGap/OR_Task/Obj_startdate'), Strtdate)
	}


	@And("I enter (.*) as task completed on date field")
	void Completeddatetask(String Completeddate) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_CareGap/OR_Task/Obj_datecompletedon'), Completeddate)
	}


	@And("I enter (.*) as task status field")
	void statustask(String status) {
		Thread.sleep(2000)
		WebUI.click(StatusDropdown)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='(//li[text()="'+status+'"])[2]'
		obj.selectdropdown(frame,xpath)
		Thread.sleep(2000)
	}


	@When("I enter (.*) as owner field")
	void ownertask(String owner) {


		Thread.sleep(2000)
		WebUI.click(OwnerDropdown)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='(//li[text()="'+owner+'"])[2]'
		obj.selectdropdown(frame,xpath)
		Thread.sleep(2000)
	}

	@When("I select assigned to from task")
	def Desriptionetask() {

		WebUI.click(findTestObject('Object Repository/Notes_Forms_NonMandatoryFieldsObjects/TelephonicOutreach/Obj_Task_AssignedToMeClick'))
	}



	@When("I enter Second_login credentials")
	public void SecondLogin() {

		WebUI.setText(findTestObject('Object Repository/OR_LoginPage/Obj_Username'), "hafiz@uat.com")

		WebUI.setText(findTestObject('Object Repository/OR_LoginPage/Obj_Password'), "Amir11")


		WebUI.click(findTestObject('Object Repository/OR_LoginPage/Obj_Signin'))

		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_HomePage/Obj_Enterprise'), 10)
		WebUI.click(findTestObject('Object Repository/OR_HomePage/Obj_Enterprise'))

		WebUI.click(findTestObject('Object Repository/OR_HomePage/Obj_SelectMcLarenHealthcare'))

		WebUI.mouseOver(findTestObject('OR_LandingPage/OR_CMR/Obj_CMR'))


		//UAT

		WebUI.click(findTestObject('Object Repository/OR_LandingPage/OR_CMR/Obj_PatientsUAT2'))


		WebUI.enableSmartWait()
		Thread.sleep(14000)



	}

	@When("I click on my assignment tab")
	public void AssignmentTabClick() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_AssignmentTabClick'))
	}

	@When("I click on Template tab")
	public void TemplateTab() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_NoteTemplateTab'))
	}

	@When("I click on Add Note tab")
	public void AddNoteTab() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_AddNoteTab'))
	}

	@When("I enter (.*) as templateName")
	public void TemplateName(String TemplateName) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_NoteTemplateTab_TemplateName'), TemplateName)
	}

	@When("I enter (.*) as TemplateBody")
	public void TemplateBody(String TemplateBody) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_NoteTemplateTab_TemplateBody'), TemplateBody)
	}

	@When("I click on save button to save template")
	public void TemplateSaveBTN() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_NoteTemplateTab_SaveBTN'))
	}

	@And("I click on complete button to save transition of care form data")
	public void I_CompleteBtn_Transition() {
		Thread.sleep(2000)

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_Notes_CompleteBTN'))
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_Notes_Complete_YesBTN'))


		Thread.sleep(4000)
	}


	@When("I enter (.*) as assigned to")
	def Assigned(String Assigned) {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_AssignedClick'))
		Thread.sleep(1000)

		WebUI.clearText(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_AssignedInput'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_AssignedInput'), Assigned)
		Thread.sleep(1000)
		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_AssignedInput'),Keys.chord(Keys.ENTER))
	}

	@When("I should see (.*) as modified by update")
	public void ModifiedByAssignment(String ModifiedBy) {

		String actualModified = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ValidateModifiedBy'))
		WebUI.verifyMatch(actualModified, ModifiedBy, false)

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_DeleteBTN'))
		Thread.sleep(1000)
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ConfirmDeleteBTN'))
	}

	@When("I should not edit or delete buttons")
	public void EditAndDeletebuttons() {

		WebUI.verifyElementNotVisible(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_EditBTNClick_Validation'))
		WebUI.verifyElementNotVisible(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_DeleteBTN_Validation'))
	}

	@When("I click on transitionForm print button")
	public void ClickPrintBTN() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_Notes_PrintBTN'))

		WebUI.switchToDefaultContent()
	}

	@When("I verify file is printed")
	public void VerifyPrintFile() {

		Methods.verifyDownloadedfile("Downloads", ".pdf")
	}


	@When("I click on Care Physician field")
	def CarePhysiciaClick() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_CarePhysicianClick'))
		Thread.sleep(1000)
	}

	@When("I enter (.*) as Care Physician")
	def CarePhysiciaInput(String CarePhysician) {


		WebUI.scrollToElement(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_CarePhysicianClick'), 10)
		Thread.sleep(1000)
		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_CarePhysicianInput'), CarePhysician)
		Thread.sleep(1000)
		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_CarePhysicianInput'),Keys.chord(Keys.ENTER))
	}

	@When("I should see (.*) as Care Physician in grid")
	public void CarePhysiciaGrid(String CarePhysician) {

		WebUI.scrollToElement(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_CarePhysicianVerify'), 30)
		String actual_CarePhysician = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_CarePhysicianVerify'))
		WebUI.verifyMatch(actual_CarePhysician, CarePhysician, false)
	}

	@When("I should see added task as (.*) with status (.*) and owner (.*) present in task section")
	def validationtask(String task, String status, String Owner) {

		String TaskXpath='//span[text()="'+task+'"]'
		String OwnerXpath='//td[text()="'+Owner+'"]'
		String StatusXpath='//td[text()="'+status+'"]'


		'Verify Task'
		obj.customVerify(frame, task, TaskXpath);
		'Verify Owner'
		obj.customVerify(frame, Owner, OwnerXpath);
		'Verify Status'
		obj.customVerify(frame, status, StatusXpath);
	}
}