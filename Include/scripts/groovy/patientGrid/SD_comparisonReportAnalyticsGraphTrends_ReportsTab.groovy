package patientGrid
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.eviware.soapui.config.ReportType
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import groovypackage.Methods
import utility_Functions.UtilityFunctions



class SD_comparisonReportAnalyticsGraphTrends_ReportsTab {

	TestObject ReportTypeClick=findTestObject('Object Repository/OR_PatientGrid/ComparisonReportAnalytics_GraphTrends_ReportsTab/Obj_Analytics_ReportsTypeClick')
	TestObject TimePeriodClick=findTestObject('Object Repository/OR_PatientGrid/ComparisonReportAnalytics_GraphTrends_ReportsTab/Obj_Analytics_TimePeriodClick')

	TestObject frame=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/CareManagementForm/Obj_CCMFrame')
	UtilityFunctions obj=new UtilityFunctions();

	@When("I click on Graph_Trends")
	public void ClickOnGraphTrends() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/ComparisonReportAnalytics_GraphTrends_ReportsTab/Obj_Analytics_GraphTrends'))
	}

	@And("I click on Reports Tab")
	public void ClickOnReports() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/ComparisonReportAnalytics_GraphTrends_ReportsTab/Obj_Analytics_ReportsTab'))
	}


	@And("I select ReportType:(.*)")
	public void i_select_ReportType(String ReportType) {
		'I click on Report Type field'
		WebUI.click(ReportTypeClick)
		'I select value from the dropdown'
		String xpath= '//ul[contains(@id ,"enrollmentCategory")]//li[text()="'+ReportType+'"]'

		obj.selectdropdown(frame,xpath)
	}

	@And("I should see (.*) as report type")
	public void ShouldSeeReportType(String ReportType1) {
		'I click on Report Type field'
		WebUI.click(ReportTypeClick)

		String xpath= '//ul[contains(@id, "enrollmentCategory")]//li[text()="'+ReportType1+'"]'

		WebUI.switchToFrame(frame, 2)

		TestObject gText = new TestObject('Mild')

		gText.addProperty('xpath', ConditionType.EQUALS, xpath)

		String actualReportType= WebUI.getText(gText)

		WebUI.verifyMatch(actualReportType, ReportType1, false)

		WebUI.switchToDefaultContent()

		Thread.sleep(2000)
	}

	@And("I should see (.*) as defaultlabel and (.*) as No Of Calls label")
	public void ShouldSeeDefaultGridLabel(String Label, String NoOfCalls) {


		String actualLabel1 = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/ComparisonReportAnalytics_GraphTrends_ReportsTab/Obj_Analytics_RangeTextVerify'))
		WebUI.verifyMatch(actualLabel1, Label, false)

		String actualLabel2 = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/ComparisonReportAnalytics_GraphTrends_ReportsTab/Obj_Analytics_NoOfCallsVerify'))
		WebUI.verifyMatch(actualLabel2, NoOfCalls, false)

		Thread.sleep(2000)
	}

	@And("I select Time Period:(.*)")
	public void i_select_TimePeriod(String TimePeriod) {
		'I click on TimePeriod field'
		WebUI.click(TimePeriodClick)
		'I select value from the dropdown'
		String xpath= '(//ul[contains(@id, "timePeriod")])[2]//li[contains(text(), "'+TimePeriod+'")]'

		obj.selectdropdown(frame,xpath)
	}

	@And("I should see (.*) as label and (.*) as No Of Calls label")
	public void ShouldSeeGridLabel(String TimePeriod, String NoOfCalls) {


		String actualLabel1 = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/ComparisonReportAnalytics_GraphTrends_ReportsTab/Obj_Analytics_RangeTextVerify'))

		if(!actualLabel1.contains(TimePeriod)){

			WebUI.verifyMatch(actualLabel1, TimePeriod, false)
		}


		String actualLabel2 = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/ComparisonReportAnalytics_GraphTrends_ReportsTab/Obj_Analytics_NoOfCallsVerify'))
		WebUI.verifyMatch(actualLabel2, NoOfCalls, false)

		Thread.sleep(2000)
	}

	@And("I click on export report button")
	public void ClickOnExportReport() {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/ComparisonReportAnalytics_GraphTrends_ReportsTab/Obj_Analytics_ExportBTN'))
	}
	@Then("I verify that exported file downloaded successfully (.*)")
	public void VerifyExportedFileDownloaded1(String ReportType) {

		File downloadFolder = new File("C:\\Users\\hafiz.amir\\Downloads")
		List namesOfFiles = Arrays.asList(downloadFolder.list())
		if(namesOfFiles.contains(ReportType)) {
			println "Success"
		}
		else {
			println "Failure"
		}
	}
}