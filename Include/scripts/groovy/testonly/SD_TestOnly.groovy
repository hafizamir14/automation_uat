package testonly
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebDriver

import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import customs_Keywords.CustomsMethods
import utility_Functions.UtilityFunctions




class SD_TestOnly extends CustomsMethods {

	WebDriver driver = DriverFactory.getWebDriver();
	UtilityFunctions obj=new UtilityFunctions();

	TestObject frame=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/CareManagementForm/Obj_CCMFrame')


	@And("I click on care plan tab11")
	public void click_On_Care_Plan_Tab11() {

		Thread.sleep(2000)
		WebUI.click(findTestObject('OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_CPButtons/OR_CarePlanTab/span_Care Plan'))
		
		
		
		
		println("Your date is ---- "+CustomsMethods.DateFormat())
	}

	@When("I search (.*) using global search12")
	public void search_Patient12(String Patient) {



		WebUI.click(findTestObject('OR_PatientGrid/OR_SearchPatient/OR_Search/OR_Input_Search_Field/Obj_inputSearch'))

		WebUI.setText(findTestObject('OR_PatientGrid/OR_SearchPatient/OR_Search/OR_Input_Search_Field/Obj_inputSearch'), Patient)

		Thread.sleep(4000)

		WebUI.click(findTestObject('OR_PatientGrid/OR_SearchPatient/OR_Search/Select_Filters/Select_Search'))

		Thread.sleep(14000)
	}

	@And("I click on add new care plan buttontest")

	public void click_On_Add_New_Care_Plan_Button12() {

		//		WebUI.switchToFrame(frame, 2)
		//
		//		WebElement Display = driver.findElement(By.xpath("//button[@id='carePlan']"));
		//		boolean isElementPresent = Display.isEnabled();
		//
		//		if(isElementPresent) {
		//
		//			Display.click();
		//		}else {
		//
		//			print("Hurrahh ..... !!")
		//		}
		//
		//		WebUI.switchToDefaultContent();

		Thread.sleep(2000)

		WebUI.click(findTestObject('OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_CPButtons/OR_AddNewCarePlan/button_NewCarePlan'))
		Thread.sleep(2000)


	}

	@And("I click on basedonpatientmedicalrecordtest")
	public void basedonpatientmedicalrecordtest() {


		if (WebUI.verifyElementPresent(findTestObject('OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_CPButtons/OR_BasedOnPatientMedicalRecord/Obj_BasedOnPatientMedicalRecord'), 3)) {

			WebUI.waitForElementClickable(findTestObject('OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_CPButtons/OR_BasedOnPatientMedicalRecord/Obj_BasedOnPatientMedicalRecord'), 10)

			WebUI.click(findTestObject('OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_CPButtons/OR_BasedOnPatientMedicalRecord/Obj_BasedOnPatientMedicalRecord'))

			Thread.sleep(10000)
		} else {
			WebUI.takeFullPageScreenshot()
		}
	}

	@Then("I should see patient (.*) as patient_nametest")
	public void patient_nametest(String Patient) {


		'Verify Care Plan Patient Name'


		String ActualName = WebUI.getText(findTestObject('OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_CPGrid/Obj_Name'))
		WebUI.verifyEqual(ActualName, Patient)
	}

	@When("I enter titletest (.*)")
	public void titletest(String Title) {

		WebUI.setText(findTestObject('OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_CPGridInput/Input'), Title)
	}

	@And("I click on save and close buttontest")
	public void click_On_SaveClose_Button() {


		WebUI.click(findTestObject('OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_CPGrid_Buttons/Obj_buttonSaveClose'))

		Thread.sleep(2000)
	}
	//	@Then("I should see (.*) success message")
	//	public void CPtest(String ExpectedText) {
	//
	//		obj.VerifyAddingAlertMessage(ExpectedText);
	//
	//	}
}