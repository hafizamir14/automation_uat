package utility_Functions;

import patientGrid.SD_BOPMR;

public class DataFactory {
	
	SD_BOPMR obj = new SD_BOPMR();
		
	public static final String clinical_add_notification="successRecord saved successfully.Hide";
	public static final String clinical_edit_notification="successRecord updated successfully.Hide";
	public static final String clinical_del_notification="successRecord deleted successfully.Hide";
	
	
	public static final String clinical_xpath_add="Record saved successfully";
	public static final String clinical_xpath_edit="Record updated successfully";
	public static final String clinical_xpath_del="Record deleted successfully";
	
	public static final String AssessmentComment_Xpath="Lorem Ipsum is simply dummy text of the printing and typesetting industry.";


	public void abc() {
		
		obj.I_should_see_assessment_data(AssessmentComment_Xpath);
		
	}
}

