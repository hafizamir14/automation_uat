package utility_Functions
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver

import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import groovy.time.TimeCategory
import internal.GlobalVariable

public class UtilityFunctions {

	TestObject frame=findTestObject('Object Repository/OR_OpenPatient/frame')

	void verifyNotificationMessage(String dynamicId,String message) {
		String xpath1 = '//div[contains(text(),"'+dynamicId+'")]'
		TestObject to = new TestObject("objectName")
		to.addProperty("xpath",ConditionType.EQUALS,xpath1)

		'Waiting for element to be clickable'
		WebUI.waitForElementClickable(to, 5)
		WebUI.click(to)

		'Verify that Alert Comes After Save Button Clicked'
		WebUI.verifyElementText(to,message)

		'Wait until Alert Disapear'
		WebUI.waitForElementNotPresent(to,GlobalVariable.timeout)
	}

	void selectdropdown(TestObject frame,String xpath) {
		WebUI.switchToFrame(frame, 2)

		TestObject t1 = new TestObject('value')
		t1.addProperty('xpath', ConditionType.EQUALS, xpath)
		WebUI.click(t1)
		WebUI.switchToDefaultContent()
	}


	void setValues(TestObject object,String value) {
		'wait for element to appear'
		WebUI.waitForElementClickable(object,GlobalVariable.timeout)

		'Set the text in the field'
		WebUI.setText(object,value)
	}

	void pressTab(TestObject object) {
		WebUI.sendKeys(object,Keys.chord(Keys.TAB))
	}

	void EnterKeys(TestObject object) {
		WebUI.sendKeys(object,Keys.chord(Keys.ENTER))
	}


	String currentDate() {
		String ret;
		def today=null;
		use(TimeCategory, {
			today = new Date()
			ret=today.format('MMddyyyy')
		})
		println ret
		return ret;
	}


	String modifyDate(int Days) {
		String ret;
		def today=null;

		use(TimeCategory, {
			today = new Date()
			def change = (today+Days.days)
			ret=change.format('MMddyyyy')
		})

		println ret
		return ret;
	}



	void customClick(TestObject frame,String xpath) {

		WebUI.switchToFrame(frame, 2)
		TestObject t1 = new TestObject('value')
		t1.addProperty('xpath', ConditionType.EQUALS,xpath)
		WebUI.waitForElementClickable(t1,GlobalVariable.timeout)
		WebUI.scrollToElement(t1,3)
		WebUI.click(t1)
		WebUI.switchToDefaultContent()
	}

	void ClickElement(TestObject object) {

		WebUI.click(object)
	}



	void customVerify(TestObject frame,String xpath,String message) {
		WebUI.switchToFrame(frame, 2)
		TestObject t1 = new TestObject('value')
		t1.addProperty('xpath', ConditionType.EQUALS,xpath)
		WebUI.waitForElementClickable(t1,GlobalVariable.timeout)
		WebUI.verifyElementText(t1,message)
		WebUI.switchToDefaultContent()
	}

	boolean customObjectPresent(TestObject frame,String xpath) {
		boolean val
		WebUI.switchToFrame(frame, 2)
		TestObject t1 = new TestObject('value')
		t1.addProperty('xpath', ConditionType.EQUALS,xpath)
		try {
			val=WebUI.verifyElementPresent(t1,5);
		}

		catch(Exception ex) {
			println("Catching the exception");
		}

		WebUI.switchToDefaultContent()
		return val;
	}


	String currentTime() {
		Date today = new Date()
		String nowTime = today.format('hh:mm a')
		return nowTime;
	}

	public void CountNumberOfCharacters(TestObject dynamicId) {

		//Take input from the user
		Scanner sc=new Scanner(System.in);
		int count = 0,i=0;

		System.out.print("Please Enter a String to Count Characters =  ");


		String xpath1 = '//textarea[contains(@id, '"carePlanAssessmentNotes"') and contains(text(),"'+dynamicId+'")]'
		TestObject to = new TestObject("objectName")
		to.addProperty("xpath",ConditionType.EQUALS,xpath1)

		String str = to;

		//Use a while loop to calculate the total characters in the string
		while(i < str.length())
		{
			if(str.charAt(i) != ' ')
			{
				count++;
			}
			i++;
		}
		System.out.println("\nThe Total Number of Characters  =  " + count);
	}

	public static VerifyAlertMessage(String ExpectedText) {

		WebDriver driver = DriverFactory.getWebDriver()

		String ActualText  = driver.findElement(By.xpath("//div[contains(text(),'"+ExpectedText+"')]")).getText();

		println("The Actual Text is --- "+ActualText);

		println("The expected text is ------"+ExpectedText);

		if(ActualText.is(ExpectedText))

			WebUI.verifyMatch(ActualText, ExpectedText, true)

	}



	public UtilityFunctions() {
	}
}
