package analytics

import org.junit.runner.RunWith

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber

@RunWith(Cucumber.class)
@CucumberOptions(features="Include/features/PatientGrid/Analytics",
glue="",
tags = "@Analytics_VerifySecondaryHeading_FormTab",
plugin=["pretty", "html:ReportsFolder", "json:ReportsFolder/cucumber.json"])

public class CR_Analytics_VerifyDateSecondaryTitle_HIE {
}
