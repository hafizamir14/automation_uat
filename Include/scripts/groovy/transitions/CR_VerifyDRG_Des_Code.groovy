package transitions

import org.junit.runner.RunWith

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber

@RunWith(Cucumber.class)
@CucumberOptions(features="Include/features/PatientGrid/Transitions/VerifyDRG_Desc_Code.feature",
glue="",
//tags = "@Analytics_AppointmentOutReachTab_Export",
plugin=["usage", "html:ReportsFolder", "json:ReportsFolder/cucumber.json"])

public class CR_VerifyDRG_Des_Code {
}
