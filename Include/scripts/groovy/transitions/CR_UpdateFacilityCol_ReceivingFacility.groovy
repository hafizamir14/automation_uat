package transitions

import org.junit.runner.RunWith

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber

@RunWith(Cucumber.class)
@CucumberOptions(features="Include/features/PatientGrid/Transitions/UpdateColFacility_ReceivingFacility.feature",
glue="",
//tags = "@Analytics_BlockAppointmentOutReachTab",
plugin=["pretty", "html:ReportsFolder", "json:ReportsFolder/cucumber.json"])

public class CR_UpdateFacilityCol_ReceivingFacility {
}
