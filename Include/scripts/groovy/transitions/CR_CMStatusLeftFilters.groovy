package transitions

import org.junit.runner.RunWith

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber

@RunWith(Cucumber.class)
@CucumberOptions(features="Include/features/PatientGrid/Transitions/CMStatusLeftFilters.feature",
glue="",
//tags = "@Analytics_AppointmentOutReachTab_Export",
plugin=["pretty", "html:ReportsFolder", "json:ReportsFolder/cucumber.json"])

public class CR_CMStatusLeftFilters {
}
