<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_Analytics_ReportSelect</name>
   <tag></tag>
   <elementGuidId>4354c3a7-0bf3-4068-a1da-da5a34ab270f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id=&quot;enrollmentCategory_listbox&quot;]//li[contains(text(), 'Telehealth Usage Report')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>56e6e3b0-954e-4c1f-8ab0-fc37d6df680a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>14787940-e01f-4a4b-ade6-48bbafea0a47</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>k-input global-patient-search</value>
      <webElementGuid>cb57d49d-844c-43b0-b3c8-b7e7ad34208d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>query</value>
      <webElementGuid>ee9eabb3-d7e3-465c-8e1a-0a2050419519</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Search</value>
      <webElementGuid>5325596c-e45b-47da-a1a1-a70bb718f367</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>patientsearch</value>
      <webElementGuid>ab67cf82-4e1b-409e-be8a-60e8094b6bce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-role</name>
      <type>Main</type>
      <value>autocomplete</value>
      <webElementGuid>8e1cbe14-5d98-4ed5-9135-656ac2421822</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>14e9601c-987f-4a5d-b524-e1a1cb52004d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>textbox</value>
      <webElementGuid>34547026-924c-4e49-84fa-73ca1551f350</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>2a1646ab-97bd-48ba-a418-3a3b8d72288c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>7ecb7266-f542-4e4f-b95f-0c075a7ab6eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-readonly</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>66948a7a-1d80-4fa0-95ad-e9eaafa8a5ca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-owns</name>
      <type>Main</type>
      <value>patientsearch_listbox</value>
      <webElementGuid>bce32420-62a4-4a55-930c-c6fd4f3c7d15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-autocomplete</name>
      <type>Main</type>
      <value>list</value>
      <webElementGuid>56d3ba01-9073-4c55-a935-ec77ee74c12b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;patientsearch&quot;)</value>
      <webElementGuid>88ea1bde-5142-4805-bc5c-e6953a589f02</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_SearchPatient/OR_Search/OR_Input_Search_Field/Obj_CCMFrame</value>
      <webElementGuid>4a369d9c-0953-4b73-aa08-aab5b9ac4d9d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='patientsearch']</value>
      <webElementGuid>2e524111-0d6e-47ab-ada6-ec4a3cdbed41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='tab_content_patient']/div/div/span/input</value>
      <webElementGuid>e7cf4e4d-1780-4171-ae01-7adf4280a731</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[22]/div/div/div/div/div/div/span/input</value>
      <webElementGuid>8bda1b85-8b22-49d3-a603-65ec9a5cafc6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
