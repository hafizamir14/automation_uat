<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_TaskUpdateBTN</name>
   <tag></tag>
   <elementGuidId>dbb4ed70-2056-4ac1-8098-7d29998155ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@class='k-grid-update btn btn-primary'])[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>b77b5e75-887e-442f-9efe-5cc2e8e4a089</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>e866ce5a-5626-4255-8a9a-322bdd05cb27</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>k-input global-patient-search</value>
      <webElementGuid>a19867db-396d-404d-aeec-163b7b5369a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>query</value>
      <webElementGuid>f2228caa-1a3f-4ff3-af16-14569a513a90</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Search</value>
      <webElementGuid>a51004d5-25e8-4298-9f63-5542aeffae0d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>patientsearch</value>
      <webElementGuid>0b5d9930-cef2-4f3f-a14d-42b013b5de1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-role</name>
      <type>Main</type>
      <value>autocomplete</value>
      <webElementGuid>386a1843-2ea0-421e-a0c1-af238387105d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>301f7c57-ad1b-40b2-981d-1f2c7e90877d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>textbox</value>
      <webElementGuid>341ea5af-59de-44f5-bae4-6977e1835c85</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>5f336ff4-8232-426f-ae5f-f82287584500</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>11a7e5f0-89dc-4fe4-8c9c-dcb6a1e0f4ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-readonly</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>805fe4df-2a22-4ac1-aa43-974f36278e9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-owns</name>
      <type>Main</type>
      <value>patientsearch_listbox</value>
      <webElementGuid>36d62ae5-4e21-4f34-b0e5-2f565ced4b03</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-autocomplete</name>
      <type>Main</type>
      <value>list</value>
      <webElementGuid>ec077e44-a528-4405-ba78-9098ac89ac12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;patientsearch&quot;)</value>
      <webElementGuid>2d8c44b5-ead2-466a-9f06-38568dc238a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_SearchPatient/OR_Search/OR_Input_Search_Field/Obj_CCMFrame</value>
      <webElementGuid>160c4229-381b-46c0-bec4-2a3a4dfbfc64</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='patientsearch']</value>
      <webElementGuid>12b56502-b3ac-4527-8b7b-73e6871b6bbc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='tab_content_patient']/div/div/span/input</value>
      <webElementGuid>4eae8cd4-7096-451d-a323-524f3518d641</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[22]/div/div/div/div/div/div/span/input</value>
      <webElementGuid>db7f8fdf-cff2-4b1f-b991-113bb2947942</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
