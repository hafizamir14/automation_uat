<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_Verify_SoapNote_Status</name>
   <tag></tag>
   <elementGuidId>731ec251-664d-41e7-b33f-80d91b0d5aee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#carePlanPatientInfoDiv_550910 > div</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//td[contains(text(), 'Signed')])[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Ahmad, Murtaza' or . = 'Ahmad, Murtaza') and @ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>89b6ddcf-eeaa-4e5a-8292-bfaa13377a9f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ahmad, Murtaza</value>
      <webElementGuid>35bdc129-ac5a-409f-935b-13707731bd8d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@id='superBillMainGrid']//div)[4]//tr//td[6]</value>
      <webElementGuid>66818715-0d5b-4de9-bea2-bd95808746ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>1ab94f53-59d7-487b-8170-14b3c357ee44</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='carePlanPatientInfoDiv_550910']/div</value>
      <webElementGuid>de3bb0c6-a006-4cb9-8218-f7d1b08123d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reject'])[1]/following::div[6]</value>
      <webElementGuid>22fdab5c-5b2a-4127-a457-793107bf509c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save &amp; Approve'])[1]/following::div[6]</value>
      <webElementGuid>a70ebe4b-e895-436b-b28d-084f01eb3263</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='DOB'])[6]/preceding::div[2]</value>
      <webElementGuid>532cf6cd-9cb7-4fdc-af6e-431771c3765f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[157]/div[2]/div[2]/div/div/div/div/div</value>
      <webElementGuid>7c56cc34-e7b7-4f34-aa8c-0bef760e6736</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
