<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_BillingStatus_Verify</name>
   <tag></tag>
   <elementGuidId>c247c410-b53e-49bf-8db6-7c2cbd64e5ac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//td[contains(text(), 'Incomplete')])[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@id='superBillMainGrid']//div)[4]//tr//td[5]</value>
      <webElementGuid>d86f5317-ff1d-47b8-bb59-b0914f7e909d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>9caf2d3b-2e05-42f7-9bd0-5ab053ec367f</webElementGuid>
   </webElementProperties>
</WebElementEntity>
