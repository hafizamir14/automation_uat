<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_PatientClick_OpenGrid</name>
   <tag></tag>
   <elementGuidId>a63a1c42-eb7a-448d-a637-657fe0ceb7ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//a[@class='k-icon k-plus'])[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@id='superBillMainGrid']//div)[4]//table//tbody//tr[1]//td[1]</value>
      <webElementGuid>43d39d11-3d7d-4949-a27d-f52e23bf8495</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>f9165b02-8283-46ec-a559-b530272d229d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
