<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_FutureAppointment_Plus</name>
   <tag></tag>
   <elementGuidId>1cd744fa-0cf4-4ed5-8c22-a74ade51b8fd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[contains(@id,'ccmAppointmentsDiv')]/div[1])//div//span[2]//a[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>70325ef8-df12-4ad8-86f9-db7e724d2a74</webElementGuid>
   </webElementProperties>
</WebElementEntity>
