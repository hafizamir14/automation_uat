<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_recordSaveSuccessfully</name>
   <tag></tag>
   <elementGuidId>0cfea700-9bc0-45b5-b7db-c04af68e840e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'successCare Plan Saved SuccessfullyHide' or . = 'successCare Plan Saved SuccessfullyHide')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(), 'successfully')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.k-notification-wrap</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>af9ad032-f4db-4f0d-8610-6d523e9dde52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>k-notification-wrap</value>
      <webElementGuid>2145098f-0abe-447b-8f83-83a2beaedd3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>successCare Plan Saved SuccessfullyHide</value>
      <webElementGuid>5ba48f70-33f2-4c3c-8dba-bd8d3242b76f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;k-webkit k-webkit88&quot;]/body[@class=&quot;k-state-border-down&quot;]/div[@class=&quot;k-animation-container _fc254a42-97d3-427a-8d07-6ab793327c93&quot;]/div[@class=&quot;k-widget k-notification k-notification-success k-popup k-group k-reset k-state-border-up&quot;]/div[@class=&quot;k-notification-wrap&quot;]</value>
      <webElementGuid>b275b3dd-fddc-45a9-ae4c-dcb86074bcb8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[4]</value>
      <webElementGuid>825250c7-ccf4-4906-9c98-acd0a7720f2a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[2]/following::div[4]</value>
      <webElementGuid>01f1d4dc-75cd-4d1e-b2f2-948a21ea86cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Care Plan Saved Successfully']/parent::*</value>
      <webElementGuid>5716b785-c57f-4b85-9722-2b8c4e23be24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[14]/div/div</value>
      <webElementGuid>701d3bd6-6a77-450c-8808-a8e10294293d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
