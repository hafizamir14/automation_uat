<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_Encounter_Plus</name>
   <tag></tag>
   <elementGuidId>398f06da-c225-4ed6-9da7-b6aeda77aa7f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[contains(@id,'carePlanPatientCareEncounter')]/div[1])//div//span[2]//a[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
   </webElementProperties>
</WebElementEntity>
