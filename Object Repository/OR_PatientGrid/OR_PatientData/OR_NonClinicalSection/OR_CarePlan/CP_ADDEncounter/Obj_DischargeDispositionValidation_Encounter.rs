<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_DischargeDispositionValidation_Encounter</name>
   <tag></tag>
   <elementGuidId>851f7278-0f8c-4272-a50e-f4e21a056ded</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>((//div[contains(@id,'carePlanPatientCareEncounter')])//table)[2]//tr[1]//td[8]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
   </webElementProperties>
</WebElementEntity>
