<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_EncounterStartDate</name>
   <tag></tag>
   <elementGuidId>d8ac2c69-7b3b-4d61-8e78-d26faace7982</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input[name='encounter.startDate']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
   </webElementProperties>
</WebElementEntity>
