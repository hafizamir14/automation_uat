<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_EncounterValidation</name>
   <tag></tag>
   <elementGuidId>20d1d0da-36ad-41e3-8ed3-8e4090eb4170</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>((//div[contains(@id,'carePlanPatientCareEncounter')])//table)[2]//tr[1]//td[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
   </webElementProperties>
</WebElementEntity>
