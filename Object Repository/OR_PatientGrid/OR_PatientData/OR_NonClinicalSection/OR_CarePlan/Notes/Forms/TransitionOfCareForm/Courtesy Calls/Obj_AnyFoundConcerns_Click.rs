<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_AnyFoundConcerns_Click</name>
   <tag></tag>
   <elementGuidId>fca2b2e3-336d-45fc-90f0-d3d6718237a1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[contains(@name,'anyFoundConcernsOrPotentialBarriersReportedToPrimaryCarePhysicianOffice')]//parent::span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@name=aprimaId']</value>
      <webElementGuid>0b90cebd-8b69-46f7-9044-368b87105015</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>4ed2368c-31b2-49be-a787-4a3deb5085c2</webElementGuid>
   </webElementProperties>
</WebElementEntity>
