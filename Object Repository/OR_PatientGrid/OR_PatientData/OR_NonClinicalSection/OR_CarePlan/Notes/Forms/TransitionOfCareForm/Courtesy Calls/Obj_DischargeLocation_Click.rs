<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_DischargeLocation_Click</name>
   <tag></tag>
   <elementGuidId>33bec3a1-d3ef-4172-aa85-41d84040ecc4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(@aria-owns,'dischargeLocation')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@name=aprimaId']</value>
      <webElementGuid>8e6513d7-ee0a-4df4-a562-072c6a7110df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>447dd988-62ec-45e4-ad27-f69a96f6d4d6</webElementGuid>
   </webElementProperties>
</WebElementEntity>
