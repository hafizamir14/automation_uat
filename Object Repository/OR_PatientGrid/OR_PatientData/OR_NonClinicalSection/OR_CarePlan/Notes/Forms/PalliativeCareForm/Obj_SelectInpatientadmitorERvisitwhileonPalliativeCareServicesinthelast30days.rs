<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_SelectInpatientadmitorERvisitwhileonPalliativeCareServicesinthelast30days</name>
   <tag></tag>
   <elementGuidId>8fef65ab-a9b1-4193-83fd-18c0bea754a5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id='palliativeCareServicesInTheLast30Days_listbox']//li[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//li[contains(@class, 'k-item k-state-focused k-state-hover') and contains(text(), 'Yes')]</value>
      <webElementGuid>0975f4a8-599a-45b2-8bea-f349f8f489ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>133b4337-9c6f-4f80-8521-53a14dc6fe62</webElementGuid>
   </webElementProperties>
</WebElementEntity>
