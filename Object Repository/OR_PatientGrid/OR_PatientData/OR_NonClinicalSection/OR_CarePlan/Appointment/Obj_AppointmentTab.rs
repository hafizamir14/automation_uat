<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_AppointmentTab</name>
   <tag></tag>
   <elementGuidId>baed70e7-7ade-4c1d-b8ba-5ff1f67d0085</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.k-item.k-state-default.k-state-hover > span.k-link</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[contains(@id, 'appointments')]//span[contains(text(), 'Appointment')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>7771a5e6-f430-4fb4-8e2e-fa246c1098e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>k-link</value>
      <webElementGuid>9108c1af-3f4c-4b5e-ace3-dcadbc5aceff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Care Plan</value>
      <webElementGuid>9e8c3c35-f54a-4306-9720-6765f42dc13b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;patient-grid&quot;)/div[@class=&quot;k-grid-content k-auto-scrollable&quot;]/table[1]/tbody[1]/tr[@class=&quot;k-detail-row&quot;]/td[@class=&quot;k-detail-cell&quot;]/div[@class=&quot;k-tabstrip-wrapper&quot;]/div[@class=&quot;pwb tabstrip k-widget k-header k-tabstrip k-floatwrap k-tabstrip-top&quot;]/ul[@class=&quot;k-tabstrip-items k-reset&quot;]/li[@class=&quot;k-item k-state-default k-state-hover&quot;]/span[@class=&quot;k-link&quot;]</value>
      <webElementGuid>40e7a6a9-51e8-407f-bf58-fcfe59fe5b9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_CPButtons/Obj_CCMFrame</value>
      <webElementGuid>67944dba-870c-4300-bd65-e3257f77a2c6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='patient-grid']/div[3]/table/tbody/tr[2]/td[2]/div/div/ul/li[15]/span[2]</value>
      <webElementGuid>e9347ace-743b-4b15-81da-47fc1044797e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Appointments'])[1]/following::span[2]</value>
      <webElementGuid>cfa9f41f-66a4-4eaa-98f1-d2cbfaafc3bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tasks'])[2]/following::span[4]</value>
      <webElementGuid>c865f0c7-ce0d-49a1-a3a2-a0eaf5cb06b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Notes'])[2]/preceding::span[2]</value>
      <webElementGuid>2c68c81d-7db0-44e6-bd2b-4f8d6b055039</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Documentation'])[1]/preceding::span[4]</value>
      <webElementGuid>023b411f-d45b-44c4-8165-1d8042b6b3fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[15]/span[2]</value>
      <webElementGuid>1d0aec56-5f94-422e-bd7c-461b4d4e2093</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
