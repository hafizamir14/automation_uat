<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_Problems_Plus</name>
   <tag></tag>
   <elementGuidId>4aabc834-838d-43bd-af2c-504e74b4a3c0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@data-mz-key='clinical.problem']/div[1]/div/button[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='clinical problem k-grid has-wizard mz-widget k-widget k-editable editable']/div[1]/div/button[1]</value>
      <webElementGuid>2f6867b4-92bc-40bc-8b36-1880e131bc38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>c3187fda-cd17-4e4e-beb0-18395a2dbb63</webElementGuid>
   </webElementProperties>
</WebElementEntity>
