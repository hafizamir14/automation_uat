<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_Entered_Procedure_Note</name>
   <tag></tag>
   <elementGuidId>6604ab5f-4050-44b8-ad19-94e71da69471</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-mz-key='clinical.procedure'])/div[3]//tr[1]//td[12]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='clinical lab k-grid has-wizard list-mode-only mz-widget k-widget k-editable batch-editing editable']/div[3]/table/tbody/tr[2]/td[2]</value>
      <webElementGuid>7d3b6a2c-e233-41cb-bdb6-e9cb8e8f72a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>4ca0ba13-1331-48b3-b4e8-b4f2cff6057d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
