<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_ProcedureResults_Plus</name>
   <tag></tag>
   <elementGuidId>ae7ebf1c-afa5-453e-ba0b-475e9a1df875</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-icon-img='procedures.png'])[2]//div[2]//button[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>(//div[@data-icon-img='labs results.png']//button)[1][count(. | //*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']) = count(//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@data-icon-img='labs results.png']//button)[1]</value>
      <webElementGuid>7e2f13e3-d695-453e-b6cf-6c61e819b445</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>7caec0d9-e996-445b-8c23-e9b51036074e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
