<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_InsuranceCheckbox</name>
   <tag></tag>
   <elementGuidId>afe4b5b9-cd70-4948-8a1d-ca7f0225dfa8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-mz-key='insurance'])[3]/div[3]/table//tr[1]//input[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@placeholder='Search and select analyte']</value>
      <webElementGuid>8c968af8-0a51-40ba-8d9d-64e39ed02afc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>d26ba085-f560-4fdd-95a8-bae72e1d8595</webElementGuid>
   </webElementProperties>
</WebElementEntity>
