<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_Insurance_ExistingBTN</name>
   <tag></tag>
   <elementGuidId>a2ff40eb-a021-4c69-9416-5b67603df568</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-icon-img='insurance Icon.svg'])[1]/div[1]//div[2]//button[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>(//div[@data-icon-img='labs results.png']//button)[1][count(. | //*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']) = count(//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@data-icon-img='labs results.png']//button)[1]</value>
      <webElementGuid>98a22057-e0ea-4cb5-8f25-bd76445dee33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>2858b71d-36cd-43b6-9d1d-97166c3ea9db</webElementGuid>
   </webElementProperties>
</WebElementEntity>
