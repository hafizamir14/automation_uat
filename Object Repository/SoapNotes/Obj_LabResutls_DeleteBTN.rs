<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_LabResutls_DeleteBTN</name>
   <tag></tag>
   <elementGuidId>294372e5-b9e3-4600-8273-8dbe0fdb9245</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@data-mz-key='clinical.lab']/div[3]//span[contains(@class, 'k-button k-button-icontext delete-panel k-button-small') and contains(@title, 'Delete')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>775e36ba-0360-48a0-896e-01b63c0ecc03</webElementGuid>
   </webElementProperties>
</WebElementEntity>
