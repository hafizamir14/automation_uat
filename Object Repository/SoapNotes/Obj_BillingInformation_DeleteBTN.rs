<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_BillingInformation_DeleteBTN</name>
   <tag></tag>
   <elementGuidId>b4e381d3-d637-44b8-86d1-4d1426c31943</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='col-md-4 col-xs-12 soap-wizard-panel lg']/div/div[2]/div/div/div[2]/div/div[3]//button[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>3b6afb4d-5329-48ef-988e-16856d06f48a</webElementGuid>
   </webElementProperties>
</WebElementEntity>
