<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_Vital_EditBTN</name>
   <tag></tag>
   <elementGuidId>5de1564f-694b-4259-a93e-1a86c2d1515e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-icon-img='vital_signs.png'])/div[3]//tr[2]//button[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>afa1dda3-1034-40be-9533-21013c58a533</webElementGuid>
   </webElementProperties>
</WebElementEntity>
