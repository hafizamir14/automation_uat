<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_Vital_DeleteBTN</name>
   <tag></tag>
   <elementGuidId>cee6eade-7e0d-4672-93e2-6498dea7fc9e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-icon-img='vital_signs.png'])/div[3]//tr[2]//button[3]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>a0393397-1c63-4b0f-b5b8-51ff81ae0384</webElementGuid>
   </webElementProperties>
</WebElementEntity>
