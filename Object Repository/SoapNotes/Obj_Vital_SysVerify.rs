<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_Vital_SysVerify</name>
   <tag></tag>
   <elementGuidId>080a4153-e5a1-4053-b746-7eca78a618d5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-mz-key='clinical.vital.all'])/div[3]//tr[2]//td[8]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>e46aa1f7-5394-4310-a1dd-4878ddec18bc</webElementGuid>
   </webElementProperties>
</WebElementEntity>
