<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_DiagnosisDeleteBTN</name>
   <tag></tag>
   <elementGuidId>2707f909-bf64-4535-9e35-67605a4124ea</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-icon-img='problem.png'])/div[3]//tr[1]//button[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>7ba61a06-c588-487c-814b-d7607ddc8650</webElementGuid>
   </webElementProperties>
</WebElementEntity>
