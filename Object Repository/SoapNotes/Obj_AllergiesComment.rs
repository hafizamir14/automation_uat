<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_AllergiesComment</name>
   <tag></tag>
   <elementGuidId>a6412dfa-26c5-4e7c-8e42-f841c8bb42ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//textarea[contains(@data-bind, 'value: textNote') and @name= 'textNote']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>18c1f551-5638-4ec1-a5a7-3fb1a0dd975f</webElementGuid>
   </webElementProperties>
</WebElementEntity>
