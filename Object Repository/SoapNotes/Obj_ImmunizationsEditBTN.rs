<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_ImmunizationsEditBTN</name>
   <tag></tag>
   <elementGuidId>6b393ff8-28f9-4b37-9053-a77b69cb663c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-icon-img='immunization.png'])/div[3]//tr[1]//button[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>f0270d13-73d8-4747-a644-7d7e3ea57dcf</webElementGuid>
   </webElementProperties>
</WebElementEntity>
