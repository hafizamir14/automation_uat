<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_MedicationDeleteBTN</name>
   <tag></tag>
   <elementGuidId>65359cc5-5d65-42f1-a8e4-0b6ddfe44af9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-icon-img='medications.png'])/div[3]//tr[1]//button[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>cf2a3b79-8f83-4da2-bf10-72c426886e27</webElementGuid>
   </webElementProperties>
</WebElementEntity>
