<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_AllergiesDeleteBTN_Confirm</name>
   <tag></tag>
   <elementGuidId>038550f2-cf82-4815-ba82-12ee2e3f9c9c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@class='confirm btn btn-primary']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>71094659-dba6-4a38-9014-bd89661791af</webElementGuid>
   </webElementProperties>
</WebElementEntity>
