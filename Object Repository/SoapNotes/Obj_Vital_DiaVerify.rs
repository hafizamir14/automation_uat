<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_Vital_DiaVerify</name>
   <tag></tag>
   <elementGuidId>a2fc44c0-6f55-4509-a654-814f5f391bcb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-mz-key='clinical.vital.all'])/div[3]//tr[2]//td[8]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>f6b7df44-cf5b-47ed-adfd-25ece57ecb04</webElementGuid>
   </webElementProperties>
</WebElementEntity>
