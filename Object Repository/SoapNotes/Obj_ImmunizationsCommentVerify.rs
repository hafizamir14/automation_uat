<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_ImmunizationsCommentVerify</name>
   <tag></tag>
   <elementGuidId>f0af537c-2443-4718-b1b9-d644a9701779</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-icon-img='immunization.png'])/div[3]//tr[1]/td[10]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>b417d014-cecc-487e-b794-67ed0421051d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
