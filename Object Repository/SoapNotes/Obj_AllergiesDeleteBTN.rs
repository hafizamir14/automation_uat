<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_AllergiesDeleteBTN</name>
   <tag></tag>
   <elementGuidId>cb5d42ea-3306-41a5-8e82-09edaf5c0c9e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@class='clinical allergy k-grid has-wizard mz-widget k-widget k-editable editable'])/div[3]//tr[1]//button[3]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>5988c5f9-6e77-420c-9a35-43fbb0882c7a</webElementGuid>
   </webElementProperties>
</WebElementEntity>
