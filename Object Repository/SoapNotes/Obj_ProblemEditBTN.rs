<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_ProblemEditBTN</name>
   <tag></tag>
   <elementGuidId>42b8b141-8c32-4f34-82e7-2f78abc73594</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-icon-img='problem.png'])/div[3]//tr[1]//button[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>4e41df34-c6f7-408c-b165-3c82f0cdeda1</webElementGuid>
   </webElementProperties>
</WebElementEntity>
