<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_MedicationDeleteBTN_Confirm</name>
   <tag></tag>
   <elementGuidId>1d325531-636c-45ed-9991-f80faf54aa47</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@class='confirm btn btn-primary']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>648d6478-fafa-4fe0-be73-de09f4f3e263</webElementGuid>
   </webElementProperties>
</WebElementEntity>
