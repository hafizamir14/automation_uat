<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_ProcedureDeleteBTN</name>
   <tag></tag>
   <elementGuidId>208a027c-d518-466e-b1e4-24588097663a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-icon-img='procedures.png'])/div[3]//tr[1]//button[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>0603a39f-7e6b-49b6-b3cd-3d6c0775c42d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
