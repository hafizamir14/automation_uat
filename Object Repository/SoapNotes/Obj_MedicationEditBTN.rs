<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_MedicationEditBTN</name>
   <tag></tag>
   <elementGuidId>22f3f0fe-7c53-4010-b79e-9a77e9570df0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-icon-img='medications.png'])/div[3]//tr[1]//button[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>92696608-abaa-4116-b7bd-a50c68611355</webElementGuid>
   </webElementProperties>
</WebElementEntity>
