<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_ProblemDeleteBTN_Confirm</name>
   <tag></tag>
   <elementGuidId>d9ae4a90-8413-4d1a-9614-0c960ad95156</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@class='confirm btn btn-primary']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>d334ad89-273d-4b5e-8791-723a39bc2017</webElementGuid>
   </webElementProperties>
</WebElementEntity>
