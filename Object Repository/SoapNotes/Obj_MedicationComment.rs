<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_MedicationComment</name>
   <tag></tag>
   <elementGuidId>6ff689a3-1f5c-4c14-b53a-456489448d46</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//textarea[contains(@data-bind, 'value: comments') and @name= 'comments'])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>fffb2e14-e1cf-4f20-8561-ced3665e7a13</webElementGuid>
   </webElementProperties>
</WebElementEntity>
