<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_AllergiesEditBTN</name>
   <tag></tag>
   <elementGuidId>2bf61e4d-efa7-4fd2-a594-e5ca61efc86d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@class='clinical allergy k-grid has-wizard mz-widget k-widget k-editable editable'])/div[3]//tr[1]//button[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>e177442e-bb41-4b59-98a4-09384c01e41e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
