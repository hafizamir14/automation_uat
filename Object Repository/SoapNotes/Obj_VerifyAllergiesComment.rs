<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_VerifyAllergiesComment</name>
   <tag></tag>
   <elementGuidId>e6b0c420-8589-49fd-a6c7-ee727c744e13</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@class='clinical allergy k-grid has-wizard mz-widget k-widget k-editable editable'])/div[3]//tr[1]/td[11]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>a7f10fe4-18af-4029-8a11-47aae4b1f176</webElementGuid>
   </webElementProperties>
</WebElementEntity>
