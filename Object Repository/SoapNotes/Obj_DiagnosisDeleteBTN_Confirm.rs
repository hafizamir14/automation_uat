<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_DiagnosisDeleteBTN_Confirm</name>
   <tag></tag>
   <elementGuidId>ac54d847-36b1-4986-ad4b-f77b59e728ea</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@class='confirm btn btn-primary']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>ed290086-f229-4745-aba9-daf09f536252</webElementGuid>
   </webElementProperties>
</WebElementEntity>
