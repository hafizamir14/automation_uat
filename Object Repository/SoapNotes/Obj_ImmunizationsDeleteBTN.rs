<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_ImmunizationsDeleteBTN</name>
   <tag></tag>
   <elementGuidId>cee19d42-cc5f-4543-9988-ee49e9f8f36b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-icon-img='immunization.png'])/div[3]//tr[1]//button[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>9a4cf6c5-0266-46d7-a300-05a4ace9bb44</webElementGuid>
   </webElementProperties>
</WebElementEntity>
