<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_VerifyMedicationComment</name>
   <tag></tag>
   <elementGuidId>7f6c28d7-4b06-46d0-a2f9-3ebed43ef6e4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-icon-img='medications.png'])/div[3]//tr[1]/td[6]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>da1057fd-393c-4269-b5b0-7ba4972b877f</webElementGuid>
   </webElementProperties>
</WebElementEntity>
