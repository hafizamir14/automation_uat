<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Refreshbutton</name>
   <tag></tag>
   <elementGuidId>0ac5f515-3171-449f-ab08-df80ff50b985</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='tocGrid']//span[@class='k-icon k-i-refresh']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>//div[@id='tocGrid']//span[@class='k-icon k-i-refresh']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/ProgramEnrollment/frameObject</value>
   </webElementProperties>
</WebElementEntity>
