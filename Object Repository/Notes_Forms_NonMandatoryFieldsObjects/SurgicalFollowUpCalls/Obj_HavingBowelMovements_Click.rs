<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_HavingBowelMovements_Click</name>
   <tag></tag>
   <elementGuidId>97516a35-07f5-47a6-b59e-43c0eddde873</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(@aria-owns, 'havingBowelMovements_listbox')]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>6b41a8d5-ddc3-4590-bdcc-325330d1aa20</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-show</name>
      <type>Main</type>
      <value>$select.isEmpty()</value>
      <webElementGuid>427096b2-4c91-4c91-8c72-ef5c29f848b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui-select-placeholder text-muted ng-binding</value>
      <webElementGuid>c6837a5c-2391-4794-983b-f2558f70cebb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Select</value>
      <webElementGuid>86b09e05-6c97-491e-bcdc-aebd55347130</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;panel46ColumnsSelect&quot;)/div[@class=&quot;ui-select-match ng-scope&quot;]/span[@class=&quot;btn btn-default form-control ui-select-toggle&quot;]/span[@class=&quot;ui-select-placeholder text-muted ng-binding&quot;]</value>
      <webElementGuid>bc32259e-e4bb-4ad1-b82f-15120a6138f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/AllSoapNoteTest/NewHRAFlows_Objects/iframe_SignOut_appCCM</value>
      <webElementGuid>8840aec1-3329-4c85-8077-ac6ac89ef893</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='panel46ColumnsSelect']/div/span/span</value>
      <webElementGuid>d5649b9a-9dec-4d08-ac7a-b6670e8b2d9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CASE MANAGEMENT'])[1]/following::span[2]</value>
      <webElementGuid>8769f0d0-5e30-467b-ac4a-e3799aba7e8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Physical Exam'])[1]/following::span[14]</value>
      <webElementGuid>204f57e9-93c2-47ac-900a-ff97e187e540</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='×'])[1]/preceding::span[2]</value>
      <webElementGuid>24c71b03-5671-457f-b7b6-1d9855aaafdc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SUMMARY AND RECOMMENDATIONS'])[1]/preceding::span[5]</value>
      <webElementGuid>dcbfcb20-6e0b-47a5-b725-cfd585fbb5f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Select']/parent::*</value>
      <webElementGuid>0d7bd56d-c031-4ad7-88b2-13765727372f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/span</value>
      <webElementGuid>29ddab4a-5956-4456-a8eb-bd5c60c4956f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
