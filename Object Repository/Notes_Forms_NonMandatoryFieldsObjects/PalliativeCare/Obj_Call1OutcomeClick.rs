<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_Call1OutcomeClick</name>
   <tag></tag>
   <elementGuidId>af0af4e8-55d4-4eec-9e4c-4e0b8b2066f3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@aria-owns='call1OutCome_listbox']</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>660f132a-270d-4042-882e-26847c50ed2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-show</name>
      <type>Main</type>
      <value>$select.isEmpty()</value>
      <webElementGuid>08246dd1-cfb9-4a66-a42a-b13e92e8c25e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui-select-placeholder text-muted ng-binding</value>
      <webElementGuid>329ec6dc-65d1-4d1b-9a03-14880099ed7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Select</value>
      <webElementGuid>7c7e09e6-e1af-492b-9d6e-b9d92eba004c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;panel46ColumnsSelect&quot;)/div[@class=&quot;ui-select-match ng-scope&quot;]/span[@class=&quot;btn btn-default form-control ui-select-toggle&quot;]/span[@class=&quot;ui-select-placeholder text-muted ng-binding&quot;]</value>
      <webElementGuid>e0f65b4c-48ca-43b4-86c9-d2a12b922eb7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/AllSoapNoteTest/NewHRAFlows_Objects/iframe_SignOut_appCCM</value>
      <webElementGuid>bf32024d-31c3-4887-a767-798172fdeb7a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='panel46ColumnsSelect']/div/span/span</value>
      <webElementGuid>d1a0636f-e925-4d28-a3a8-dd3ade22c2d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CASE MANAGEMENT'])[1]/following::span[2]</value>
      <webElementGuid>14db9fd4-7840-44ff-9e28-8e6a9a431b9f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Physical Exam'])[1]/following::span[14]</value>
      <webElementGuid>bf351f6c-a375-4a83-b2fb-f865d810c5bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='×'])[1]/preceding::span[2]</value>
      <webElementGuid>bccde6ea-80ea-4156-ab96-0424a90ea753</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SUMMARY AND RECOMMENDATIONS'])[1]/preceding::span[5]</value>
      <webElementGuid>2c872541-7fe6-4a73-9ca8-984a053d050b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Select']/parent::*</value>
      <webElementGuid>5f9a7a11-36a8-47e7-adf0-b4b044784c7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/span</value>
      <webElementGuid>9216f65b-7c5a-41bb-bb2e-04d9733e2a9a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
