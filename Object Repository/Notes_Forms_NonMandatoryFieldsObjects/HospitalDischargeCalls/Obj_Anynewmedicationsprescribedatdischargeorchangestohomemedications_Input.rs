<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_Anynewmedicationsprescribedatdischargeorchangestohomemedications_Input</name>
   <tag></tag>
   <elementGuidId>9b1f369f-92cb-4b4a-ac49-7f314782f3dd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//textarea[contains(@id, 'anyNewMedicationsPrescribedAtDischargeOrChangesToHomeMedicationsDetail')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>b5c0150e-9896-4c70-b0b3-0871494064e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-show</name>
      <type>Main</type>
      <value>$select.isEmpty()</value>
      <webElementGuid>e7c0c84e-97c7-4d27-b412-b562f147e47c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui-select-placeholder text-muted ng-binding</value>
      <webElementGuid>baef21b9-5add-494b-a18b-c06af5f894c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Select</value>
      <webElementGuid>5290d9b7-e3bd-4119-84e4-2c44ef0d73f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;panel46ColumnsSelect&quot;)/div[@class=&quot;ui-select-match ng-scope&quot;]/span[@class=&quot;btn btn-default form-control ui-select-toggle&quot;]/span[@class=&quot;ui-select-placeholder text-muted ng-binding&quot;]</value>
      <webElementGuid>f0c03c86-6193-41f1-9eb9-3e17a0698ae9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/AllSoapNoteTest/NewHRAFlows_Objects/iframe_SignOut_appCCM</value>
      <webElementGuid>bbf01cdb-31f0-4efc-97f6-1402a6a41410</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='panel46ColumnsSelect']/div/span/span</value>
      <webElementGuid>8de7885f-9afc-4a0b-953f-619398540c51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CASE MANAGEMENT'])[1]/following::span[2]</value>
      <webElementGuid>de6428e2-1e9e-4192-b9db-808fbaad9e9d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Physical Exam'])[1]/following::span[14]</value>
      <webElementGuid>2e67927d-3318-4d9d-b7cc-296799dac58e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='×'])[1]/preceding::span[2]</value>
      <webElementGuid>e005b0f9-8d31-4cb9-8136-e26ee8bcb3ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SUMMARY AND RECOMMENDATIONS'])[1]/preceding::span[5]</value>
      <webElementGuid>0585ef36-db4c-46cf-bdf9-5bb70dfc7657</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Select']/parent::*</value>
      <webElementGuid>01513366-6491-43d1-8da1-e5108c36d0b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/span</value>
      <webElementGuid>0f9b0249-644c-4bd3-897e-48820d80aff9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
