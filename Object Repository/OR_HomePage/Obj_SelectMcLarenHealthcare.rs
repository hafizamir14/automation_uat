<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_SelectMcLarenHealthcare</name>
   <tag></tag>
   <elementGuidId>8c0ea165-3531-4d8e-b681-0f21711e8794</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#selectedEnterprise</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//option[contains(text(),'McLaren Healthcare')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>803c5c7c-62d5-486c-8adf-05e3f6c6f328</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav navbar-nav</value>
      <webElementGuid>7fc81827-a989-42ac-817a-b899f7ad64f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>selectedEnterprise</value>
      <webElementGuid>160e0474-4a12-45fd-aa4c-22a863267162</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
								
											MHPN
								
						</value>
      <webElementGuid>6f7a2193-4640-45d5-8765-80f0f52212cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;selectedEnterprise&quot;)</value>
      <webElementGuid>939f1610-97ad-434f-97bc-5eb7640428fb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='selectedEnterprise']</value>
      <webElementGuid>6aa8ab45-0102-4792-ac5b-85cbcf78bb4f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='enterprise-dropdown']/select</value>
      <webElementGuid>01371831-cec3-4208-a4ff-c693fa483e48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CM User Guide'])[1]/following::select[1]</value>
      <webElementGuid>7a68d130-57c3-4fb7-a1fb-c1e481eb1152</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SNF List'])[1]/following::select[1]</value>
      <webElementGuid>73c49a94-aeac-4d6e-8ec9-38b27dc04bb9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Amir, Hafiz'])[1]/preceding::select[1]</value>
      <webElementGuid>4cc8ddb9-309a-44a8-9bea-014fab226484</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Amir, Hafiz'])[2]/preceding::select[1]</value>
      <webElementGuid>687b9902-fdb7-49d9-8dba-4ce911bcdbb9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>24d682bb-9eb2-437a-a00f-c3b9ba779bbc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
