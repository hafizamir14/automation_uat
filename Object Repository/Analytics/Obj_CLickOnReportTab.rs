<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_CLickOnReportTab</name>
   <tag></tag>
   <elementGuidId>ef57df74-5498-47f2-9923-bd8735d510ae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//a[@id='tab_link_analytic_TeleHealth'])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>2c804358-d279-4c79-a457-80e36e896ada</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-show</name>
      <type>Main</type>
      <value>$select.isEmpty()</value>
      <webElementGuid>5b1ef935-a692-45dd-bdda-2b5ca9d5ee35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui-select-placeholder text-muted ng-binding</value>
      <webElementGuid>f832cb03-093e-4c86-aadc-5ee40d858129</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Select</value>
      <webElementGuid>b19cee9f-812e-4db3-8e97-48af52a712fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;panel46ColumnsSelect&quot;)/div[@class=&quot;ui-select-match ng-scope&quot;]/span[@class=&quot;btn btn-default form-control ui-select-toggle&quot;]/span[@class=&quot;ui-select-placeholder text-muted ng-binding&quot;]</value>
      <webElementGuid>e4b61d5a-120e-4b85-8cd9-92ac0ca64cb7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/AllSoapNoteTest/NewHRAFlows_Objects/iframe_SignOut_appCCM</value>
      <webElementGuid>43d37d71-e37e-4932-8646-c022a470ed56</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='panel46ColumnsSelect']/div/span/span</value>
      <webElementGuid>09e54967-a26a-47f6-9115-c79a849da5fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CASE MANAGEMENT'])[1]/following::span[2]</value>
      <webElementGuid>670c6df0-f722-4f72-bcb5-56cd9bb51310</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Physical Exam'])[1]/following::span[14]</value>
      <webElementGuid>aacc5abe-beb4-4a02-8b54-546df2d55613</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='×'])[1]/preceding::span[2]</value>
      <webElementGuid>272edadf-332b-4ec8-8839-2860e203820e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SUMMARY AND RECOMMENDATIONS'])[1]/preceding::span[5]</value>
      <webElementGuid>a72cf888-617f-4e36-8df7-3310c32b2557</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Select']/parent::*</value>
      <webElementGuid>5f2e8327-0c5b-411a-bc8a-936c8d8a0fbf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/span</value>
      <webElementGuid>4adb8a92-7479-48ad-9636-c2db88a5fee8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
