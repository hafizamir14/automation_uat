<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_CoHort_SaveInput</name>
   <tag></tag>
   <elementGuidId>ffd7b62c-6fc4-4d2a-86b3-fe892901f14f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input[placeholder='Enter cohort name']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_Telehealth/Obj_frame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@id='tocGrid']//table)[2]//tr[1]//td[57]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Telehealth/Obj_frame</value>
   </webElementProperties>
</WebElementEntity>
