<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_CoHortSaveMessage</name>
   <tag></tag>
   <elementGuidId>0f2faeb6-0961-4511-80c8-9ef582c43d36</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(), 'Cohort saved successfully')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_Telehealth/Obj_frame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@id='tocGrid']//table)[2]//tr[1]//td[57]</value>
   </webElementProperties>
</WebElementEntity>
