<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_Transitioni_AssignDate</name>
   <tag></tag>
   <elementGuidId>f88cc981-087a-4f4b-b0e0-4686de6318cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_Telehealth/Obj_frame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@id='toc_filter_start_assign_date'])</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@id='tocGrid']//table)[2]//tr[1]//td[57]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Telehealth/Obj_frame</value>
   </webElementProperties>
</WebElementEntity>
