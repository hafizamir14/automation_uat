<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_CareProvider</name>
   <tag></tag>
   <elementGuidId>a3262f39-381c-4118-b40b-6660cd3b9c2a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_Telehealth/Obj_frame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[@id='tocGrid']//table)[4]//tr[1]//td[contains(text(), 'Dove, Jane')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@id='tocGrid']//table)[2]//tr[1]//td[55]</value>
      <webElementGuid>fae24722-026c-4e43-bcc1-803e2a4a138c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Telehealth/Obj_frame</value>
      <webElementGuid>1679c960-4621-4eb0-9cb2-e5661729216c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
