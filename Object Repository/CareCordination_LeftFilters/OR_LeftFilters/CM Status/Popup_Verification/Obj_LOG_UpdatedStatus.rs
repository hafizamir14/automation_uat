<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_LOG_UpdatedStatus</name>
   <tag></tag>
   <elementGuidId>6bd55a6c-7697-46d3-ab74-8c28c97d3b6f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_Telehealth/Obj_frame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[contains(@class, 'col-sm-12 status_log')])[1]/div[2]//tr[1]//td[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@class='k-grid-content k-auto-scrollable'])[10]//tr[1]//td[3]</value>
      <webElementGuid>a6a59456-6950-412f-be04-cbef0974bfd7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Telehealth/Obj_frame</value>
      <webElementGuid>f8445ad4-3541-4f19-a1bd-b8ee42df6358</webElementGuid>
   </webElementProperties>
</WebElementEntity>
