<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_LOG_UpdatedDate</name>
   <tag></tag>
   <elementGuidId>9ce789fd-fac0-4d38-9cd5-eb97a2c9a179</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_Telehealth/Obj_frame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[contains(@class, 'col-sm-12 status_log')])[1]/div[2]//tr[1]//td[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@class='k-grid-content k-auto-scrollable'])[10]//tr[1]//td[2]</value>
      <webElementGuid>f566f94e-9366-4d4f-ac7e-ac2dd289f507</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Telehealth/Obj_frame</value>
      <webElementGuid>a75d3eb4-180c-431c-8486-1f17ec480376</webElementGuid>
   </webElementProperties>
</WebElementEntity>
