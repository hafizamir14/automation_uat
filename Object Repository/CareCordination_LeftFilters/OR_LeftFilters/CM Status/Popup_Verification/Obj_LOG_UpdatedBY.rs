<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_LOG_UpdatedBY</name>
   <tag></tag>
   <elementGuidId>73fbf5ab-3426-4553-9601-443aae4dd3ee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_Telehealth/Obj_frame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[contains(@class, 'col-sm-12 status_log')])[1]/div[2]//tr[1]//td[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@class='k-grid-content k-auto-scrollable'])[10]//tr[1]//td[1]</value>
      <webElementGuid>d5c60da9-aa14-4641-b393-6bdbd09ab655</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Telehealth/Obj_frame</value>
      <webElementGuid>1ad17ac2-0dd4-4bbd-a29f-d090758517dd</webElementGuid>
   </webElementProperties>
</WebElementEntity>
