<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_Export_BTNCLick</name>
   <tag></tag>
   <elementGuidId>20b355e0-59a5-41a5-b8d5-7bdfcf010f54</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='toc_btnExport']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_Telehealth/Obj_frame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@class='k-header k-grid-toolbar'])[3]/div[1]</value>
      <webElementGuid>b8894067-1110-469f-b7bc-1d332ab9c5b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Telehealth/Obj_frame</value>
      <webElementGuid>01865fc4-6353-4945-a71f-9997ab13a6b3</webElementGuid>
   </webElementProperties>
</WebElementEntity>
