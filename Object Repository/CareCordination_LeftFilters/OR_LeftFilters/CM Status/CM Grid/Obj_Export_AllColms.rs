<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_Export_AllColms</name>
   <tag></tag>
   <elementGuidId>1fd041c9-b0b9-4d45-873a-d6bca9bb579f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id='toc_ulExport']/li[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_Telehealth/Obj_frame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@class='k-header k-grid-toolbar'])[3]/div//li[1]</value>
      <webElementGuid>9233fb09-d979-4362-9c65-8c72ea21ff3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Telehealth/Obj_frame</value>
      <webElementGuid>f3e979a3-95c7-4dde-904a-b28987c921a0</webElementGuid>
   </webElementProperties>
</WebElementEntity>
