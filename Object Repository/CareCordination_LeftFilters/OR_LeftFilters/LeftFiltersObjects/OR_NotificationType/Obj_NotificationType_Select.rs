<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_NotificationType_Select</name>
   <tag></tag>
   <elementGuidId>27f53f03-e0fc-4b58-9279-a2ba2485d873</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id=&quot;toc_filter_notification_listbox&quot;]/li[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'CCM' or . = 'CCM') and @ref_element = 'Object Repository/OR_PatientGrid/OR_LeftFilters/GridObjects/Obj_CCMFrame']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>li.k-item.k-state-hover</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>04ffc85c-a825-4333-b06a-ba070f9948b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>2baf0cb9-366f-43cb-8978-1fa4ac6c99d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>option</value>
      <webElementGuid>c8d68f80-77f9-4864-a1bd-db3f8a5c1cec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>unselectable</name>
      <type>Main</type>
      <value>on</value>
      <webElementGuid>cba842d3-2426-429c-8776-34102fcd05fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>k-item k-state-hover</value>
      <webElementGuid>2afac55f-cba9-41f2-8b88-84e98c2e8a2c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-offset-index</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>77f0e7b4-2926-47d3-8276-1553460daea4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>CCM</value>
      <webElementGuid>8d26cc5a-4b7d-42a7-95a2-e3ed66a83c94</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;careType_listbox&quot;)/li[@class=&quot;k-item k-state-hover&quot;]</value>
      <webElementGuid>69d94948-f3ce-4fd4-a8ef-c966a1b86cab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_LeftFilters/GridObjects/Obj_CCMFrame</value>
      <webElementGuid>bf3e35b6-a8b7-43bf-bd56-873621ea3f18</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='careType_listbox']/li</value>
      <webElementGuid>e88cc73c-f08b-42f2-b199-41654ad9da12</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All'])[37]/following::li[1]</value>
      <webElementGuid>82ddbabc-c73b-4ad6-a3fc-4f9b5b7ef5de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MCare Readmission'])[2]/following::li[2]</value>
      <webElementGuid>c87cf22f-5cc6-46fb-885f-cdc38f9b04f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CJR'])[2]/preceding::li[1]</value>
      <webElementGuid>75e8e63d-bbf7-42eb-bd0d-e86924b7e9ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Heart Failure'])[2]/preceding::li[2]</value>
      <webElementGuid>feea19a8-b933-4b7e-8e7d-0bfeb5bbc0f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[90]/div/div[3]/ul/li</value>
      <webElementGuid>b542e79b-4d05-4863-ab40-8438f5e1b3ad</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
