<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_CovidBundleClick</name>
   <tag></tag>
   <elementGuidId>1b8a925a-35d9-4f03-b31b-0e29ceda4232</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(@aria-owns, 'toc_filter_covid_status_listbox')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>c03f4d3f-f671-46c4-a277-f67f07be0622</webElementGuid>
   </webElementProperties>
</WebElementEntity>
