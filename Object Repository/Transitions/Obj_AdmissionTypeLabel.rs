<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_AdmissionTypeLabel</name>
   <tag></tag>
   <elementGuidId>3a0eae96-aa9f-4165-b35d-9b91dc7b24a6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(text(), 'Admission Type')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>e7012666-0adb-4b5b-a4c5-5463dd07869e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
