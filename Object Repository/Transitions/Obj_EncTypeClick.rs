<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_EncTypeClick</name>
   <tag></tag>
   <elementGuidId>c21f77ce-5642-49e1-a8bb-7860a066a8ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ul[contains(@id, 'toc_filter_encounter_taglist')]//parent::div</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>72ccf050-1437-4cfa-a2d2-7f813247b4c2</webElementGuid>
   </webElementProperties>
</WebElementEntity>
