<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_ReceivingFacilityColVerify</name>
   <tag></tag>
   <elementGuidId>371c8555-5b55-4e16-a91a-d1c5d699b3e4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//th[@data-field='pacPracticeName']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>f6f37d4f-1f88-4d4b-b8ed-7a9f92b6afb0</webElementGuid>
   </webElementProperties>
</WebElementEntity>
