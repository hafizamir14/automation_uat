<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_DRGCodeValueVerify</name>
   <tag></tag>
   <elementGuidId>f82197e0-fdcf-4cbc-afc4-cba4e2cc6965</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@id='tocGrid'])//div[4]//tr[1]//td[contains(text(), '469')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>e3c1cfef-d165-47a2-a5e3-aec8989d2d5f</webElementGuid>
   </webElementProperties>
</WebElementEntity>
