<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_DRGDescValueVerify</name>
   <tag></tag>
   <elementGuidId>7adc0f78-a59b-4225-ae64-72f629833644</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@id='tocGrid'])//div[4]//tr[1]//div[contains(text(), 'MAJOR HIP AND KNEE JOINT')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>6549bc53-aa90-4f06-97e4-31d913bd5c1b</webElementGuid>
   </webElementProperties>
</WebElementEntity>
