<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_EncTpyeVerify</name>
   <tag></tag>
   <elementGuidId>c12ed5f1-ea88-4960-b397-feb49e98a9c4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='toc_filter_form']/div[1]/div[8]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>e7f38887-2f9c-459f-a29e-bdbeee140b4b</webElementGuid>
   </webElementProperties>
</WebElementEntity>
