<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_CovidBundleLabel</name>
   <tag></tag>
   <elementGuidId>873a284f-4083-4dc5-bbfc-aa2795a3c7bc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(text(), 'COVID Bundle')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>e6f6d04d-63e0-4344-b628-e9cd125de8d3</webElementGuid>
   </webElementProperties>
</WebElementEntity>
