<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_PrimeHealthCare</name>
   <tag></tag>
   <elementGuidId>494b44c7-a66e-4e04-9490-181d8d55262e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//option[contains(text(),'Prime Healthcare')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#selectedEnterprise</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>17c3ce48-9b89-4409-8ae3-dc10fc1165ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav navbar-nav</value>
      <webElementGuid>dcb30139-c205-4028-99b4-9daa62421716</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>selectedEnterprise</value>
      <webElementGuid>88ed6996-3871-4e7a-8e3e-6a7e9dab65e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
								
											Aprima Enterprise_1
								
											MHPN
								
											Prime Healthcare
								
						</value>
      <webElementGuid>5526db5d-13c1-4d79-9306-4a31519e4d93</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;selectedEnterprise&quot;)</value>
      <webElementGuid>7b08941b-7801-4c1a-a5d1-eb640f707ad5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='selectedEnterprise']</value>
      <webElementGuid>181d00fb-01aa-49f6-a8f8-06c059c83dea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='enterprise-dropdown']/select</value>
      <webElementGuid>718606c7-7ae0-42fa-892e-801db7eef05d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CM User Guide'])[1]/following::select[1]</value>
      <webElementGuid>543ab841-dda1-4af2-9d7c-bdb506203012</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Care Coordination'])[1]/following::select[1]</value>
      <webElementGuid>d664ea05-1b2c-483d-a2db-ceb96634c8df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Amir, Hafiz'])[1]/preceding::select[1]</value>
      <webElementGuid>7d07946b-db6a-4ce9-8d68-1d7b3f6c4d30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Amir, Hafiz'])[2]/preceding::select[1]</value>
      <webElementGuid>1299b2f3-b7d9-46cb-a13e-2ec19f27b338</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>9bb06b58-b852-4e72-9d40-02f61ed0acae</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
