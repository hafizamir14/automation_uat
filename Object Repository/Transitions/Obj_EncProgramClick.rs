<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_EncProgramClick</name>
   <tag></tag>
   <elementGuidId>9a1ac0e7-b2b5-401a-93cf-8d9aa5856ae1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='toc_filter_program']//parent::div</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>26813606-cb9d-4371-8641-1c9eeb3b4aa3</webElementGuid>
   </webElementProperties>
</WebElementEntity>
