<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_NotifyTypeVerify</name>
   <tag></tag>
   <elementGuidId>aefd98c4-dc85-408d-b8f6-ddfd087b7df4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='toc_filter_form']/div[1]/div[10]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>4bb0c7fb-877d-4bbd-a67b-3e6036ac29be</webElementGuid>
   </webElementProperties>
</WebElementEntity>
