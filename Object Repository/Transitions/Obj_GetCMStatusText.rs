<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_GetCMStatusText</name>
   <tag></tag>
   <elementGuidId>4ab4e8d4-6276-4d79-9b7f-b4df64f4f154</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[@aria-owns= 'toc_editor_programStatusId_listbox']/span/span[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>5f45bd82-f549-47ec-a61b-e2a60b6e2170</webElementGuid>
   </webElementProperties>
</WebElementEntity>
