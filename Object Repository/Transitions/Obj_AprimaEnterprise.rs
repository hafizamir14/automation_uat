<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_AprimaEnterprise</name>
   <tag></tag>
   <elementGuidId>6778fe84-e36f-4634-bdf9-67820780e3e6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='selectedEnterprise']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#selectedEnterprise</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>506c739d-df7b-4b59-9b36-f29e499eead7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav navbar-nav</value>
      <webElementGuid>0ed4a4cf-4ae1-481c-8735-1ef3887be641</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>selectedEnterprise</value>
      <webElementGuid>3460b00b-ed63-4ed9-8b8b-76acfb0f9aab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
								
											Aprima Enterprise_1
								
											MHPN
								
											Prime Healthcare
								
						</value>
      <webElementGuid>215d9d3e-93c8-4771-a142-ee4da56e5251</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;selectedEnterprise&quot;)</value>
      <webElementGuid>26b2c96f-f25d-4d51-b1d2-7a8ff3161559</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='selectedEnterprise']</value>
      <webElementGuid>f99972a3-2d0e-4547-86b8-162fa95b1b73</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='enterprise-dropdown']/select</value>
      <webElementGuid>3da30c03-c8d0-4386-832c-3fcd856f8dc8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CM User Guide'])[1]/following::select[1]</value>
      <webElementGuid>26f8bd41-66f1-42de-9a9a-cedfaf081815</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Care Coordination'])[1]/following::select[1]</value>
      <webElementGuid>0be96383-6400-4dc1-8001-198eae8f995f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Amir, Hafiz'])[1]/preceding::select[1]</value>
      <webElementGuid>11e1aa6c-b0f0-4a26-bf28-9563bf6bc807</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Amir, Hafiz'])[2]/preceding::select[1]</value>
      <webElementGuid>973e5907-6e26-4cec-aa52-73a514837230</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>78d7554f-bfd6-49ea-bb96-e6e727c77893</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
