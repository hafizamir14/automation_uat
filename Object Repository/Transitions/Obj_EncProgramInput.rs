<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_EncProgramInput</name>
   <tag></tag>
   <elementGuidId>f888a395-9e65-4572-a4f8-245a61857e5d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@aria-owns='toc_filter_program_taglist toc_filter_program_listbox']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>648eedec-8080-42a0-8ce4-d403da456c3b</webElementGuid>
   </webElementProperties>
</WebElementEntity>
