<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_DRGCodeVerify</name>
   <tag></tag>
   <elementGuidId>be4a0085-c7c4-48dc-8a4d-129a349a8eb6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//th[@data-field='drgCode']//a[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>a41714c1-351d-4b65-9067-d0c9a7f069f8</webElementGuid>
   </webElementProperties>
</WebElementEntity>
