<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_CovidBundleVerify</name>
   <tag></tag>
   <elementGuidId>92f3e22b-ee0b-4c0c-87b1-40fa2b818d15</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(@aria-owns, 'toc_filter_covid_status_listbox')]/span/span[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>b15dc2a3-56fb-4ed2-aa0c-e3bc9a23a318</webElementGuid>
   </webElementProperties>
</WebElementEntity>
