<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_EncTypeValueVerify</name>
   <tag></tag>
   <elementGuidId>cffb6ac2-162b-40d9-97fe-cbcbd849acbe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ul[contains(@id, 'toc_filter_encounter_taglist')]/li[1]/span[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>5e5250af-02c0-4b06-a2e5-11b8ea209519</webElementGuid>
   </webElementProperties>
</WebElementEntity>
