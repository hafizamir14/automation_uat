<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_DRGDescriptionVerify</name>
   <tag></tag>
   <elementGuidId>94272dd2-680d-48ed-bd07-c1fdd5ab605a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//th[@data-field='drgDescription']//a[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>598131f0-a8ce-486d-8bfb-77d39f4dc507</webElementGuid>
   </webElementProperties>
</WebElementEntity>
