<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_Sign Out</name>
   <tag></tag>
   <elementGuidId>a77f5169-31e1-479c-850e-f96080b8bd47</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#signOutButton</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@id='signOutButton'])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>f6430a51-3e46-48c5-b9d4-c302dd74b11d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>signOutButton</value>
      <webElementGuid>1458de1d-753a-41af-87d7-c6f1288c5ddd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-signout</value>
      <webElementGuid>59bfcc55-b6ec-4308-80c9-9bafe2c8d34f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign Out</value>
      <webElementGuid>90ffcc2b-17be-4e28-9e42-ae756f16279f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;signOutButton&quot;)</value>
      <webElementGuid>901bc9e1-001b-4e51-81c4-83848d19a5e8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='signOutButton']</value>
      <webElementGuid>1af1eda4-1d93-49c6-9d44-ba1c6daa2534</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='formLogin']/button</value>
      <webElementGuid>ecf9119c-c060-47e1-9afe-fd9b4653a335</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='amir@slt.com'])[1]/following::button[1]</value>
      <webElementGuid>49b4b2b8-4176-4952-98a8-a29bc4253402</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Amir, Hafiz'])[2]/following::button[1]</value>
      <webElementGuid>1f0ca9c4-ca48-40d2-809b-7be6ae3f6717</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RingCentral'])[1]/preceding::button[1]</value>
      <webElementGuid>430adc8d-5a67-4d1a-a1c8-276f143abd26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign Out']/parent::*</value>
      <webElementGuid>74fab546-6c63-4034-89fc-1b74c1e6e36b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/button</value>
      <webElementGuid>eca59d61-8123-4fa5-923c-b195c023bb06</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
