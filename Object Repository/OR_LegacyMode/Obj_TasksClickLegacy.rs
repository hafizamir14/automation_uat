<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_TasksClickLegacy</name>
   <tag></tag>
   <elementGuidId>84edea7b-8149-40b6-9c1d-c09539196fed</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[contains(@onclick, 'campaign')]/div[contains(text(), 'Tasks')]//parent::li</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
