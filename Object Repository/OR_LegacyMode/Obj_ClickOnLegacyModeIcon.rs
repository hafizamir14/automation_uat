<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_ClickOnLegacyModeIcon</name>
   <tag></tag>
   <elementGuidId>4d4914ec-18f1-4ac6-a2a2-a13000f9dd57</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//a[@id='navSwitch'])[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>2a97afe1-f87f-43f0-9d0b-3b9eddd31b9d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
