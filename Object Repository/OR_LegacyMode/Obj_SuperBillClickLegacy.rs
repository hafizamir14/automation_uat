<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_SuperBillClickLegacy</name>
   <tag></tag>
   <elementGuidId>f84d0bca-8d11-4c60-b199-34390afc248c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[contains(@onclick, 'superbill')]/div[contains(text(), 'Superbill')]//parent::li</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
