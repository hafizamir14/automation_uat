<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>inputPatient</name>
   <tag></tag>
   <elementGuidId>4cdb375a-981a-414d-9f4a-036a293e1c06</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@data-bind=&quot;value:filters[0].value&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>a9db0d97-2d81-48c0-b875-9bbd53894226</webElementGuid>
   </webElementProperties>
</WebElementEntity>
