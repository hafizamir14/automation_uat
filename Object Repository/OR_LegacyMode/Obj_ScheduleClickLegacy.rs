<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_ScheduleClickLegacy</name>
   <tag></tag>
   <elementGuidId>4c365d38-862e-4dba-af98-8b268af7fb8d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[contains(@onclick, 'schedule')]/div[contains(text(), 'Schedule')]//parent::li</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
