<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_PatientClickLegacy</name>
   <tag></tag>
   <elementGuidId>5eae74b0-a516-4598-a9be-0d341248276e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[contains(@onclick, 'patient')]/div[contains(text(), 'Patients')]//parent::li</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
