<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>filterlabel</name>
   <tag></tag>
   <elementGuidId>19da0c78-7ad6-47b2-8efc-26c50868e9de</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@class=&quot;k-item k-filter-item k-state-default k-last&quot;]//span[@class=&quot;k-link&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>a940ec3c-3cc7-4516-92f4-d957828d8ae4</webElementGuid>
   </webElementProperties>
</WebElementEntity>
