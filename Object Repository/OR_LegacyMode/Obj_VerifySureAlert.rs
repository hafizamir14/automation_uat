<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_VerifySureAlert</name>
   <tag></tag>
   <elementGuidId>0120a743-cde4-4efb-a768-6771c8697d36</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class, 'modal-content borderRadiusBottom')]//div[contains(text(), 'You are about to change the theme')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
