<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_InputSearch</name>
   <tag></tag>
   <elementGuidId>bc713633-9739-4277-b2bd-4c36d5ae22f9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@class='k-input global-patient-search'])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Telehealth/Obj_frame</value>
      <webElementGuid>88336490-7805-417e-8946-b2731e4dd8d4</webElementGuid>
   </webElementProperties>
</WebElementEntity>
