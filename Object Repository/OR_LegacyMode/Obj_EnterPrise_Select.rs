<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_EnterPrise_Select</name>
   <tag></tag>
   <elementGuidId>a2b006dd-4b51-404b-827d-24d981ff1a5f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//ul[contains(@class, 'enterpriseCapsulePadding')]//li[contains(text(), 'Harmony Cares Medical Group')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
