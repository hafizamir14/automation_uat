<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>filterbutton</name>
   <tag></tag>
   <elementGuidId>c0a97275-cfc2-46dc-8765-f270f571f827</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@type=&quot;submit&quot; and @class=&quot;k-button k-primary&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_OpenPatient/frame</value>
      <webElementGuid>64317a1c-3ce4-47a3-8ee5-d3b15efdd815</webElementGuid>
   </webElementProperties>
</WebElementEntity>
