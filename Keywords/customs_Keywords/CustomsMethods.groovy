package customs_Keywords
import java.text.DateFormat
import java.text.SimpleDateFormat

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords



public class CustomsMethods {





	@Keyword
	def static refreshBrowser() {
		KeywordUtil.logInfo("Refreshing")
		WebDriver webDriver = DriverFactory.getWebDriver()
		webDriver.navigate().refresh()
		KeywordUtil.markPassed("Refresh successfully")
	}


	@Keyword
	public static clickElement(TestObject to) {
		try {
			WebElement element = WebUiBuiltInKeywords.findWebElement(to);
			KeywordUtil.logInfo("Clicking element")
			element.click()
			KeywordUtil.markPassed("Element has been clicked")
		} catch (WebElementNotFoundException e) {
			KeywordUtil.markFailed("Element not found")
		} catch (Exception e) {
			KeywordUtil.markFailed("Fail to click on element")
		}
	}


	@Keyword
	def static CharacterCount(String Comment) {

		String exampleString = Comment;

		int stringLength = exampleString.length();

		println("String length: " + stringLength);

		//		int stringLengthWithoutSpaces = exampleString.replace(" ", "").length();
		//		print("String length without counting whitespaces: " + stringLengthWithoutSpaces);
	}

	@Keyword
	def static DateFormat() {

		// Create object of SimpleDateFormat class and decide the format
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy ");

		//get current date time with Date()
		Date date = new Date();

		// Now format the date
		String date1= dateFormat.format(date);

		// Print the Date
		System.out.println(date1);
	}

	public String parseDate(StringBuilder dateString, String formatString) {
		String separator = "";
		String slashSeperator = "/";
		if (formatString.contains(slashSeperator)) {
			separator = slashSeperator;
		} else {
			String dotSeparator = ".";
			if (formatString.contains(dotSeparator)) {
				separator = dotSeparator;
			}
		}

		if (dateString.length() == 8) {
			dateString.insert(2, separator);
			dateString.insert(5, separator);
		} else {
			return "False Date Entered";
		}

		return dateString.toString();
	}
}